package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Iman on 5/9/2017.
 */

public class Book
{
    public static Book book;
    private String id;
    private String book_sub_id;
    private String book_have_sub;
    private String book_name;
    private String book_factor;
    private String book_academic_year;
    private String book_public_pro;
    private String book_describe;
    private String book_view_type;

    public String getId() {
        return id;
    }

    public String getBook_sub_id() {
        return book_sub_id;
    }

    public String getBook_have_sub() {
        return book_have_sub;
    }

    public String getBook_name() {
        return book_name;
    }

    public String getBook_factor() {
        return book_factor;
    }

    public String getBook_academic_year() {
        return book_academic_year;
    }

    public String getBook_public_pro() {
        return book_public_pro;
    }

    public String getBook_describe() {
        return book_describe;
    }

    public String getBook_view_type() {
        return book_view_type;
    }

    public Book(JSONArray object){

        try
        {
            this.id = object.get(0).toString();
            this.book_sub_id = object.get(1).toString();
            this.book_have_sub = object.get(2).toString();
            this.book_name = object.get(3).toString();
            this.book_factor = object.get(4).toString();
            this.book_academic_year = object.get(5).toString();
            this.book_public_pro = object.get(6).toString();
            this.book_describe = object.get(7).toString();
            this.book_view_type = object.get(8).toString();
        }
        catch (JSONException e){e.printStackTrace();}

    }

    public Book(String name)
    {
        this.book_name = name;
    }
}
