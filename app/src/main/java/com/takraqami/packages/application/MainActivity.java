package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.takraqami.packages.application.Darsname.CategoryItem;
import com.takraqami.packages.application.Darsname.CategoryItemAdapter;
import com.takraqami.packages.application.Darsname.DarsnameActivity;
import com.takraqami.packages.application.test.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import co.ronash.pushe.Pushe;

public class MainActivity extends AppCompatActivity {

    public static EditText phone,name,phonelogin;
    public static Spinner major;
    public static Button singin, guest, singup;

    public static Account account;
    public static Context context;
    public static MainActivity activity;
    public static Login login;
    private static String[] majorArray;
    private static ArrayAdapter majorAdpt;

    public static GetAccount accountManager;
    public SectionsPagerAdapter mSectionsPagerAdapter;
    public ViewPager mViewPager;

    public static Typeface font;

    static AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        Pushe.initialize(this,true);
        if(!getSharedPreferences("androidhive-welcome",MODE_PRIVATE).getBoolean("pushe_reg",false)
                && Pushe.isPusheInitialized(this)
                && !IntroActivity.manager.getStudent_id().isEmpty()){
            registerPushe();
        }

//        ((ImageView)findViewById(R.id.imageView2)).setImageResource(R.drawable.logo);
        if(new NetworkConnection().check(MainActivity.this))
        {
            font = Typeface.createFromAsset(getAssets(),"fonts/font.ttf");

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setAdapter(mSectionsPagerAdapter);


            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);
        }

        activity = this;
        context = MainActivity.this;

        if(IntroActivity.manager.isLogedin())
        {
            Intent intent = new Intent(MainActivity.this,ActivitySplash.class);
            startActivity(intent);
            finish();
        }

    }

    public void registerPushe(){
        StringRequest req = new StringRequest(Request.Method.POST , "http://takraqami.ir/Application/GetPushID.php"
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                if(response.contains("1")){
                    System.out.println(response);
                    getSharedPreferences("androidhive-welcome",MODE_PRIVATE).edit().putBoolean("pushe_reg",true).apply();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                //System.out.print();
                params.put("user_id",IntroActivity.manager.getStudent_id());
                params.put("push_id",Pushe.getPusheId(MainActivity.this));
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(req);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "ورود";
                case 1:
                    return "ثبت نام";
            }
            return null;
        }
    }
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment()
        {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = null;

            switch(getArguments().getInt(ARG_SECTION_NUMBER))
            {
                case 1:
                    rootView = inflater.inflate(R.layout.login_page, container, false);
                    phonelogin = (EditText) rootView.findViewById(R.id.phonelogin);
                    phonelogin.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int i, KeyEvent keyEvent) {
                            if(((EditText)view).getText().length()>11)
                            {
                                ((EditText)view).setText("");
                                Toast.makeText(context,"برای شماره تلفن فقط مجاز به وارد کردن 11 کاراکتر میباشید",Toast.LENGTH_LONG).show();
                            }
                            return false;
                        }
                    });
                    singin = (Button) rootView.findViewById(R.id.singin);
                    singin.setTypeface(font);
                    guest = (Button) rootView.findViewById(R.id.guest);
                    guest.setTypeface(font);
                    new getMajor().execute();
                    singin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            login = new Login();
                            if(new NetworkConnection().check(context)) {
                                MainActivity.account = null;
                                if (phonelogin.getText().toString() != "") {
                                    login.execute(phonelogin.getText().toString());
                                    login = null;
                                } else {
                                    Toast.makeText(MainActivity.context, "شماره تلفن را وارد نمایید", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });

                    guest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                View guest_field = LayoutInflater.from(context).inflate(R.layout.guest_field, null, false);
                                final Spinner field = (Spinner) guest_field.findViewById(R.id.major);
                                Button enter = (Button) guest_field.findViewById(R.id.singin);
                                TextView desc = (TextView) guest_field.findViewById(R.id.description);
                                desc.setTypeface(font);
                                enter.setTypeface(font);
                                if(new NetworkConnection().check(MainActivity.context))
                                {
                                    new getMajor().execute();
                                    majorAdpt = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, majorArray);
                                    field.setAdapter(majorAdpt);
                                }
                                enter.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(activity, ActivitySplash.class);
                                        IntroActivity.manager.setAccount_mode("Guest");
                                        IntroActivity.manager.setAccount_name("کاربر مهمان");
                                        IntroActivity.manager.setStudent_major(field.getSelectedItem().toString());
                                        IntroActivity.manager.setStudent_major_id(field.getSelectedItemPosition() + 1);
                                        startActivity(intent);
                                        dialog.dismiss();
                                        activity.finish();
                                    }
                                });
                                builder.setView(guest_field);
                                dialog = builder.create();
                                dialog.show();
                            }
                    });

                    break;
                case 2:
                    rootView = inflater.inflate(R.layout.activity_singup, container, false);
                    ((ImageView) rootView.findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

                    MainActivity.name = (EditText) rootView.findViewById(R.id.name);
                    MainActivity.phone = (EditText) rootView.findViewById(R.id.phone);
                    phone.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int i, KeyEvent keyEvent) {
                            if(((EditText)view).getText().length()>11)
                            {
                                ((EditText)view).setText("");
                                Toast.makeText(context,"برای شماره تلفن فقط مجاز به وارد کردن 11 کاراکتر میباشید",Toast.LENGTH_LONG).show();
                            }
                            return false;
                        }
                    });
                    MainActivity.major = (Spinner) rootView.findViewById(R.id.major);
                    MainActivity.singup = (Button) rootView.findViewById(R.id.singupbtn);
                    singup.setTypeface(font);

                    if(new NetworkConnection().check(MainActivity.context))
                    {
                        new GetData().execute();
                    }

                    singup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(name.getText().toString().isEmpty())
                            {
                                Toast.makeText(activity,"فیلد نام اجباری است",Toast.LENGTH_LONG).show();
                            }
                            else if(phone.getText().toString().isEmpty())
                            {
                                Toast.makeText(activity,"فیلد تلفن همراه اجباری است",Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                String[] account = new String[3];
                                account[0] = name.getText().toString();
                                account[1] = phone.getText().toString();
                                account[2] = String.valueOf(major.getSelectedItemId()+1);
                                //Toast.makeText(MainActivity.context,account[0] + " " + account[1] + " " + account[2], Toast.LENGTH_LONG).show();
                                new Singupsync().execute(new Account(account));
                            }
                        }
                    });

                    break;
            }

            return rootView;
        }
    }

    static class Singupsync extends AsyncTask<Account,Void,Void>
    {
        ProgressDialog dialog;

        public Singupsync()
        {
            dialog = new ProgressDialog(activity);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("در حال ارسال اطلاعات");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Account... accounts) {
            new SingupOperations().singup(accounts[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            if(SingupOperations.response.toString().equals("Existphone"))
            {
                Toast.makeText(context,"شماره قبلا ثبت شده است",Toast.LENGTH_LONG).show();
            }
            else if(SingupOperations.response.toString().equals("Error"))
            {
                //Error
            }
            else
            {
                Intent intent = new Intent(activity,Identify.class);
                intent.putExtra("phone",phone.getText().toString());
                intent.putExtra("code",SingupOperations.response);
                IntroActivity.manager.setAccount_mode("Registered");
                IntroActivity.manager.setStudent_id(SingupOperations.id);
                IntroActivity.manager.setAccount_name(name.getText().toString());
                IntroActivity.manager.setStudent_phone(phone.getText().toString());
                IntroActivity.manager.setStudent_major(major.getSelectedItem().toString());
                IntroActivity.manager.setStudent_major_id(major.getSelectedItemPosition()+1);
                context.startActivity(intent);
                activity.finish();
            }
        }
    }

    static class Login extends AsyncTask<String,Void,Void> {
        ProgressDialog loader = new ProgressDialog(MainActivity.activity);
        public Login()
        {
            accountManager = new GetAccount();
            MainActivity.account = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setMessage("در حال بازیابی اطلاعات حساب شما");
            loader.setCancelable(false);
            loader.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            accountManager.getAccount(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.dismiss();
            if(account != null) {
                Intent intent = new Intent(MainActivity.activity,Identify.class);

                intent.putExtra("phone",phonelogin.getText().toString());
                intent.putExtra("code",account.getCode().toString());


                if(account.getReciption().toString().equals("null"))
                {
                    IntroActivity.manager.setAccount_mode("Free");
                }
                else
                {
                    IntroActivity.manager.setAccount_mode("Activated");
                    IntroActivity.manager.setAccount_Reciption(account.getReciption());
                }

                IntroActivity.manager.setStudent_id(account.getID());
                IntroActivity.manager.setAccount_name(account.getFamily());
                IntroActivity.manager.setStudent_phone(account.getPhone());
                IntroActivity.manager.setStudent_major(account.getMajor());
                IntroActivity.manager.setStudent_major_id(Integer.parseInt(account.getMajor()));
                context.startActivity(intent);
                activity.finish();
            }
            else
            {
                GetAccount.registerDialog(MainActivity.context);
            }
        }
    }

    static class GetData extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getMajors();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            majorAdpt = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, majorArray);
            major.setAdapter(majorAdpt);
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }

    static class getMajor extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog mdialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mdialog = new ProgressDialog(context);
            mdialog.setMessage("لطفا صبر کنید");
            mdialog.setCancelable(false);
            mdialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getMajors();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mdialog.dismiss();
        }
    }

    private static void getMajors()
    {
        try
        {
            URL url = new URL("http://www.takraqami.ir/Application/Majors.php");
            URLConnection connection = url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("major");
            majorArray = new String[data.length()];
            for(int i = 0; i<data.length();i++)
            {
                JSONArray object = data.getJSONArray(i);
                majorArray[i] = object.get(0).toString();
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }
}