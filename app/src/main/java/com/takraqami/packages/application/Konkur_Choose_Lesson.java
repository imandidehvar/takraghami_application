package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bignerdranch.expandablerecyclerview.model.Parent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Konkur_Choose_Lesson extends AppCompatActivity {

    private ArrayList<Konkur_Lesson> special;
    private ArrayList<Konkur_Lesson> general;

    private ArrayList<Karnameh_Lesson> lessons;

    String timeGeneral, timeSpecial;
    boolean generalStatus, specialStatus;
    String konkur_id;
    Handler handler;
    Button btnKarnameh;

    private Map<String,List<Konkur_Lesson>> list;
    private Dialog dialogV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konkur__choose__lesson);

        handler = new Handler();
        konkur_id = getIntent().getExtras().get("konkur_id").toString();
        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        general = new ArrayList<Konkur_Lesson>();
        special = new ArrayList<Konkur_Lesson>();

        new GetDate().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()),konkur_id);

        btnKarnameh = (Button) findViewById(R.id.showKarnameh);
        btnKarnameh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
                {
                    if (Karnameh_item.answered.size() <= 0) {
/*                        for (int i = 0; i < general.size(); i++) {
                            Karnameh_item.answered.add(new Karnameh_item(general.get(i).getBook_name(), 0, Integer.parseInt(general.get(i).getBook_factor())));
                        }
                        for (int i = 0; i < special.size(); i++) {
                            Karnameh_item.answered.add(new Karnameh_item(special.get(i).getBook_name(), 0, Integer.parseInt(special.get(i).getBook_factor())));
                        }*/
                        for (int i = 0; i < lessons.size(); i++) {
                            Karnameh_item.answered.add(new Karnameh_item(lessons.get(i).name, lessons.get(i).id, 0,lessons.get(i).factor,konkur_id,false));
                        }
                        Intent intent = new Intent(Konkur_Choose_Lesson.this, Karnameh.class);
                        intent.putExtra("konkur_id",konkur_id);
                        startActivity(intent);
                        finish();
                    } else {
                        for (int i = 0; i < lessons.size(); i++)
                        {
                            boolean flag = false;
                            for (int j = 0; j < Karnameh_item.answered.size(); j++)
                            {
                                if(Karnameh_item.answered.get(j).name.equals(lessons.get(i).name.toString()))
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            if(flag == false)
                            {
                                Karnameh_item.answered.add(new Karnameh_item(lessons.get(i).name, lessons.get(i).id, 0,lessons.get(i).factor,konkur_id,false));
                            }
                        }
                        Intent intent = new Intent(Konkur_Choose_Lesson.this, Karnameh.class);
                        intent.putExtra("konkur_id",konkur_id);
                        startActivity(intent);
                        finish();
                    }
                }
            });
    }

    class ExpandableAdapter extends ExpandableRecyclerAdapter<GroupName,Konkur_Lesson, ParentView, ChildView>
    {
        Context mContext;
        List<GroupName> list;

        public ExpandableAdapter(Context context, List<GroupName> parentItemList) {
            super(parentItemList);
            mContext = context;
            list = parentItemList;
        }

        @Override
        public ParentView onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.konkur_lesson_header, parentViewGroup, false);
            return new ParentView(view);
        }

        @Override
        public ChildView onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.test_tamrin_item_child, childViewGroup, false);
            return new ChildView(view,childViewGroup);
        }

        @Override
        public void onBindParentViewHolder(final ParentView parentViewHolder,final int parentPosition, final GroupName parent)
        {
            parentViewHolder.bind(parent);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  new GetTimer().execute(konkur_id);
                    if(parentViewHolder.headerText.getText().toString().equals("دروس تخصصی"))
                    {
                        parentViewHolder.timer.setText(timeSpecial);
                    }
                    else if(parentViewHolder.headerText.getText().toString().equals("دروس عمومی"))
                    {
                        parentViewHolder.timer.setText(timeGeneral);
                    }

                    if(generalStatus)
                    {
                        if(parentViewHolder.headerText.getText().toString().equals("دروس تخصصی"))
                        {
                            parentViewHolder.mLayout.setEnabled(false);
                            parentViewHolder.setExpanded(false);
                            collapseParent(parentPosition);

                        }
                        else if(parentViewHolder.headerText.getText().toString().equals("دروس عمومی"))
                        {
                            parentViewHolder.mLayout.setEnabled(true);
                            parentViewHolder.setExpanded(true);
                            expandParent(parentPosition);
                        }
                    }
                    else if(specialStatus)
                    {
                        if(parentViewHolder.headerText.getText().toString().equals("دروس تخصصی"))
                        {
                            parentViewHolder.mLayout.setEnabled(true);
                            parentViewHolder.setExpanded(true);
                            expandParent(parentPosition);
                        }
                        else if(parentViewHolder.headerText.getText().toString().equals("دروس عمومی"))
                        {
                            parentViewHolder.mLayout.setEnabled(false);
                            parentViewHolder.setExpanded(false);
                            collapseParent(parentPosition);

                        }
                    }
                    handler.postDelayed(this,1000);
                }
            },1000);
        }

        @Override
        public void onBindChildViewHolder(@NonNull ChildView childViewHolder, int parentPosition, int childPosition, @NonNull Konkur_Lesson child)
        {
            childViewHolder.bind(child);
        }

    }
    class ParentView extends ParentViewHolder {

        public TextView headerText, timer;
        public ImageView mParentDropDownArrow;
        public CardView mLayout;
        boolean status;

        public ParentView(View itemView) {
            super(itemView);
            status = false;
            headerText = (TextView) itemView.findViewById(R.id.parent_list_item_crime_title_text_view);
            timer = (TextView) itemView.findViewById(R.id.timer);
            mParentDropDownArrow = (ImageView) itemView.findViewById(R.id.parent_list_item_expand_arrow);
            mLayout = (CardView) itemView.findViewById(R.id.expandable_header);
            mLayout.setEnabled(false);
            headerText.setTypeface(MainActivity.font);
            mLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(status == false)
                    {
                        status = true;
                        mParentDropDownArrow.setBackgroundResource(R.drawable.ic_arrow_drop_up_white);
                    }
                    else
                    {
                        status = false;
                        mParentDropDownArrow.setBackgroundResource(R.drawable.ic_arrow_drop_down_white);
                    }
                }
            });
        }

        public void bind(GroupName name)
        {
            headerText.setText(name.name);
            mLayout.setBackgroundResource(name.color);
            timer.setText(name.time);
        }
    }

    class ChildView extends ChildViewHolder {

        public Button bookname;
        public Konkur_Lesson book;
        public ViewGroup root;
        public ChildView(final View itemView, final ViewGroup rootView) {
            super(itemView);
            bookname = (Button) itemView.findViewById(R.id.child_list_item_crime_date_text_view);
            bookname.setTypeface(MainActivity.font);
            root = rootView;
            bookname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!checkBook(book.getBook_name()))
                    {
                        Book.book = book;
                        Intent intent = new Intent(Konkur_Choose_Lesson.this,Konkur_Test.class);
                        intent.putExtra("book_id",book.getId());
                        intent.putExtra("konkur_id",konkur_id);
                        startActivity(intent);
                    }
                    else
                    {
                        Snackbar.make(itemView,"شما سوالات این درس را قبلا پاسخ داده اید!",Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }

        public void bind(Konkur_Lesson book)
        {
            bookname.setText(book.getBook_name());
            this.book = book;
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if((general.size()+special.size()) > 0 && Karnameh_item.answered.size() == (general.size()+special.size()))
        {
            dialog("پایان آزمون",
                    "شما به تمام درس های این ازمون پاسخ داده اید. \n جهت مشاهده ی کارنامه خود روی دکمه مشاهده کارنامه کلید کنید",
                    new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(Konkur_Choose_Lesson.this,Karnameh.class);
                    intent.putExtra("konkur_id",konkur_id);
                    startActivity(intent);
                    finish();
                }
            },"مشاهده ی کارنامه");
        }
    }

    public boolean checkBook(String name)
    {
        for (int i = 0; i < Karnameh_item.answered .size(); i++)
        {
            if(name.equals(Karnameh_item.answered.get(i).name.toString()))
            {
                return true;
            }
        }
        return false;
    }

    class GetDate extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Konkur_Choose_Lesson.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            GroupName group2 = new GroupName("دروس عمومی",R.drawable.expandable_header_style_general, general.subList(0,general.size()),"02:00:00");
            GroupName group1 = new GroupName("دروس تخصصی",R.drawable.expandable_header_style_special, special.subList(0,special.size()),"02:00:00");

            List<GroupName> groupNames = Arrays.asList(group2,group1);

            RecyclerView list = (RecyclerView) findViewById(R.id.lessons_list);
            ExpandableRecyclerAdapter adapter = new ExpandableAdapter(Konkur_Choose_Lesson.this,groupNames);
            list.setAdapter(adapter);
            list.setLayoutManager(new LinearLayoutManager(Konkur_Choose_Lesson.this,LinearLayoutManager.VERTICAL,false));
            dialog.dismiss();
        }
    }

    private void getData(String string)
    {
        try
        {
            lessons = new ArrayList<Karnameh_Lesson>();
            String req = URLEncoder.encode("major","utf-8") + "=" + URLEncoder.encode(string,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Get_Books.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
             StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("book");
            for(int i = 0; i < data.length();i++)
            {
                JSONArray object = data.getJSONArray(i);
                lessons.add(new Karnameh_Lesson(object.get(0).toString(),object.get(3).toString(),object.getInt(4)));
                if(object.get(6).equals("0"))
                {
                    general.add(new Konkur_Lesson(object));
                }
                else
                {
                    special.add(new Konkur_Lesson(object));
                }
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    class GetTimer extends AsyncTask<String,Void,Void>
    {
        @Override
        protected Void doInBackground(String... strings) {
            getTimer(strings[0]);
            return null;
        }
    }

    private void getTimer(String string)
    {
        try
        {
            String req = URLEncoder.encode("konkur_id","utf-8") + "=" + URLEncoder.encode(string,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Get_Konkur_timer.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray general = json.getJSONArray("gnrl");
            JSONArray special = json.getJSONArray("spec");
            timeGeneral = general.get(0).toString() + ":" + general.get(1).toString() + ":" + general.get(2).toString();
            timeSpecial = special.get(0).toString() + ":" + special.get(1).toString() + ":" + special.get(2).toString();
            if(general.get(3).toString().equals("1"))
            {
                generalStatus = true;
            }
            else
            {
                generalStatus = false;
            }
            if(special.get(3).toString().equals("1"))
            {
                specialStatus = true;
            }
            else
            {
                specialStatus = false;
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }


    class Karnameh_Lesson
    {
        public String id;
        public String name;
        public int factor;

        public Karnameh_Lesson(String id, String name, int factor)
        {
            this.id = id;
            this.name = name;
            this.factor = factor;
        }
    }

    public class GroupName implements Parent<Konkur_Lesson>
    {
        String name;
        int color;
        String time;
        private List<Konkur_Lesson> books;

        @Override
        public List<Konkur_Lesson> getChildList() {
            return books;
        }

        @Override
        public boolean isInitiallyExpanded() {
            return false;
        }

        public GroupName(String name, int color, List<Konkur_Lesson> book, String time) {
            this.name = name;
            this.color = color;
            this.books = book;
            this.time = time;
        }

    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, String btn1)
    {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_soon,parent,false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setText(btn1);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialogV = builder.create();
        dialogV.setCanceledOnTouchOutside(false);
        dialogV.setCancelable(false);
        dialogV.show();
        ok.setOnClickListener(onOkKeyPress);
        dialogV.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                finish();
            }
        });
    }

}
