package com.takraqami.packages.application.test;

import android.support.v4.app.Fragment;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by etteh on 5/14/2017.
 */

public abstract class FieldFragment extends Fragment {

    TextView farsiText;
    SeekBar farsiBar;

    TextView diniText;
    SeekBar diniBar;

    TextView arabiText;
    SeekBar arabiBar;

    TextView zabanText;
    SeekBar zabanBar;

    public String calculate(){
        return "";
    }

}
