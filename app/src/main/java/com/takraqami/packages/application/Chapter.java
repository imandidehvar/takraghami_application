package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Iman on 5/19/2017.
 */

public class Chapter
{
    public static Chapter chapter;
    private String chapterID;
    private String bookID;
    private String chapterTitle;

    public Chapter(JSONArray object)
    {
        try
        {
            this.chapterID = object.get(0).toString();
            this.bookID = object.get(1).toString();
            this.chapterTitle = object.get(2).toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public String getChapterID() {
        return chapterID;
    }

    public String getBookID() {
        return bookID;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }
}
