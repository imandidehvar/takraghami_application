package com.takraqami.packages.application.classroom;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.util.VolleySingleton;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ClassActivity extends AppCompatActivity {

    final String URL = "http://takraqami.ir/Application/Get_Videos.php";

    LinkedList<ClassItem> classes;
    ListView listView;
    CategoryItem category;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);

        category = new CategoryItem(getIntent().getStringExtra("id")
                ,getIntent().getStringExtra("name"),getIntent().getStringExtra("major"),getIntent().getStringExtra("number"));

        ((TextView) findViewById(R.id.classTextView)).setText(category.name);
        ((TextView) findViewById(R.id.classTextView)).setTypeface(MainActivity.font);
        findViewById(R.id.background_tarh).setBackgroundResource(R.mipmap.bg_tarh);
        listView = (ListView) findViewById(R.id.list);
        classes = new LinkedList<>();

        initList();
    }

    public void initList(){
        StringRequest req = new StringRequest(Request.Method.POST , URL , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray array = new JSONArray(response);
                    for(int i=0;i<array.length();i++){
                        classes.add( ClassItem.fromJson(array.getJSONObject(i)));
                        //sendDurationRequest(i);
                    }
                    listView.setAdapter(new ClassItemAdapter(ClassActivity.this
                            ,R.layout.class_view,classes));
                    progress.dismiss();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: 09/14/2017
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("id",category.id);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(req);
        progress = new ProgressDialog(ClassActivity.this);
        progress.setMessage("لطفا صبر کنید");
        progress.setCancelable(false);
        progress.show();
    }
}
