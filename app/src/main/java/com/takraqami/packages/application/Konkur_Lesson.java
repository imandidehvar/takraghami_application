package com.takraqami.packages.application;

import org.json.JSONArray;

/**
 * Created by Iman on 6/10/2017.
 */

public class Konkur_Lesson extends Book
{
    public boolean answered;

    public Konkur_Lesson(JSONArray object) {
        super(object);
    }

    public Konkur_Lesson(String name) {
        super(name);
    }
}
