package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Iman on 5/20/2017.
 */

public class Question
{
    private String id;
    private String question;
    private String trueSelect;
    private String testAnswer;
    private boolean status;

    public Question(JSONArray object)
    {
        try
        {
            this.id = object.get(0).toString();
            this.question = object.get(1).toString();
            this.trueSelect = object.get(2).toString();
            this.testAnswer = object.get(3).toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getTrueSelect() {
        return trueSelect;
    }

    public String getTestAnswer() {
        return testAnswer;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
