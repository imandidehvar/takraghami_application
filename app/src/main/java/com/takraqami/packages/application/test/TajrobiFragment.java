package com.takraqami.packages.application.test;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.Database.MyDatabase;


public class TajrobiFragment extends FieldFragment {

    TextView riaziText;
    SeekBar riaziBar;

    TextView zistText;
    SeekBar zistBar;

    TextView fizikText;
    SeekBar fizikBar;

    TextView shimiText;
    SeekBar shimiBar;

    public TajrobiFragment() {}

    public static TajrobiFragment newInstance() {
        return new TajrobiFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tajrobi, container, false);

        farsiText = (TextView) v.findViewById(R.id.farsiText);
        farsiBar = (SeekBar) v.findViewById(R.id.farsiBar);
        farsiBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(farsiText.getText().toString().contains(":")){
                    String first = farsiText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    farsiText.setText(first+second);
                } else{
                    farsiText.setText(farsiText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        diniText = (TextView) v.findViewById(R.id.diniText);
        diniBar = (SeekBar) v.findViewById(R.id.diniBar);
        diniBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(diniText.getText().toString().contains(":")){
                    String first = diniText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    diniText.setText(first+second);
                } else{
                    diniText.setText(diniText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        arabiText = (TextView) v.findViewById(R.id.arabiText);
        arabiBar = (SeekBar) v.findViewById(R.id.arabiBar);
        arabiBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(arabiText.getText().toString().contains(":")){
                    String first = arabiText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    arabiText.setText(first+second);
                } else{
                    arabiText.setText(arabiText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        zabanText = (TextView) v.findViewById(R.id.zabanText);
        zabanBar = (SeekBar) v.findViewById(R.id.zabanBar);
        zabanBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(zabanText.getText().toString().contains(":")){
                    String first = zabanText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    zabanText.setText(first+second);
                } else{
                    zabanText.setText(zabanText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        riaziText = (TextView) v.findViewById(R.id.riaziText);
        riaziBar = (SeekBar) v.findViewById(R.id.riaziBar);
        riaziBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(riaziText.getText().toString().contains(":")){
                    String first = riaziText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    riaziText.setText(first+second);
                } else{
                    riaziText.setText(riaziText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        zistText = (TextView) v.findViewById(R.id.zistText);
        zistBar = (SeekBar) v.findViewById(R.id.zistBar);
        zistBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(zistText.getText().toString().contains(":")){
                    String first = zistText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    zistText.setText(first+second);
                } else{
                    zistText.setText(zistText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        fizikText = (TextView) v.findViewById(R.id.fizikText);
        fizikBar = (SeekBar) v.findViewById(R.id.fizikBar);
        fizikBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(fizikText.getText().toString().contains(":")){
                    String first = fizikText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    fizikText.setText(first+second);
                } else{
                    fizikText.setText(fizikText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        shimiText = (TextView) v.findViewById(R.id.shimiText);
        shimiBar = (SeekBar) v.findViewById(R.id.shimiBar);
        shimiBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(shimiText.getText().toString().contains(":")){
                    String first = shimiText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    shimiText.setText(first+second);
                } else{
                    shimiText.setText(shimiText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return v;
    }

    @Override
    public String calculate(){
        double avg = ((farsiBar.getProgress()-33)*4 +
                (diniBar.getProgress()-33)*3 +
                (arabiBar.getProgress()-33)*2 +
                (zabanBar.getProgress()-33)*2 +
                (riaziBar.getProgress()-33)*2 +
                (zistBar.getProgress()-33)*4 +
                (fizikBar.getProgress()-33)*2 +
                (shimiBar.getProgress()-33)*3)/23 ;
        Cursor c = new MyDatabase(getContext(),MyDatabase.TAJROBI_DATABASE_NAME).getReadableDatabase()
                .query("tajrobi",new String[]{"rank"},"percent <= ?", new String[]{String.valueOf(avg)}
                        , null, null, "rank ASC");
        int first=0;
        int second=30000;
        int i=0;
        if (c != null && c.moveToFirst()) {
            do{
                if(i==0){
                    first = c.getInt(0);
                } else {
                    second = c.getInt(0);
                    break;
                }
                i++;
            } while (c.moveToNext());
        }
        if(c!=null){
            c.close();
        }
        if(second!=30000){
            return "در منطقه یک (احتمالا) بین "+String.valueOf(first)+" تا "+String.valueOf(second);
        }
        return "در منطقه یک (احتمالا) بالاتر از 30000";
    }

}
