package com.takraqami.packages.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ChooseChapter extends AppCompatActivity {

    ArrayList<Chapter> chapters;
    RecyclerView list;
    TextView title;
    String book_id,subBook_id;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_chapter);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        activity = this;
        list = (RecyclerView) findViewById(R.id.lessons_list);
        title = (TextView) findViewById(R.id.bookname);
        title.setTypeface(MainActivity.font);
        book_id = getIntent().getExtras().get("book_id").toString();
        subBook_id = getIntent().getExtras().get("subbook_id").toString();

        title.setText(Book.book.getBook_name());
        new GetData().execute(subBook_id);
    }

    class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        ViewGroup parent;
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            this.parent = parent;
            View view = LayoutInflater.from(ChooseChapter.this).inflate(R.layout.test_tamrin_item_child,parent,false);
            return new ConfigViewer(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ConfigViewer viewer = (ConfigViewer) holder;
            viewer.button.setText(chapters.get(position).getChapterTitle().toString());
            viewer.chapter = ChooseChapter.this.chapters.get(position);
        }

        @Override
        public int getItemCount() {
            return chapters.size();
        }
    }

    public class GetData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ChooseChapter.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            list.setLayoutManager(new LinearLayoutManager(ChooseChapter.this,LinearLayoutManager.VERTICAL,false));
            Adapter adapter = new Adapter();
            list.setAdapter(adapter);
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getData(String string) {
        try {
            chapters = new ArrayList<Chapter>();
            String req = URLEncoder.encode("book_id", "utf-8") + "=" + URLEncoder.encode(string, "UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/GetChapters.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("chapter");
            for (int i = 0; i < data.length(); i++) {
                JSONArray object = data.getJSONArray(i);
                chapters.add(new Chapter(object));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class ConfigViewer extends RecyclerView.ViewHolder {
        Button button;
        public Chapter chapter;

        public ConfigViewer(View view) {
            super(view);
            button = (Button) view.findViewById(R.id.child_list_item_crime_date_text_view);
            button.setTypeface(MainActivity.font);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ChooseChapter.this,Test.class);
                    Chapter.chapter = chapter;
                    intent.putExtra("book_id",book_id);
                    intent.putExtra("subbook_id",subBook_id);
                    intent.putExtra("chapter",chapter.getChapterID());
                    startActivity(intent);
                }
            });
        }
    }
}
