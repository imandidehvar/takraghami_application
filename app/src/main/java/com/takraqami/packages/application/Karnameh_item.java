package com.takraqami.packages.application;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Iman on 6/13/2017.
 */

public class Karnameh_item {
        public String name;
        public String id;
        public float percent;
        public int factor;
        public ArrayList<Question> questions;
        public ArrayList<Integer> answers;
        public boolean status;
        public String konkur_id;

    public static ArrayList<Karnameh_item> answered = new ArrayList<Karnameh_item>();

    public Karnameh_item(String name,float percent,int factor,ArrayList<Question> questions,ArrayList<Integer> answers,boolean status)
    {
        this.name = name;
        this.percent = percent;
        this.factor = factor;
        this.questions = questions;
        this.answers = answers;
        this.status = status;
    }

    public Karnameh_item(String name, String id,float percent,int factor,String konkur_id, boolean status)
    {
        this.name = name;
        this.id = id;
        this.percent = percent;
        this.factor = factor;
        this.konkur_id = konkur_id;
        this.status = status;
    }

    public Karnameh_item(String name, String id,float percent,int factor, boolean status)
    {
        this.name = name;
        this.id = id;
        this.percent = percent;
        this.factor = factor;
        this.konkur_id = konkur_id;
        this.status = status;
    }
    public Karnameh_item(String name, String id,float percent, boolean status)
    {
        this.name = name;
        this.id = id;
        this.percent = percent;
        this.konkur_id = konkur_id;
        this.status = status;
    }
}
