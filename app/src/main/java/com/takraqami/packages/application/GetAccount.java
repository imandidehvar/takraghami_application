package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by Iman on 4/13/2017.
 */

public class GetAccount
{
    public void getAccount(String phone) {
        try
        {
            String phonereq = URLEncoder.encode("phone","utf-8")+"="+URLEncoder.encode(phone,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Login.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(phonereq);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Log.d("Test=>", stringBuilder.toString());
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("accounts");
            JSONArray object = data.getJSONArray(0);
            if(object.length() <= 0)
            {
                MainActivity.account = null;
            }
            else
            {
                MainActivity.account = new Account(object);
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

/*    public static void paymentDialog(final Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("خرید برنامه");
        builder.setMessage("در صورت تمایل به خرید برنامه دکمه خرید و در غیر اینصورت به عنوان مهمان وارد برنامه شوید");
        builder.setPositiveButton("خرید برنامه", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                check(context);
            }
        });
        builder.setNegativeButton("ورود به عنوان مهمان", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                System.exit(0);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }*/

    public static void registerDialog(final Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("ثبت نام");
        builder.setMessage("شما در برنامه ثبت نام نکردید برای ثبت نام میتوانید از بخش ثبت نام استفاده کنید");
        builder.setPositiveButton("خب!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
