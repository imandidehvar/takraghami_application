package com.takraqami.packages.application;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.takraqami.packages.application.test.Database.MyDatabase;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by iman on 7/7/2017 AD.
 */

public class Azmoon_Karnameh extends AppCompatActivity
{
    ListView list;
    String konkur_id;
    private Dialog dialog;
    int avg;
    String lessons = "";
    String members,rotbe = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karnameh);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);
        ((RelativeLayout) findViewById(R.id.activity_test__tamrin)).setBackgroundColor(Color.parseColor("#672DA6"));

        list = (ListView) findViewById(R.id.percents);

        ((TextView) findViewById(R.id.text1)).setTypeface(MainActivity.font);
        ((TextView) findViewById(R.id.text1)).setText("رتبه شما");
        ((TextView) findViewById(R.id.rotbeh)).setTypeface(MainActivity.font);
        ((TextView) findViewById(R.id.rotbeh)).setText(calculate());
        list.setAdapter(new Adapter());
    }

    class Adapter extends BaseAdapter {
        @Override
        public int getCount() {
            return Karnameh_item.answered.size();
        }

        @Override
        public Object getItem(int i) {
            return Karnameh_item.answered.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View item = LayoutInflater.from(Azmoon_Karnameh.this).inflate(R.layout.karnameh_item,viewGroup,false);
            TextView tv = (TextView) item.findViewById(R.id.percent);
            ProgressBar percentbar = (ProgressBar) item.findViewById(R.id.percentbar);
            tv.setTypeface(MainActivity.font);
            tv.setText(Karnameh_item.answered.get(i).name + ": " + (int)(Karnameh_item.answered.get(i).percent) + "%");
            percentbar.setProgress((int)(Karnameh_item.answered.get(i).percent));
            return item;
        }
    }

    public String calculate()
    {
        int factors = 0;
        for (int i = 0; i < Karnameh_item.answered.size(); i++)
        {
            avg += ((Karnameh_item.answered.get(i).percent)*Karnameh_item.answered.get(i).factor);
            factors += Karnameh_item.answered.get(i).factor;
        }
        avg = avg/factors;
        String major = "", database = "";
        switch (IntroActivity.manager.getStudent_major_id())
        {
            case 1:
                major = "riazi";
                database = MyDatabase.RIAZI_DATABASE_NAME;
                break;
            case 2:
                major = "tajrobi";
                database = MyDatabase.TAJROBI_DATABASE_NAME;
                break;
            case 3:
                major = "ensani";
                database = MyDatabase.ENSANI_DATABASE_NAME;
                break;
        }

        Cursor c = new MyDatabase(this,database).getReadableDatabase()
                .query(major,new String[]{"rank"},"percent <= ?", new String[]{String.valueOf(avg)}
                        , null, null, "rank ASC");
        int first=30001;
        int second=30001;
        int i=0;
        if (c != null && c.moveToFirst()) {
            do{
                if(i==0){
                    first = c.getInt(0);
                } else {
                    second = c.getInt(0);
                    break;
                }
                i++;
            } while (c.moveToNext());
        }
        if(c!=null){
            c.close();
        }
        if(second!=30001){
            return String.valueOf(first)+" تا "+String.valueOf(second);
        }
        return " بالاتر از 30000";
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Karnameh_item.answered.clear();
        finish();
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, String btn1) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_soon, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setText(btn1);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
    }
}
