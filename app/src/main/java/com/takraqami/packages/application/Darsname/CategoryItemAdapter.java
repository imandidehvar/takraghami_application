package com.takraqami.packages.application.Darsname;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;
import com.takraqami.packages.application.classroom.ClassActivity;

import java.util.List;

/**
 * Created by Majid Ettehadi on 09/14/2017.
 */

public class CategoryItemAdapter extends ArrayAdapter<CategoryItem> {

    final String URL = "http://takraqami.ir/Application/Get_Query.php";

    public CategoryItemAdapter(Context context, int resource, List<CategoryItem> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.category_view, null);
        }

        final CategoryItem item = getItem(position);

        if (item != null) {
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView number = (TextView) v.findViewById(R.id.number);

            title.setText(item.name);
            title.setTypeface(MainActivity.font);
            number.setText(item.number+" فایل");
            number.setTypeface(MainActivity.font);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(),DarsActivity.class);
                    intent.putExtra("id",item.id);
                    intent.putExtra("name",item.name);
                    intent.putExtra("major",item.major);
                    intent.putExtra("number",item.number);
                    getContext().startActivity(intent);
                }
            });
        }

        return v;
    }

}
