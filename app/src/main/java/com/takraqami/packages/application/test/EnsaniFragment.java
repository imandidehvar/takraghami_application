package com.takraqami.packages.application.test;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.Database.MyDatabase;


public class EnsaniFragment extends FieldFragment {

    TextView riaziText;
    SeekBar riaziBar;

    TextView eghtesadText;
    SeekBar eghtesadBar;

    TextView farsi2Text;
    SeekBar farsi2Bar;

    TextView arabi2Text;
    SeekBar arabi2Bar;

    TextView tarikhText;
    SeekBar tarikhBar;

    TextView oloomText;
    SeekBar oloomBar;

    TextView manteghText;
    SeekBar manteghBar;

    TextView ravanText;
    SeekBar ravanBar;

    public EnsaniFragment() {}

    public static EnsaniFragment newInstance() {
        return new EnsaniFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ensani, container, false);

        farsiText = (TextView) v.findViewById(R.id.farsiText);
        farsiBar = (SeekBar) v.findViewById(R.id.farsiBar);
        farsiBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(farsiText.getText().toString().contains(":")){
                    String first = farsiText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    farsiText.setText(first+second);
                } else{
                    farsiText.setText(farsiText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        diniText = (TextView) v.findViewById(R.id.diniText);
        diniBar = (SeekBar) v.findViewById(R.id.diniBar);
        diniBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(diniText.getText().toString().contains(":")){
                    String first = diniText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    diniText.setText(first+second);
                } else{
                    diniText.setText(diniText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        arabiText = (TextView) v.findViewById(R.id.arabiText);
        arabiBar = (SeekBar) v.findViewById(R.id.arabiBar);
        arabiBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(arabiText.getText().toString().contains(":")){
                    String first = arabiText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    arabiText.setText(first+second);
                } else{
                    arabiText.setText(arabiText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        zabanText = (TextView) v.findViewById(R.id.zabanText);
        zabanBar = (SeekBar) v.findViewById(R.id.zabanBar);
        zabanBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(zabanText.getText().toString().contains(":")){
                    String first = zabanText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    zabanText.setText(first+second);
                } else{
                    zabanText.setText(zabanText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        riaziText = (TextView) v.findViewById(R.id.riaziText);
        riaziBar = (SeekBar) v.findViewById(R.id.riaziBar);
        riaziBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(riaziText.getText().toString().contains(":")){
                    String first = riaziText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    riaziText.setText(first+second);
                } else{
                    riaziText.setText(riaziText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        eghtesadText = (TextView) v.findViewById(R.id.eghtesadText);
        eghtesadBar = (SeekBar) v.findViewById(R.id.eghtesadBar);
        eghtesadBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(eghtesadText.getText().toString().contains(":")){
                    String first = eghtesadText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    eghtesadText.setText(first+second);
                } else{
                    eghtesadText.setText(eghtesadText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        farsi2Text = (TextView) v.findViewById(R.id.farsi2Text);
        farsi2Bar = (SeekBar) v.findViewById(R.id.farsi2Bar);
        farsi2Bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(farsi2Text.getText().toString().contains(":")){
                    String first = farsi2Text.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    farsi2Text.setText(first+second);
                } else{
                    farsi2Text.setText(farsi2Text.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        arabi2Text = (TextView) v.findViewById(R.id.arabi2Text);
        arabi2Bar = (SeekBar) v.findViewById(R.id.arabi2Bar);
        arabi2Bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(arabi2Text.getText().toString().contains(":")){
                    String first = arabi2Text.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    arabi2Text.setText(first+second);
                } else{
                    arabi2Text.setText(arabi2Text.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        tarikhText = (TextView) v.findViewById(R.id.tarikhText);
        tarikhBar = (SeekBar) v.findViewById(R.id.tarikhBar);
        tarikhBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(tarikhText.getText().toString().contains(":")){
                    String first = tarikhText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    tarikhText.setText(first+second);
                } else{
                    tarikhText.setText(tarikhText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        oloomText = (TextView) v.findViewById(R.id.oloomText);
        oloomBar = (SeekBar) v.findViewById(R.id.oloomBar);
        oloomBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(oloomText.getText().toString().contains(":")){
                    String first = oloomText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    oloomText.setText(first+second);
                } else{
                    oloomText.setText(oloomText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        manteghText = (TextView) v.findViewById(R.id.manteghText);
        manteghBar = (SeekBar) v.findViewById(R.id.manteghBar);
        manteghBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(manteghText.getText().toString().contains(":")){
                    String first = manteghText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    manteghText.setText(first+second);
                } else{
                    manteghText.setText(manteghText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ravanText = (TextView) v.findViewById(R.id.ravanText);
        ravanBar = (SeekBar) v.findViewById(R.id.ravanBar);
        ravanBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(ravanText.getText().toString().contains(":")){
                    String first = ravanText.getText().toString().split(":")[0];
                    String second = ": "+(i-33)+"%";
                    ravanText.setText(first+second);
                } else{
                    ravanText.setText(ravanText.getText().toString()+": "+(i-33)+"%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return v;
    }

    @Override
    public String calculate(){
        double avg = ((farsiBar.getProgress()-33)*4 +
                (diniBar.getProgress()-33)*3 +
                (arabiBar.getProgress()-33)*2 +
                (zabanBar.getProgress()-33)*2 +
                (riaziBar.getProgress()-33)*2 +
                (eghtesadBar.getProgress()-33) +
                (arabi2Bar.getProgress()-33)*4 +
                (tarikhBar.getProgress()-33) +
                (oloomBar.getProgress()-33) +
                (manteghBar.getProgress()-33)*3 +
                (ravanBar.getProgress()-33) +
                (farsi2Bar.getProgress()-33)*4)/28 ;
        Cursor c = new MyDatabase(getContext(),MyDatabase.ENSANI_DATABASE_NAME).getReadableDatabase()
                .query("ensani",new String[]{"rank"},"percent <= ?", new String[]{String.valueOf(avg)}
                        , null, null, "rank ASC");
        int first=0;
        int second=25000;
        int i=0;
        if (c != null && c.moveToFirst()) {
            do{
                if(i==0){
                    first = c.getInt(0);
                } else {
                    second = c.getInt(0);
                    break;
                }
                i++;
            } while (c.moveToNext());
        }
        if(c!=null){
            c.close();
        }
        if(second!=25000){
            return "در منطقه یک (احتمالا) بین "+String.valueOf(first)+" تا "+String.valueOf(second);
        }
        return "در منطقه یک (احتمالا) بالاتر از 25000";
    }

}
