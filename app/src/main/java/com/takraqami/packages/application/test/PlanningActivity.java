package com.takraqami.packages.application.test;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.takraqami.packages.application.ActivitySplash;
import com.takraqami.packages.application.BuyIntent;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.Moshaver;
import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.Calendar.CivilDate;
import com.takraqami.packages.application.test.Calendar.DateConverter;
import com.takraqami.packages.application.test.Calendar.PersianDate;
import com.takraqami.packages.application.test.Database.DBHelper;

import java.util.ArrayList;

public class PlanningActivity extends AppCompatActivity {

    public final String[] months = {"","فروردین","اردیبهشت","خرداد","تیر","مرداد",
            "شهریور","مهر","آبان","آذر","دی","بهمن","اسفند"};

    TextView todayText;
    TextView monthTitle;
    TextView yearTitle;
    ImageButton next;
    ImageButton prev;
    TextView eventListTitle;
    RecyclerView eventList;
    Button addEvent;

    PersianDate today;
    DBHelper dbHelper;
    PersianDate selectedDate;
    EventListAdapter adapter;
    CalendarHandler handler;

    Dialog dialog;

    int month;
    int year;

    AlertDialog dialogV;

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);

        if(IntroActivity.manager.getAccount_mode().equals("Guest"))
        {
            dialog("محدودیت نسخه مهمان", "جهت استفاده از تمامی امکانات برنامه لطفا ابتدا در برنامه ثبت نام کنید", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    },
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(PlanningActivity.this, com.takraqami.packages.application.MainActivity.class));
                            finish();
                            ActivitySplash.activity.finish();
                        }
                    }, "بازگشت", "ثبت نام در برنامه");
        }
        else if(IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered"))
        {
            dialog("محدودیت نسخه", "جهت استفاده از تمامی امکانات برنامه لطفا نسخه کامل را خریداری نمایید", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    },
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(PlanningActivity.this, BuyIntent.class));
                        }
                    }, "بازگشت", "خرید نسخه کامل");
        }

        todayText = (TextView) findViewById(R.id.today);
        yearTitle = (TextView) findViewById(R.id.yearTitle);
        monthTitle = (TextView) findViewById(R.id.monthTitle);
        next = (ImageButton) findViewById(R.id.next);
        prev = (ImageButton) findViewById(R.id.prev);
        eventListTitle = (TextView) findViewById(R.id.eventListTitle);
        eventList = (RecyclerView) findViewById(R.id.eventList);
        addEvent = (Button) findViewById(R.id.addNewEvent);

        eventList.setHasFixedSize(true);
        eventList.setLayoutManager(new LinearLayoutManager(this));
        eventList.setItemAnimator(new DefaultItemAnimator());

        today = DateConverter.civilToPersian(new CivilDate());
        dbHelper = new DBHelper(this);
        handler=new CalendarHandler(this,R.id.calendarGrid);
        handler.drawCalendar(today.getYear(),today.getMonth());
        year = today.getYear();
        month = today.getMonth();

        next.setOnClickListener(new NextClickListener());
        prev.setOnClickListener(new PrevClickListener());

        initToday();
        initMonth(today.getMonth());
        initYear(today.getYear());
        selectedDate=today.clone();
    }

    @Override
    public void onResume(){
        super.onResume();
        initEvent(selectedDate);
    }

    public void initToday(){
        String todayString = "امروز: "+String.valueOf(today.getDayOfMonth())+" ";
        todayString=todayString+months[today.getMonth()]+" ";
        todayString=todayString+String.valueOf(today.getYear());
        todayText.setText(todayString);
    }

    public void initMonth(int month){
        monthTitle.setText(months[month]);
    }

    public void initYear(int year){
        yearTitle.setText(String.valueOf(year));
    }

    public void initEvent(PersianDate date){

        //init
        CivilDate convertDate = DateConverter.persianToCivil(date);
        String year = String.valueOf(convertDate.getYear());
        String month = String.valueOf(convertDate.getMonth());
        if(month.length()==1){
            month="0"+month;
        }
        String day = String.valueOf(convertDate.getDayOfMonth());
        if(day.length()==1){
            day="0"+day;
        }

        //query
        Cursor c = dbHelper.selectQry("SELECT * FROM plan WHERE "+DBHelper.PlanColumns.FROM+" >= '"
                +year+"-"+month+"-"+day+" 00:00:00' AND "+DBHelper.PlanColumns.FROM+" <= '"
                +year+"-"+month+"-"+day+" 23:59:59' ORDER BY "+DBHelper.PlanColumns.FROM+" ASC");
        ArrayList<Event> events = new ArrayList<>();
        if (c != null && c.moveToFirst()){
            do{
                events.add(new Event(c.getInt(0),c.getString(1),c.getString(2),c.getString(3)
                        ,c.getString(4),c.getInt(5),c.getInt(6)));
            } while (c.moveToNext());
        }
        if(c!=null){
            c.close();
        }
        if(events.isEmpty()){
            eventListTitle.setText("شما برنامه ای برای این روز ندارید!");
        } else {
            eventListTitle.setText("برنامه های روز انتخاب شده:");
        }
        adapter = new EventListAdapter(R.layout.event_row, this, events);
        eventList.setAdapter(adapter);
    }

    public void addEventOCL(View v){
        CivilDate convertDate=DateConverter.persianToCivil(selectedDate);
        String year = String.valueOf(convertDate.getYear());
        String month = String.valueOf(convertDate.getMonth());
        if(month.length()==1){
            month="0"+month;
        }
        String day = String.valueOf(convertDate.getDayOfMonth());
        if(day.length()==1){
            day="0"+day;
        }
        Intent intent = new Intent(this, AddEventActivity.class);
        intent.putExtra(AddEventActivity.EXTRA_DATE,year+"-"+month+"-"+day);
        startActivity(intent);
    }

    private class NextClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if(month==12){
                month=1;
                year++;
            } else {
                month++;
            }
            initMonth(month);
            initYear(year);
            handler.drawCalendar(year,month);
        }
    }

    private class PrevClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if(month==1){
                month=12;
                year--;
            } else {
                month--;
            }
            initMonth(month);
            initYear(year);
            handler.drawCalendar(year,month);
        }
    }

}
