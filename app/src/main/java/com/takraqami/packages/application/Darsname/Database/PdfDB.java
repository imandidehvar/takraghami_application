package com.takraqami.packages.application.Darsname.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Majid Ettehadi on 2017.
 */

public class PdfDB extends SQLiteOpenHelper {

    public static class PdfColumns{
        public static String ID = "id";
        public static String CONTENT = "content";
    }

    private static final String DB_NAME = "pdfdb";
    private static final int DB_VER = 1;

    public PdfDB(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS" +
                " pdf ( " +
                PdfColumns.ID+" TEXT, " +
                PdfColumns.CONTENT+" TEXT " +
                " );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS pdf");
        onCreate(db);
    }

    public boolean doQry(String sql) {
        try {
            this.getWritableDatabase().execSQL(sql);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Cursor selectQry(String sql) {
        try {
            return this.getWritableDatabase().rawQuery(sql, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
