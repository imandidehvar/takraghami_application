package com.takraqami.packages.application.test;

import android.app.Activity;
import android.content.Context;
import android.widget.GridView;

import com.takraqami.packages.application.test.Calendar.CivilDate;
import com.takraqami.packages.application.test.Calendar.DateConverter;
import com.takraqami.packages.application.test.Calendar.Day;
import com.takraqami.packages.application.test.Calendar.PersianDate;

import java.util.LinkedList;

/**
 * Created by etteh on 5/8/2017.
 */

public class CalendarHandler {

    Context context;
    int viewId;

    public CalendarHandler(Context context, int viewId){
        this.context=context;
        this.viewId=viewId;
    }

    public void drawCalendar(int year, int month){
        LinkedList<Day> days = new LinkedList<>();
        int firstDay = getFirstDayOfMonth(year,month);
        for(int i=0;i<42;i++){
            if(i<8 && i<firstDay){
                days.add(new Day(""));
            } else if((i-firstDay+1)<=29){
                Day day = new Day(String.valueOf(i-firstDay+1));
                if(isHoliday(year,month,i-firstDay+1)){
                    day.holiday=true;
                }
                if(isToday(year,month,i-firstDay+1)){
                    day.today=true;
                }
                day.date=new PersianDate(year,month,i-firstDay+1);
                days.add(day);
            } else if((i-firstDay+1)==30 && month<12){
                Day day = new Day(String.valueOf(i-firstDay+1));
                if(isHoliday(year,month,i-firstDay+1)){
                    day.holiday=true;
                }
                if(isToday(year,month,i-firstDay+1)){
                    day.today=true;
                }
                day.date=new PersianDate(year,month,i-firstDay+1);
                days.add(day);
            }else if(month<7){
                Day day = new Day(String.valueOf(i-firstDay+1));
                if(isHoliday(year,month,i-firstDay+1)){
                    day.holiday=true;
                }
                if(isToday(year,month,i-firstDay+1)){
                    day.today=true;
                }
                day.date=new PersianDate(year,month,i-firstDay+1);
                days.add(day);
                break;
            } else {
                break;
            }
        }
        CalendarGridAdapter adapter = new CalendarGridAdapter(context,days);
        GridView grid = ((GridView)((Activity) context).findViewById(viewId));
        grid.setNumColumns(7);
        grid.setAdapter(adapter);
    }

    private int getFirstDayOfMonth(int year, int month){
        CivilDate convertDate = DateConverter.persianToCivil(new PersianDate(year,month,1));
        return convertDate.getDayOfWeek()%7;
    }

    private boolean isHoliday(int year, int month, int day){
        CivilDate convertDate = DateConverter.persianToCivil(new PersianDate(year,month,day));
        return (convertDate.getDayOfWeek()==6);
    }

    private boolean isToday(int year, int month, int day){
        CivilDate convertDate = DateConverter.persianToCivil(new PersianDate(year,month,day));
        return (convertDate.equals(new CivilDate()));
    }

}
