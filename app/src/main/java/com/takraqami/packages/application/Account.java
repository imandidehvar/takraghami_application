package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Iman on 4/13/2017.
 */

public class Account
{

    private String ID;
    private String family;
    private String phone;
    private String major;
    private String reciption;
    private String code;

    public String getID() {
        return ID;
    }

    public String getFamily() {
        return family;
    }

    public String getPhone() {
        return phone;
    }

    public String getMajor() {
        return major;
    }

    public String getReciption() {
        return reciption;
    }

    public String getCode() {
        return code;
    }

    public Account(JSONArray array) {
        try {
                this.ID = array.get(0).toString();
                this.family = array.get(1).toString();
                this.phone = array.get(2).toString();
                this.major = array.get(3).toString();
                this.reciption = array.get(4).toString();
                this.code = array.get(5).toString();
        }
        catch (JSONException e){e.printStackTrace();}
    }
    public Account(String[] fields)
    {
        this.family = fields[0].toString();
        this.phone = fields[1].toString();
        this.major = fields[2].toString();
    }

}
