package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MaghalehList extends AppCompatActivity {

    public ArrayList<Maghleh> maghaleha;
    ListView list;
    TextView notfound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maghaleh_list);
        maghaleha = new ArrayList<Maghleh>();

        ((TextView) findViewById(R.id.txttitle)).setTypeface(MainActivity.font);

        list = (ListView) findViewById(R.id.list);
        notfound = (TextView) findViewById(R.id.notfound);
        new GetData().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()));
    }

    class Adapter extends BaseAdapter {

        CardView card;
        TextView category, date, title, txtlike, txtdislike;
        LinearLayout likebtn, dislikebtn;
        ImageView picture;
        @Override
        public int getCount() {
            return maghaleha.size();
        }

        @Override
        public Object getItem(int position) {
            return maghaleha.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(MaghalehList.this).inflate(R.layout.maghaleh_item, parent, false);
            card = (CardView) view.findViewById(R.id.card);
            category = (TextView) view.findViewById(R.id.category);
            category.setTypeface(MainActivity.font);
            date = (TextView) view.findViewById(R.id.date);
            date.setTypeface(MainActivity.font);
            title = (TextView) view.findViewById(R.id.title);
            title.setTypeface(MainActivity.font);
            txtlike = (TextView) view.findViewById(R.id.liketext);
            txtlike.setTypeface(MainActivity.font);
            txtdislike = (TextView) view.findViewById(R.id.disliketext);
            txtdislike.setTypeface(MainActivity.font);
            likebtn = (LinearLayout) view.findViewById(R.id.like);
            dislikebtn = (LinearLayout) view.findViewById(R.id.dislike);
            picture = (ImageView) view.findViewById(R.id.picture);

            title.setText(maghaleha.get(position).getTitle().toString());
            date.setText(maghaleha.get(position).getDate());
            category.setText(maghaleha.get(position).getCategory());
            txtdislike.setText(maghaleha.get(position).getDislikes());
            txtlike.setText(maghaleha.get(position).getLikes());
            Picasso.with(MaghalehList.this).load(maghaleha.get(position).getPicture()).into(picture);

            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(MaghalehList.this,MaghalehShow.class);
                    intent.putExtra("maghaleh_id",maghaleha.get(position).getId());
                    startActivity(intent);
                }
            });

            return view;
        }
    }

    public class GetData extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MaghalehList.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(maghaleha.size() <= 0)
            {
                notfound.setVisibility(View.VISIBLE);
            }
            else
            {
                Adapter adapter = new Adapter();
                list.setAdapter(adapter);
            }
            dialog.dismiss();
        }
    }

    private void getData(String major) {
        try {
            URL url = new URL("http://takraqami.ir/Application/Get_Maghaleh_List.php");
            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(major,"UTF-8");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Log.d("Test",stringBuilder.toString());
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("maghaleh");
            for (int i = 0; i < data.length(); i++) {
                JSONArray object = data.getJSONArray(i);
                maghaleha.add(new Maghleh(object));
                Log.d("Test", String.valueOf(maghaleha.size()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class ConfigViewer extends RecyclerView.ViewHolder {

        public ConfigViewer(View view) {
            super(view);

        }
    }
}
