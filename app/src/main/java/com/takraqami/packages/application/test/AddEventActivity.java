package com.takraqami.packages.application.test;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.Database.DBHelper;
import com.mohamadamin.persianmaterialdatetimepicker.time.RadialPickerLayout;
import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddEventActivity extends AppCompatActivity {

    public static final String EXTRA_DATE = "date_extra";
    public static final String EXTRA_ID = "id_extra";

    EditText title;
    LinearLayout fromLayout;
    TextView from;
    LinearLayout untilLayout;
    TextView until;
    CheckBox notification;
    EditText description;
    Button registerEvent;

    String date;
    String fromDate;
    String untilDate;

    DBHelper dbHelper;
    Event event;
    int mode=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        title = (EditText) findViewById(R.id.title);
        fromLayout = (LinearLayout) findViewById(R.id.fromLayout);
        from = (TextView) findViewById(R.id.from);
        untilLayout = (LinearLayout) findViewById(R.id.untilLayout);
        until = (TextView) findViewById(R.id.until);
        notification = (CheckBox) findViewById(R.id.notification);
        description = (EditText) findViewById(R.id.description);
        registerEvent = (Button) findViewById(R.id.registerEvent);

        dbHelper = new DBHelper(this);

        if(getIntent().hasExtra(EXTRA_ID)){
            fillInfo(getIntent().getIntExtra(EXTRA_ID,0));
        }
        if(getIntent().hasExtra(EXTRA_DATE)){
            date = getIntent().getStringExtra(EXTRA_DATE);
        }

        initLayout();
    }

    private void fillInfo(int id){
        Cursor c = dbHelper.selectQry("SELECT * FROM plan WHERE "+DBHelper.PlanColumns.ID+" = "+String.valueOf(id));
        if (c != null && c.moveToFirst()){
            event = new Event(c.getInt(0),c.getString(1),c.getString(2),c.getString(3)
                    ,c.getString(4),c.getInt(5),c.getInt(6));
        }
        if(c!=null){
            c.close();
        }
        title.setText(event.title);
        date=event.from.split(" ")[0];
        fromDate = event.from;
        from.setText(event.from.split(" ")[1].substring(0,event.from.split(" ")[1].length()-3));
        untilDate = event.until;
        until.setText(event.until.split(" ")[1].substring(0,event.from.split(" ")[1].length()-3));
        if(event.notification>0){
            notification.setChecked(true);
        }
        description.setText(event.description);
        mode=1;
    }

    private void initLayout(){

        //from
        fromLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                        String h,m;
                        if(hourOfDay<10){
                            h="0"+String.valueOf(hourOfDay);
                        } else {
                            h=String.valueOf(hourOfDay);
                        }
                        if(minute<10){
                            m="0"+String.valueOf(minute);
                        } else {
                            m=String.valueOf(minute);
                        }
                        fromDate = date+" "+h+":"+m+":00";
                        from.setText(h+":"+m);
                    }
                },6,0,true).show(getFragmentManager(),"TimePiker");
            }
        });

        //until
        untilLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                        String h,m;
                        if(hourOfDay<10){
                            h="0"+String.valueOf(hourOfDay);
                        } else {
                            h=String.valueOf(hourOfDay);
                        }
                        if(minute<10){
                            m="0"+String.valueOf(minute);
                        } else {
                            m=String.valueOf(minute);
                        }
                        untilDate = date+" "+h+":"+m+":00";
                        until.setText(h+":"+m);
                    }
                },6,0,true).show(getFragmentManager(),"TimePiker");
            }
        });

        //button
        registerEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //init
                String titleStr=title.getText().toString();
                String descriptionStr=description.getText().toString();
                String notificationStr="0";
                if(titleStr.isEmpty()){
                    Toast.makeText(AddEventActivity.this,"عنوان برنامه خالی است!",Toast.LENGTH_LONG).show();
                    return;
                }
                if(fromDate==null){
                    Toast.makeText(AddEventActivity.this,"ساعت شروع برنامه خالی است!",Toast.LENGTH_LONG).show();
                    return;
                }
                if(untilDate==null){
                    Toast.makeText(AddEventActivity.this,"ساعت پایان برنامه خالی است!",Toast.LENGTH_LONG).show();
                    return;
                }
                try{
                    Date tmpFrom=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fromDate);
                    Date tmpUntil=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(untilDate);
                    if(tmpFrom.after(tmpUntil)){
                        Toast.makeText(AddEventActivity.this,"ساعت پایان قبل از ساعت شروع است!",Toast.LENGTH_LONG).show();
                        return;
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    return;
                }
                if(notification.isChecked()){
                    notificationStr="1";
                }

                //action
                if(mode==0){
                    dbHelper.doQry("INSERT INTO plan VALUES (NULL,'"+titleStr+"','"+fromDate+"','"+untilDate
                            +"','"+descriptionStr+"',"+String.valueOf(Event.STATUS_NORMAL)+","+notificationStr+")");
                    Toast.makeText(AddEventActivity.this,"ّبرنامه با موفقیت ثبت شد.",Toast.LENGTH_LONG).show();
                    if(notification.isChecked()){
                        handleNotification(getLastId());
                    }
                } else {
                    dbHelper.doQry("UPDATE plan SET "+DBHelper.PlanColumns.TITLE+" = '"+titleStr+"' , "
                            +DBHelper.PlanColumns.FROM+" = '"+fromDate+"' , "+DBHelper.PlanColumns.UNTIL+" = '"
                            +untilDate+"' , "+DBHelper.PlanColumns.DESCRIPTION+" = '"+descriptionStr+"' , "
                            +DBHelper.PlanColumns.NOTIFICATION+" = "+notificationStr+" WHERE "
                            +DBHelper.PlanColumns.ID+" = "+event.id);
                    Toast.makeText(AddEventActivity.this,"ّبرنامه با موفقیت به روز شد.",Toast.LENGTH_LONG).show();
                    if(notification.isChecked()){
                        handleNotification(event.id);
                    } else if(event.notification>0){
                        cancelNotification(event.id);
                    }
                }
                finish();
            }
        });
    }

    private int getLastId(){
        int res = 0;
        Cursor c = dbHelper.selectQry("SELECT MAX(plan_id) AS tmp_id FROM plan");
        if (c != null && c.moveToFirst()){
            res = c.getInt(0);
        }
        if(c!=null){
            c.close();
        }
        return res;
    }

    private void handleNotification(int reqCode) {
        int y = Integer.parseInt(fromDate.split("-")[0]);
        int M = Integer.parseInt(fromDate.split("-")[1])-1;
        int d = Integer.parseInt(fromDate.split("-")[2].split(" ")[0]);
        int h = Integer.parseInt(fromDate.split(" ")[1].split(":")[0]);
        int m = Integer.parseInt(fromDate.split(" ")[1].split(":")[1]);
        Calendar cal = Calendar.getInstance();
        cal.set(y,M,d,h,m);
        cal.set(Calendar.SECOND,0);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        alarmIntent.putExtra(AlarmReceiver.EXTRA_MSG,title.getText().toString());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, reqCode, alarmIntent
                , PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),pendingIntent);
    }

    private void cancelNotification(int reqCode) {
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        alarmIntent.putExtra(AlarmReceiver.EXTRA_MSG,title.getText().toString());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, reqCode, alarmIntent
                , PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}
