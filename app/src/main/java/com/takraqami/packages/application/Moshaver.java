package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Iman on 4/30/2017.
 */

public class Moshaver extends AppCompatActivity
{
    TextView tvHaveData;
    RecyclerView list;
    Button addMoshaver;
    Dialog dialog;
    ArrayList<Counseling> questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moshaver);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        tvHaveData = (TextView) findViewById(R.id.tvHaveData);
        tvHaveData.setTypeface(MainActivity.font);
        list = (RecyclerView) findViewById(R.id.lessons_list);
        addMoshaver = (Button) findViewById(R.id.newMoshavere);
        addMoshaver.setTypeface(MainActivity.font);
        new GetData().execute(IntroActivity.manager.getStudent_id());
        addMoshaver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (IntroActivity.manager.getAccount_mode().equals("Guest")) {
                        dialog("محدودیت نسخه", "برای دسترسی به مشاوره آنلاین لطفا ابتدا در برنامه ثبت نام کنید", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(Moshaver.this, MainActivity.class));
                                        finish();
                                        ActivitySplash.activity.finish();
                                    }
                                }, "بازگشت", "ثبت نام");


                    }

                else if(IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered"))
                {
                    if(questions.size() < 1)
                    {
                        Intent intent = new Intent(Moshaver.this,Activity_Moshaver_Info.class);
                        intent.putExtra("mode","send");
                        intent.putExtra("user",IntroActivity.manager.getStudent_id());
                        startActivity(intent);
                    }
                    else
                    {
/*                        AlertDialog.Builder builder = new AlertDialog.Builder(Moshaver.this);
                        builder.setTitle("خرید برنامه");
                        builder.setMessage("درنسخه معمولی فقط مجاز به ارسال یک سوال میباشید. برای خرید برنامه از منوی کشویی صفحه اصلی استفاده کنید");
                        builder.setPositiveButton("خب!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                finish();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();*/

                        dialog("محدودیت نسخه", "درنسخه معمولی فقط مجاز به ارسال یک سوال میباشید. لطفا نسخه کامل را خریداری نمایید", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(Moshaver.this, BuyIntent.class));
                                    }
                                }, "بازگشت", "خرید نسخه کامل");


                    }
                }
                else
                {
                    Intent intent = new Intent(Moshaver.this,Activity_Moshaver_Info.class);
                    intent.putExtra("mode","send");
                    intent.putExtra("user",IntroActivity.manager.getStudent_id());
                    startActivity(intent);
                }
            }
        });
    }


    class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(Moshaver.this).inflate(R.layout.moshaver_list_item,parent,false);
            return new ConfigViewer(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position)
        {
            ConfigViewer viewer = (ConfigViewer) holder;
            viewer.title.setText(questions.get(position).getTitle());
            if(questions.get(position).getFlag().equals("0"))
            {
                viewer.color.setBackgroundResource(R.drawable.moshaver_question_no_answered);
            }
            else
            {
                viewer.color.setBackgroundResource(R.drawable.moshaver_question_answered);
            }
            viewer.enter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Moshaver.this, Activity_Moshaver_Info.class);
                    Counseling.counseling = questions.get(position);
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return questions.size();
        }
    }

    private class ConfigViewer extends RecyclerView.ViewHolder
    {
        ImageView color;
        TextView title;
        ImageView enter;
        public ConfigViewer(View view)
        {
            super(view);
            color = (ImageView) view.findViewById(R.id.color);
            title = (TextView) view.findViewById(R.id.title);
            title.setTypeface(MainActivity.font);
            enter = (ImageView) view.findViewById(R.id.enter);
        }
    }


    public class GetData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Moshaver.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(questions.size()>0)
            {
                tvHaveData.setVisibility(View.INVISIBLE);
                Adapter adapter = new Adapter();
                list.setLayoutManager(new LinearLayoutManager(Moshaver.this,LinearLayoutManager.VERTICAL,false));
                list.setAdapter(adapter);
            }
            dialog.dismiss();
        }
    }

    private void getData(String string)
    {
        try
        {
            questions = new ArrayList<Counseling>();
            String req = URLEncoder.encode("user","utf-8") + "=" + URLEncoder.encode(string,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Get_Counseling.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("question");
            for(int i = 0; i < data.length();i++)
            {
                JSONArray object = data.getJSONArray(i);
                questions.add(new Counseling(object));
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

}
