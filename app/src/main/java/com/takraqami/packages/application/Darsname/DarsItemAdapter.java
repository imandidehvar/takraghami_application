package com.takraqami.packages.application.Darsname;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Priority;
import com.coolerfall.download.URLDownloader;
import com.squareup.picasso.Picasso;
import com.takraqami.packages.application.ActivitySplash;
import com.takraqami.packages.application.BuyIntent;
import com.takraqami.packages.application.Darsname.Database.PdfDB;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;
import com.takraqami.packages.application.classroom.PlayVideoActivity;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.takraqami.packages.application.MainActivity.context;

/**
 * Created by Majid Ettehadi on 09/14/2017.
 */

public class DarsItemAdapter extends ArrayAdapter<DarsItem> {

    public static final String FILE_STORAGE_PATH = "pdf";

    PdfDB db;
    DownloadManager downloadManager;
    Dialog dialog;

    public DarsItemAdapter(Context context, int resource, List<DarsItem> items) {
        super(context, resource, items);
        db = new PdfDB(context);
        downloadManager = new DownloadManager.Builder().context(context)
                .downloader(URLDownloader.create())
                .build();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.class_view, null);
        }

        final DarsItem item = getItem(position);

        if (item != null) {
            CircleImageView image = (CircleImageView) v.findViewById(R.id.image);
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView size = (TextView) v.findViewById(R.id.size);
            //TextView duration = (TextView) v.findViewById(R.id.duration);

            title.setText(item.title);
            title.setTypeface(MainActivity.font);
            size.setText("تعداد صفحات: " + item.page_count);
            size.setTypeface(MainActivity.font);

            try{
                //duration
                //duration.setText("زمان: "+getVideoDuration(item.content_location.replaceAll("\\s","%20")));

                //image
                //System.out.println("http://"+item.pic_location.replaceAll("\\s","%20"));
                Picasso.with(context).load("http://"+item.pic_location.replaceAll("\\s","%20"))
                        .placeholder(getContext().getResources().getDrawable(R.drawable.pdf))
                        .into(image);
            } catch (Exception e){
                e.printStackTrace();
            }


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(IntroActivity.manager.getAccount_mode().equals("Guest"))
                    {
                        dialog("محدودیت نسخه مهمان", "جهت استفاده از تمامی امکانات برنامه لطفا ابتدا در برنامه ثبت نام کنید", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ((DarsActivity) getContext()).finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getContext().startActivity(new Intent(getContext(), com.takraqami.packages.application.MainActivity.class));
                                        ((DarsActivity) getContext()).finish();
                                        ActivitySplash.activity.finish();
                                    }
                                }, "بازگشت", "ثبت نام در برنامه");
                    }
                    else if(IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered"))
                    {
                        dialog("محدودیت نسخه", "جهت استفاده از تمامی امکانات برنامه لطفا نسخه کامل را خریداری نمایید", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ((DarsActivity) getContext()).finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getContext().startActivity(new Intent(getContext(), BuyIntent.class));
                                    }
                                }, "بازگشت", "خرید نسخه کامل");
                    } else{
                        action(item);
                    }
                }
            });
        }

        return v;
    }


    public void action(final DarsItem darsItem){
        String path="";

        //query
        Cursor c = db.selectQry("SELECT content FROM pdf WHERE "
                + PdfDB.PdfColumns.ID+" = '"+darsItem.id+"'");
        if (c != null && c.moveToFirst()){
            if(!c.isNull(0)){
                path=c.getString(0);
            }
        }
        if(c!=null){
            c.close();
        }

        if(path.isEmpty() || !(new File(path)).exists()){
            if(availableInternet()){
                //show dialog
                final ProgressDialog dialog;
                dialog = new ProgressDialog(getContext());
                dialog.setMessage("در حال دانلود فایل "+darsItem.title);
                dialog.setIndeterminate(true);
                dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        downloadManager.cancelAll();
                    }
                });
                dialog.show();

                //send request
                DownloadRequest request = new DownloadRequest.Builder()
                        .url(darsItem.content_location.replaceAll("\\s","%20"))
                        .retryTime(5)
                        .retryInterval(2, TimeUnit.SECONDS)
                        .progressInterval(1, TimeUnit.SECONDS)
                        .priority(Priority.HIGH)
                        .destinationDirectory(getContext().getExternalFilesDir(null)+ File.separator+FILE_STORAGE_PATH)
                        .downloadCallback(new DownloadCallback() {

                            @Override
                            public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                                dialog.setIndeterminate(false);
                                dialog.setMax((int)totalBytes/1024);
                                dialog.setProgress((int)bytesWritten/1024);
                            }

                            @Override public void onSuccess(int downloadId, String filePath) {
                                db.doQry("INSERT INTO pdf (id,content) VALUES('"+darsItem.id+"','"+filePath+"')");
                                dialog.dismiss();
                                showFile(filePath);
                            }

                            @Override public void onFailure(int downloadId, int statusCode, String errMsg) {
                                dialog.dismiss();
                                //System.out.println(errMsg);
                                Toast.makeText(getContext(),R.string.download_failed,Toast.LENGTH_LONG).show();
                            }
                        })
                        .build();
                downloadManager.add(request);
            } else {
                Toast.makeText(getContext(),R.string.no_internet,Toast.LENGTH_LONG).show();
            }
        } else {
            showFile(path);
        }

    }

    public boolean availableInternet(){
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void showFile(String fpath){
        try{
            Uri path;
            File pdfFile = new File(fpath);
            if(Build.VERSION.SDK_INT >= 24){
                path = FileProvider.getUriForFile(getContext().getApplicationContext()
                        , getContext().getPackageName() + ".provider", pdfFile);
            } else{
                path = Uri.fromFile(pdfFile);
            }
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            getContext().startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(getContext(), R.string.no_pdf, Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(getContext(),  e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) ((DarsActivity) getContext()).findViewById(android.R.id.content);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

}
