package com.takraqami.packages.application.classroom;

import org.json.JSONObject;

/**
 * Created by Majid Ettehadi on 09/14/2017.
 */

public class ClassItem {

    String id;
    String title;
    String category;
    String size;
    String duration;
    String pic_location;
    String content_location;
    String hd_location;

    public ClassItem(String id, String title, String category, String size
            , String duration, String pic_location, String content_location, String hd_location){
        this.id=id;
        this.title=title;
        this.category=category;
        this.size=size;
        this.duration=duration;
        this.pic_location=pic_location;
        this.content_location=content_location;
        this.hd_location=hd_location;
    }

    public static ClassItem fromJson(JSONObject json){
        try{
            return new ClassItem(json.getString("id")
                    , json.getString("title")
                    , json.getString("category")
                    , json.getString("size")
                    , json.getString("duration")
                    , json.getString("pic_location")
                    , json.getString("content_location")
                    , json.getString("hd_location"));
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
