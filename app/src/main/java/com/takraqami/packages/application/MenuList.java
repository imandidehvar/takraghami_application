package com.takraqami.packages.application;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Iman on 4/29/2017.
 */

public class MenuList extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context context;
    private ViewGroup parent;

    private ArrayList<MenuItem> items;

    public void add(int icon,int color,String text,Class child)
    {
        items.add(new MenuItem(icon,color,text,child));
    }

    public MenuList(Context context)
    {
        this.context = context;
        items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        final View view = LayoutInflater.from(context).inflate(R.layout.menuitem,parent,false);
        return new ConfigViewer(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ConfigViewer viewer = (ConfigViewer) holder;
        viewer.menutext.setText(items.get(position).name);
        viewer.icon.setBackgroundResource(items.get(position).icon);
        viewer.color.setBackgroundColor(items.get(position).color);
        viewer.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,items.get(position).child);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    private class ConfigViewer extends RecyclerView.ViewHolder
    {
        private ImageView icon,color;
        private TextView menutext;
        private CardView cardView;
        public ConfigViewer(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.menuicon);
            color = (ImageView) itemView.findViewById(R.id.menucolor);
            menutext = (TextView) itemView.findViewById(R.id.menutext);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            menutext.setTypeface(MainActivity.font);
        }
    }

    class MenuItem
    {
        int icon;
        int color;
        String name;
        Class child;

        public MenuItem(int icon, int color, String name, Class child) {
            this.icon = icon;
            this.color = color;
            this.name = name;
            this.child = child;
        }
    }
}

