package com.takraqami.packages.application.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.takraqami.packages.application.R;

public class RankingActivity extends AppCompatActivity {

    Spinner spinner;
    Button calculate;
    FieldFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);


        spinner = (Spinner) findViewById(R.id.spinner);
        calculate = (Button) findViewById(R.id.calculate);
        initSpinner();
        initCalculate();
    }

    private void initSpinner(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.ranking_spinner_arrays, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 1:
                        calculate.setVisibility(View.VISIBLE);
                        fragment = RiaziFragment.newInstance();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout
                                , fragment).commit();
                        break;
                    case 2:
                        calculate.setVisibility(View.VISIBLE);
                        fragment = EnsaniFragment.newInstance();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout
                                , fragment).commit();
                        break;
                    case 3:
                        calculate.setVisibility(View.VISIBLE);
                        fragment = TajrobiFragment.newInstance();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout
                                , fragment).commit();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initCalculate(){
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RankingActivity.this,ShowRankActivity.class);
                intent.putExtra(ShowRankActivity.EXTRA_TEXT,fragment.calculate());
                startActivity(intent);
            }
        });
    }
}
