package com.takraqami.packages.application.test;

/**
 * Created by etteh on 5/7/2017.
 */

public class Event {

    public static final int STATUS_NORMAL=0;
    public static final int STATUS_DONE=1;
    public static final int STATUS_CANCEL=2;

    public int id;
    public String title;
    public String from;
    public String until;
    public String description;
    public int status;
    public int notification;

    public Event(int id, String title, String from, String until, String description, int status, int notification){
        this.id=id;
        this.title=title;
        this.from=from;
        this.until=until;
        this.description=description;
        this.status=status;
        this.notification=notification;
    }

}
