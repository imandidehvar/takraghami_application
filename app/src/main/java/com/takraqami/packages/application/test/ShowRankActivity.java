package com.takraqami.packages.application.test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.takraqami.packages.application.R;

public class ShowRankActivity extends AppCompatActivity {

    public static final String EXTRA_TEXT = "extra_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_rank);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        ((TextView) findViewById(R.id.text)).setText(getIntent().getStringExtra(EXTRA_TEXT));
    }
}
