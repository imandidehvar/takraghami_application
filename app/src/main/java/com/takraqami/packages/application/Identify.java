package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Identify extends AppCompatActivity {

    public static String phone, code, status, name, reciption;
    public static EditText codetxt;
    public static TextView timertxt, reSendSms;
    public static Button enter;
    private static final int interval = 1000;
    private static Handler handler;
    private static boolean secondSmsTry;
    private static String supportLink;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    public static Identify identify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        handler = new Handler();
        handler.postDelayed(runnable,interval);

        identify = this;

        phone = getIntent().getExtras().get("phone").toString();
        code = getIntent().getExtras().get("code").toString();

        secondSmsTry = false;
    }

    private static Runnable runnable = new Runnable(){
        public void run()
        {
            if(Integer.valueOf(timertxt.getText().toString()) > 0)
            {
                timertxt.setText(String.valueOf(Integer.valueOf(timertxt.getText().toString()) - 1));
                reSendSms.setEnabled(false);
                reSendSms.setTextColor(Color.parseColor("#fcb7b7"));
            }
            else
            {
                timertxt.setText("0");
                reSendSms.setEnabled(true);
                reSendSms.setTextColor(Color.parseColor("#ec2323"));
            }
            handler.postDelayed(this,interval);
        }
    };

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "کد تایید";
            }
            return null;
        }
    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment()
        {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = null;

            switch(getArguments().getInt(ARG_SECTION_NUMBER))
            {
                case 1:
                    rootView = inflater.inflate(R.layout.activity_identify,container,false);

                    //new GetIdentCode().execute(phone,code);

                    enter = (Button) rootView.findViewById(R.id.enter);
                    codetxt = (EditText) rootView.findViewById(R.id.code);
                    timertxt = (TextView) rootView.findViewById(R.id.timertxt);
                    reSendSms = (TextView) rootView.findViewById(R.id.resend);

                    //handler.postDelayed(Identify.runnable, interval);

                    /*reSendSms.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(!secondSmsTry)
                            {
                                //new GetIdentCode().execute(phone,code);
                                timertxt.setText("60");
                                reSendSms.setEnabled(false);
                                reSendSms.setVisibility(View.INVISIBLE);
                                handler.postDelayed(Identify.runnable, interval);
                                secondSmsTry = true;
                            }
                            else
                            {
                                reSendSms.setEnabled(false);
                                reSendSms.setVisibility(View.INVISIBLE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(identify);
                                builder.setTitle("خطا در ارسال");
                                builder.setMessage("اشکالی در ارسال پیامک تایید پیش امده. لطفا از گزینه ی ارسال به ایمیل استفاده کنید یا لحظاتی دیگر مچدد تلاش کنید");
                                builder.setNegativeButton("خب!", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
*/
                    enter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (codetxt.getText().toString().equals(code))
                            {
                                //new GetIdentCode().execute(phone,code,"true");
                                IntroActivity.manager.setAccountIdetified(true);
                                Intent intent = new Intent(identify, ActivitySplash.class);
                                identify.startActivity(intent);
                                identify.finish();
                            }
                            else
                            {
                                Toast.makeText(identify, "کد ورود صحیح نیست", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    reSendSms.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            supportLink = "";
                            new GetSupport().execute();
                        }
                    });

                    break;
            }

            return rootView;
        }
    }

    private static class GetSupport extends AsyncTask<Void,Void,Void>
    {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Identify.identify);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getSupport();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            if(supportLink.equals("null") || supportLink.equals("") || supportLink.equals(null))
            {
                Toast.makeText(Identify.identify,"درحال حاظر اوپراتور جهت پاسخگویی در دسترس نیست",Toast.LENGTH_LONG).show();
            }
            else
            {
                Intent telegram = new Intent(Intent.ACTION_VIEW , Uri.parse("https://t.me/takraghami_help"));
                identify.startActivity(telegram);
            }
        }
    }

    private static void getSupport()
    {
        try
        {

            URL url = new URL("http://www.takraqami.ir/Application/Get_Support.php");
            URLConnection connection = url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("link");
            JSONArray object = data.getJSONArray(0);
            supportLink = object.get(0).toString();
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}

    }
    boolean bstatus =false;
    @Override
    public void onBackPressed() {
        if(bstatus)
        {
            super.onBackPressed();
            finish();
        }
        else
        {
            Toast.makeText(identify,"جهت خروج مجدد کلید بازگشت را فشار دهید",Toast.LENGTH_LONG).show();
            bstatus = true;
        }
    }

    /*static class GetIdentCode extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        public GetIdentCode()
        {
            dialog = new ProgressDialog(identify);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("درحال ارسال کد تایید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getCode(strings[0],strings[1],strings[2]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            //IntroActivity.manager.setAccountIdetified(true);
            //Intent intent = new Intent(identify, ActivitySplash.class);
            //identify.startActivity(intent);
            //identify.finish();
        }
    }
        public static void getCode(String phone,String code,String status)
        {
            try
            {
                String req = URLEncoder.encode("phone","utf-8")+"="+URLEncoder.encode(phone,"UTF-8");
                req += "&" + URLEncoder.encode("code","utf-8")+"="+URLEncoder.encode(code,"UTF-8");
                req += "&" + URLEncoder.encode("status","utf-8")+"="+URLEncoder.encode(status,"UTF-8");
                URL url = new URL("http://www.takraqami.ir/Application/Identify.php");
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(req);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                String result = stringBuilder.toString();
                JSONObject json = new JSONObject(result);
                JSONArray data = json.getJSONArray("res");
            }
            catch (IOException e) {e.printStackTrace();}
            catch (JSONException e){e.printStackTrace();}
        }*/
}

