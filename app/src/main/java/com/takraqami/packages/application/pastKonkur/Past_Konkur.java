package com.takraqami.packages.application.pastKonkur;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Past_Konkur extends AppCompatActivity {

    ArrayList<konkurs> list = new ArrayList<konkurs>();
    RecyclerView layoutList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past__konkur);
        ((TextView) findViewById(R.id.txttitle)).setTypeface(MainActivity.font);
        new GetData().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()));
    }


    public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(Past_Konkur.this).inflate(R.layout.past_konkur_main_item,parent,false);
            return new ConfigViewer(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ConfigViewer view = (ConfigViewer) holder;
            view.title.setText(list.get(position).title.toString());
            view.item.setCardBackgroundColor(Color.parseColor("#33000000"));
            view.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Past_Konkur.this,Past_konkur_Lessons.class);
                    intent.putExtra("lessonId",list.get(position).id);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }


    private class ConfigViewer extends RecyclerView.ViewHolder {
        TextView title;
        CardView item;

        public ConfigViewer(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            title.setTypeface(MainActivity.font);
            item = (CardView) view.findViewById(R.id.item);
        }
    }

    public class GetData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Past_Konkur.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            layoutList = (RecyclerView) findViewById(R.id.lessons_list);
            layoutList.setLayoutManager(new LinearLayoutManager(Past_Konkur.this, LinearLayoutManager.VERTICAL, false));
            Adapter adapter = new Adapter();
            layoutList.setAdapter(adapter);
            dialog.dismiss();
        }
    }

    private void getData(String string)
    {
        try
        {
            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(string,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Get_Konkurs.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONArray json = new JSONArray(result);
            for(int i = 0; i < json.length();i++)
            {
                list.add(new konkurs(json.getJSONObject(i)));
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    class konkurs
    {
        int id;
        String title;

        public konkurs(JSONObject object)
        {
            try{
                this.id = object.getInt("id");
                this.title = object.getString("past_konkur_title");
            }
            catch (JSONException e)
            {e.printStackTrace();}
        }
    }
}
