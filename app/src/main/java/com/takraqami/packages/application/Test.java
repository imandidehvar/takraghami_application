package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Test extends AppCompatActivity {

    String major_id, book_id, subBook_id, chapter_id, page_title;
    TextView title, sahih, qalat;
    ImageView soal, javab, qNext;
    Button select1, select2, select3, select4, finish,reportV;
    ArrayList<Question> questions;
    ArrayList<Integer> selects;
    AlertDialog dialog;

    boolean status;
    Typeface font;
    int nazade;
    int record;
    boolean report;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        font = Typeface.createFromAsset(getAssets(),"fonts/nazanin.ttf");
        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        reportV = (Button) findViewById(R.id.report);
        title = (TextView) findViewById(R.id.title1);
        title.setTypeface(MainActivity.font);
        sahih = (TextView) findViewById(R.id.txtsahihc);
        sahih.setTypeface(MainActivity.font);
        qalat = (TextView) findViewById(R.id.txtqalatc);
        qalat.setTypeface(MainActivity.font);
        soal = (ImageView) findViewById(R.id.question_img);
        select1 = (Button) findViewById(R.id.select1);
        select1.setTypeface(font);
        select2 = (Button) findViewById(R.id.select2);
        select2.setTypeface(font);
        select3 = (Button) findViewById(R.id.select3);
        select3.setTypeface(font);
        select4 = (Button) findViewById(R.id.select4);
        select4.setTypeface(font);
        javab = (ImageView) findViewById(R.id.pasokh);
        qNext = (ImageView) findViewById(R.id.questionnext);
        finish = (Button) findViewById(R.id.finish);
        finish.setTypeface(MainActivity.font);

        Toast.makeText(this,"جهت مشاهده کامل سوال آنرا جابجا کنید",Toast.LENGTH_LONG).show();

        status = false;

        sahih.setText("0");
        qalat.setText("0");
        record = 0;

        major_id = String.valueOf(IntroActivity.manager.getStudent_major_id());
        book_id = getIntent().getExtras().get("book_id").toString();
        subBook_id = getIntent().getExtras().get("subbook_id").toString();
        chapter_id = getIntent().getExtras().get("chapter").toString();

        new GetData().execute(major_id, book_id, subBook_id, chapter_id);

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = "پایان سوالات " + Book.book.getBook_name();

                String message = "تعداد کل سوالات: " + record + "\n";
                message += "تعداد پاسخ های درست: " + sahih.getText() + "\n";
                message += "تعداد پاسخ های غلط: " + qalat.getText() + "\n";
                message += "تعداد سوالات بی پاسخ: " + nazade + "\n";

                dialog(title, message, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        }, "انتخاب درس", "ادامه ارزشیابی");
            }
        });

        reportV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SendReport().execute(questions.get(record).getId());

            }
        });

        qNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!status) {
                    nazade++;
                }
                if (record < questions.size() - 1) {
                    record++;
                    loadQuestion();
                } else {
                    String title = "پایان سوالات " + Book.book.getBook_name();

                    String message = "تعداد کل سوالات: " + questions.size() + "\n";
                    message += "تعداد پاسخ های درست: " + sahih.getText() + "\n";
                    message += "تعداد پاسخ های غلط: " + qalat.getText() + "\n";
                    message += "تعداد سوالات بی پاسخ: " + nazade + "\n";

                    dialog(title, message, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    record = 0;
                                    loadQuestion();
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);
                                }
                            }, "انتخاب درس", "شروع مجدد آزمون");
                }
            }
        });

        select1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selects.add(1);
                check("1");
            }
        });
        select2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selects.add(2);
                check("2");
            }
        });
        select3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selects.add(3);
                check("3");
            }
        });
        select4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selects.add(4);
                check("4");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void check(String s) {
        status = true;
        if (s.equals(questions.get(record).getTrueSelect()))
        {
            sahih.setText(String.valueOf(Integer.parseInt(sahih.getText().toString())+1));
            if (s.equals("1"))
            {
                select1.setBackgroundResource(R.drawable.select_true);
                select1.setTextColor(Color.WHITE);
            }
            else if (s.equals("2"))
            {
                select2.setBackgroundResource(R.drawable.select_true);
                select2.setTextColor(Color.WHITE);
            }
            else if (s.equals("3"))
            {
                select3.setBackgroundResource(R.drawable.select_true);
                select3.setTextColor(Color.WHITE);
            }
            else if (s.equals("4"))
            {
                select4.setBackgroundResource(R.drawable.select_true);
                select4.setTextColor(Color.WHITE);
            }
        }
        else
        {
            qalat.setText(String.valueOf(Integer.parseInt(qalat.getText().toString())+1));
            if (questions.get(record).getTrueSelect().equals("1"))
            {
                select1.setBackgroundResource(R.drawable.select_true);
                select1.setTextColor(Color.WHITE);
            }
            else if (questions.get(record).getTrueSelect().equals("2"))
            {
                select2.setBackgroundResource(R.drawable.select_true);
                select2.setTextColor(Color.WHITE);
            }
            else if (questions.get(record).getTrueSelect().equals("3"))
            {
                select3.setBackgroundResource(R.drawable.select_true);
                select3.setTextColor(Color.WHITE);
            }
            else if (questions.get(record).getTrueSelect().equals("4"))
            {
                select4.setBackgroundResource(R.drawable.select_true);
                select4.setTextColor(Color.WHITE);
            }

            if (s.equals("1"))
            {
                select1.setBackgroundResource(R.drawable.select_wrong);
                select1.setTextColor(Color.WHITE);
            }
            else if (s.equals("2"))
            {
                select2.setBackgroundResource(R.drawable.select_wrong);
                select2.setTextColor(Color.WHITE);
            }
            else if (s.equals("3"))
            {
                select3.setBackgroundResource(R.drawable.select_wrong);
                select3.setTextColor(Color.WHITE);
            }
            else if (s.equals("4"))
            {
                select4.setBackgroundResource(R.drawable.select_wrong);
                select4.setTextColor(Color.WHITE);
            }
        }
        Picasso.with(Test.this).load(questions.get(record).getTestAnswer()).into(javab);
        javab.setVisibility(View.VISIBLE);
        select1.setEnabled(false);
        select2.setEnabled(false);
        select3.setEnabled(false);
        select4.setEnabled(false);
        try{Thread.sleep(100);} catch (InterruptedException e){e.printStackTrace();}
        ((HorizontalScrollView)findViewById(R.id.javab)).postDelayed(runnable,1000);

    }

    public class GetData extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Test.this);
            dialog.setMessage("در حال آماده سازی آزمون");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0], strings[1], strings[2], strings[3]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            selects = new ArrayList<Integer>();
            loadQuestion();
            dialog.dismiss();
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(!((HorizontalScrollView)findViewById(R.id.question)).fullScroll(HorizontalScrollView.FOCUS_RIGHT))
            {
                ((HorizontalScrollView)findViewById(R.id.question)).fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
            else
            {
                ((HorizontalScrollView)findViewById(R.id.question)).postDelayed(runnable,40000);
            }

            if(!((HorizontalScrollView)findViewById(R.id.javab)).fullScroll(HorizontalScrollView.FOCUS_RIGHT))
            {
                ((HorizontalScrollView)findViewById(R.id.javab)).fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
            else
            {
                ((HorizontalScrollView)findViewById(R.id.javab)).postDelayed(runnable,40000);
            }
        }
    };


    public void loadQuestion() {
        if (IntroActivity.manager.getAccount_mode().equals("Guest")) {
            if (record >= 3) {
                String title = "محدودیت نسخه مهمان";
                String message = "برای استفاده از سوالات بیشتر ابتدا در برنامه ثبت نام کنید";
                message += "\n" + "کارنامه شما" + "\n";
                message += "تعداد کل سوالات: " + record + "\n";
                message += "تعداد پاسخ های درست: " + sahih.getText() + "\n";
                message += "تعداد پاسخ های غلط: " + qalat.getText() + "\n";
                message += "تعداد سوالات بی پاسخ: " + nazade + "\n";

                dialog(title, message, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                IntroActivity.manager.setAccount_mode("");
                                IntroActivity.manager.setAccount_name("");
                                IntroActivity.manager.setAccount_Reciption("");
                                IntroActivity.manager.setAccountIdetified(false);
                                ActivitySplash.activity.finish();
                                Test_Tamrin.activity.finish();
                                ChooseLesson.activity.finish();
                                ChooseChapter.activity.finish();
                                startActivity(new Intent(Test.this,MainActivity.class));
                                finish();
                            }
                        }, "انتخاب درس","ثبت نام");


            }
        }
        else if (IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered"))
        {
            if (record >= 3) {
                String title = "محدودیت نسخه";
                String message = "برای استفاده از سوالات بیشتر و دسترسی به هزاران تست لطفا نسخه کامل را خریداری نمایید";
                message += "\n" + "کارنامه شما" + "\n";
                message += "تعداد کل سوالات: " + record + "\n";
                message += "تعداد پاسخ های درست: " + sahih.getText() + "\n";
                message += "تعداد پاسخ های غلط: " + qalat.getText() + "\n";
                message += "تعداد سوالات بی پاسخ: " + nazade + "\n";

                dialog(title, message, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(Test.this,BuyIntent.class));
                            }
                        }, "انتخاب درس","خرید نسخه کامل");


            }
        }

        page_title = Book.book.getBook_name() + " - تست" + (record + 1);
        title.setText(page_title);
        Picasso.with(this).load(questions.get(record).getQuestion()).memoryPolicy(MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_STORE).into(soal);
        Picasso.with(this).load(questions.get(record).getTestAnswer()).memoryPolicy(MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_STORE).into(javab);

        ((HorizontalScrollView)findViewById(R.id.question)).postDelayed(runnable,1000);

        javab.setVisibility(View.INVISIBLE);

        select1.setEnabled(true);
        select2.setEnabled(true);
        select3.setEnabled(true);
        select4.setEnabled(true);

        status = false;

        select1.setBackgroundResource(R.drawable.select_normal);
        select2.setBackgroundResource(R.drawable.select_normal);
        select3.setBackgroundResource(R.drawable.select_normal);
        select4.setBackgroundResource(R.drawable.select_normal);
        select1.setTextColor(Color.parseColor("#da9292"));
        select2.setTextColor(Color.parseColor("#da9292"));
        select3.setTextColor(Color.parseColor("#da9292"));
        select4.setTextColor(Color.parseColor("#da9292"));
    }

    private void getData(String major, String book, String subBook, String chapter) {
        try {
            questions = new ArrayList<Question>();

            String req = URLEncoder.encode("major", "utf-8") + "=" + URLEncoder.encode(major, "UTF-8");
            req += "&" + URLEncoder.encode("lesson", "utf-8") + "=" + URLEncoder.encode(book, "UTF-8");
            req += "&" + URLEncoder.encode("subLesson", "utf-8") + "=" + URLEncoder.encode(subBook, "UTF-8");
            req += "&" + URLEncoder.encode("chapter", "utf-8") + "=" + URLEncoder.encode(chapter, "UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/GetQuestion.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("question");
            for (int i = 0; i < data.length(); i++) {
                JSONArray object = data.getJSONArray(i);
                questions.add(new Question(object));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

    class SendReport extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Test.this);
            dialog.setMessage("درحال ارسال درخواست برسی سوال");
            dialog.setTitle("گزارش سوال");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            sendReport(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            Toast.makeText(Test.this,"این سوال جهت برسی به لیست اضافه شد",Toast.LENGTH_LONG).show();
        }

        private void sendReport(String question) {
            try {
                String req = URLEncoder.encode("question_id", "utf-8") + "=" + URLEncoder.encode(question, "UTF-8");

                URL url = new URL("http://www.takraqami.ir/Application/Report_Question.php");
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(req);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                String result = stringBuilder.toString();
                JSONObject json = new JSONObject(result);
                JSONArray data = json.getJSONArray("res");
                for (int i = 0; i < data.length(); i++) {
                    JSONArray object = data.getJSONArray(i);
                    if (object.get(0).toString().equals("OK")) {
                        report = true;
                    } else {
                        report = false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}