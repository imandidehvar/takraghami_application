package com.takraqami.packages.application;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class flashcards extends AppCompatActivity {

    int count,index;
    String title,link;
    ProgressBar progressBar;
    TextView indextxt,counttxt,titletxt;
    ImageView next,dblnext,last,dbllast,flash;
    AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flashcards);
        indextxt = (TextView) findViewById(R.id.index);
        indextxt.setTypeface(MainActivity.font);

        counttxt = (TextView) findViewById(R.id.count);
        counttxt.setTypeface(MainActivity.font);

        titletxt = (TextView) findViewById(R.id.onvan);
        titletxt.setTypeface(MainActivity.font);

        progressBar = (ProgressBar) findViewById(R.id.progress);

        next = (ImageView) findViewById(R.id.next);
        last = (ImageView) findViewById(R.id.last);
        dbllast = (ImageView) findViewById(R.id.dbllast);
        dblnext = (ImageView) findViewById(R.id.dblnext);
        flash = (ImageView) findViewById(R.id.flash);


        count = Integer.parseInt(getIntent().getExtras().get("count").toString());
        counttxt.setText(String.valueOf(count));
        index = 1;
        title = getIntent().getExtras().get("name").toString();
        titletxt.setText(title);
        link =  getIntent().getExtras().get("link").toString();
        
        loadFlash();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index<count)
                {
                    index++;
                    loadFlash();
                }
                else
                {
                    Toast.makeText(flashcards.this,"پایان فلش کارت ها",Toast.LENGTH_LONG).show();
                }
            }
        });
        last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index>1)
                {
                    index--;
                    loadFlash();
                }
            }
        });
        dblnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((index+5)<count)
                {
                    index += 5;
                }
                else
                {
                    index = count;
                }
                loadFlash();
            }
        });
        dbllast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((index-5)>=1)
                {
                    index-=5;
                }
                else
                {
                    index = 1;
                }
                loadFlash();
            }
        });
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run()
        {
            progressBar.setMax(count);
            progressBar.setProgress(index);
            progressBar.postDelayed(this,1000);
        }
    };

    public void loadFlash()
    {
        String addr = link + "//" + index + ".gif";
        //Toast.makeText(flashcards.this,addr,Toast.LENGTH_LONG).show();
        //Glide.with(flashcards.this).load(addr).error(R.drawable.flash_image).into(flash);
        Picasso.with(flashcards.this).load(addr).into(flash);
        indextxt.setText(String.valueOf(index));
        progressBar.post(runnable);
        //progressBar.setProgress(index);
        if(index > 5 && !IntroActivity.manager.getAccount_mode().equals("Activated")) {
            if (IntroActivity.manager.getAccount_mode().equals("Guest")) {
                dialog("محدودیت نسخه مهمان", "برای دسترسی به تمام قابلیت ها لطفا نسخه کامل را تهیه فرمایید",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(flashcards.this, MainActivity.class));
                                ActivitySplash.activity.finish();
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        }, "ثبت نام", "بازگشت");
            }

            if (IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered")) {
                dialog("محدودیت نسخه", "برای دسترسی به تمام قابلیت ها لطفا نسخه کامل را تهیه فرمایید",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(flashcards.this, BuyIntent.class));
                            }},
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        }
                        , "خرید نسخه کامل", "بازگشت");
            }
        }
    }


    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }
}
