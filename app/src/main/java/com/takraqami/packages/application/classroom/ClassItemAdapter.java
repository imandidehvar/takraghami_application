package com.takraqami.packages.application.classroom;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.takraqami.packages.application.ActivitySplash;
import com.takraqami.packages.application.BuyIntent;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.takraqami.packages.application.MainActivity.context;

/**
 * Created by Majid Ettehadi on 09/14/2017.
 */

public class ClassItemAdapter extends ArrayAdapter<ClassItem> {

    Dialog dialog;

    public ClassItemAdapter(Context context, int resource, List<ClassItem> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.class_view, null);
        }

        final ClassItem item = getItem(position);

        if (item != null) {
            CircleImageView image = (CircleImageView) v.findViewById(R.id.image);
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView size = (TextView) v.findViewById(R.id.size);
            //TextView duration = (TextView) v.findViewById(R.id.duration);

            title.setText(item.title);
            title.setTypeface(MainActivity.font);
            size.setText("حجم: " + item.size);
            size.setTypeface(MainActivity.font);

            try{
                //duration
                //duration.setText("زمان: "+getVideoDuration(item.content_location.replaceAll("\\s","%20")));

                //image
                //System.out.println("http://"+item.pic_location.replaceAll("\\s","%20"));
                Picasso.with(context).load("http://"+item.pic_location.replaceAll("\\s","%20"))
                        .placeholder(getContext().getResources().getDrawable(R.drawable.video))
                        .into(image);
            } catch (Exception e){
                e.printStackTrace();
            }


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(IntroActivity.manager.getAccount_mode().equals("Guest"))
                    {
                        dialog("محدودیت نسخه مهمان", "جهت استفاده از تمامی امکانات برنامه لطفا ابتدا در برنامه ثبت نام کنید", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ((ClassActivity)getContext()).finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getContext().startActivity(new Intent(getContext(), com.takraqami.packages.application.MainActivity.class));
                                        ((ClassActivity)getContext()).finish();
                                        ActivitySplash.activity.finish();
                                    }
                                }, "بازگشت", "ثبت نام در برنامه");
                    }
                    else if(IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered"))
                    {
                        dialog("محدودیت نسخه", "جهت استفاده از تمامی امکانات برنامه لطفا نسخه کامل را خریداری نمایید", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ((ClassActivity)getContext()).finish();
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getContext().startActivity(new Intent(getContext(), BuyIntent.class));
                                    }
                                }, "بازگشت", "خرید نسخه کامل");
                    } else{
                        Intent intent = new Intent(getContext(),PlayVideoActivity.class);
                        intent.putExtra("id",item.id);
                        intent.putExtra("title",item.title);
                        intent.putExtra("category",item.category);
                        intent.putExtra("size",item.size);
                        intent.putExtra("duration",item.duration);
                        intent.putExtra("pic_location",item.pic_location);
                        intent.putExtra("content_location",item.content_location);
                        intent.putExtra("hd_location",item.hd_location);
                        getContext().startActivity(intent);
                    }
                }
            });
        }

        return v;
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) ((ClassActivity)getContext()).findViewById(android.R.id.content);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

    public String getVideoDuration(String url){
        /*FFmpegMediaMetadataRetriever mFFmpegMediaMetadataRetriever = new FFmpegMediaMetadataRetriever();
        mFFmpegMediaMetadataRetriever.setDataSource(context, Uri.parse(url));
        String mVideoDuration =  mFFmpegMediaMetadataRetriever .extractMetadata(FFmpegMediaMetadataRetriever .METADATA_KEY_DURATION);
        long mTimeInMilliseconds= Long.parseLong(mVideoDuration);
        int h=(int)(mTimeInMilliseconds/3600000);
        int m=(int)((mTimeInMilliseconds-(h*3600000))/60000);
        int s=(int)((mTimeInMilliseconds-(h*3600000)-(m*60000)));
        return String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);*/
        return "";
    }

}
