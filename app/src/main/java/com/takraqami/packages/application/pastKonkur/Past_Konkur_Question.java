package com.takraqami.packages.application.pastKonkur;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.takraqami.packages.application.Karnameh_item;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Past_Konkur_Question extends AppCompatActivity {

    String address, name;
    int count;

    Typeface font;


    String page_title;
    TextView title;
    ImageView soal, javab, qNext;
    Button select1, select2, select3, select4;
    ArrayList<Integer> selects;
    ArrayList<String> answers;
    AlertDialog dialog;

    Picasso picasso;
    boolean status;
    int nazade;
    float tselect, fselect;
    int record;

    String book_id,selected_book_id,selected_konkur_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ((RelativeLayout) findViewById(R.id.activity_test__tamrin)).setBackgroundColor(Color.parseColor("#672DA6"));

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);
        font = Typeface.createFromAsset(getAssets(),"fonts/nazanin.ttf");


        address = getIntent().getExtras().getString("address");
        count = getIntent().getExtras().getInt("count");
        name = getIntent().getExtras().getString("name");
        selected_book_id = getIntent().getExtras().getString("book_id");
        selected_konkur_id = getIntent().getExtras().getString("konkur_id");
        answers = new ArrayList<String>();

        title = (TextView) findViewById(R.id.title1);
        title.setTypeface(MainActivity.font);
        soal = (ImageView) findViewById(R.id.question_img);
        javab = (ImageView) findViewById(R.id.pasokh);
        select1 = (Button) findViewById(R.id.select1);
        select1.setTypeface(font);
        select2 = (Button) findViewById(R.id.select2);
        select2.setTypeface(font);
        select3 = (Button) findViewById(R.id.select3);
        select3.setTypeface(font);
        select4 = (Button) findViewById(R.id.select4);
        select4.setTypeface(font);
        ((ImageView) findViewById(R.id.pasokh)).setVisibility(View.INVISIBLE);
        ((Button) findViewById(R.id.finish)).setVisibility(View.INVISIBLE);
        ((Button) findViewById(R.id.report)).setVisibility(View.INVISIBLE);
        qNext = (ImageView) findViewById(R.id.questionnext);
        ((LinearLayout) findViewById(R.id.karname)).setVisibility(View.INVISIBLE);
        selects = new ArrayList<Integer>();

        status = false;
        record = 0;
        nazade = 0;
        tselect = 0;
        fselect = 0;

        new GetAnswers().execute(selected_konkur_id,selected_book_id,String.valueOf(count));

        loadQuestion();

        qNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                if (record < count) {
                    if (!status) {
                        nazade++;
                        record++;
                        selects.add(0);
                        loadQuestion();
                    } else {
                        record++;
                        loadQuestion();
                    }
                } else {
                    float percent = (float) (((tselect * 3) - fselect) / (count * 3)) * 100;
                    Karnameh_item.answered.add(new Karnameh_item(name, percent, 1, null, selects, true));
                    finish();
                }
            }
        });


        select1.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View view){
                selects.add(1);
                check("1");
            }
        });
        select2.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View view){
                selects.add(2);
                check("2");
            }
        });
        select3.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View view){
                selects.add(3);
                check("3");
            }
        });
        select4.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View view){
                selects.add(4);
                check("4");
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        float percent = (float) (((tselect * 3) - fselect) / (count * 3)) * 100;
        Karnameh_item.answered.add(new Karnameh_item(name, percent,1, null, selects, true));
        finish();
    }

    class GetAnswers extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Past_Konkur_Question.this);
            dialog.setMessage("در حال آماده سازی آزمون");
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected Void doInBackground(String... strings) {
            try
            {
                String req = URLEncoder.encode("konkur_id","utf-8") + "=" + URLEncoder.encode(strings[0],"UTF-8");
                req += "&" + URLEncoder.encode("lesson_id","utf-8") + "=" + URLEncoder.encode(strings[1],"UTF-8");
                req += "&" + URLEncoder.encode("course_count","utf-8") + "=" + URLEncoder.encode(strings[2],"UTF-8");

                URL url = new URL("http://www.takraqami.ir/Application/Get_Last_konkur_Answers.php");
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(req);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                String result = stringBuilder.toString();
                JSONObject json = new JSONObject(result);
                JSONArray data = json.getJSONArray("answers");
                for(int i = 0; i<data.length();i++) {
                    answers.add(data.getString(i));
                }
            }
            catch (IOException e) {e.printStackTrace();}
            catch (JSONException e){e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            selects = new ArrayList<Integer>();
            dialog.dismiss();
        }
    }

    private void check(String s)
    {
        if (s.equals(answers.get(record).toString())) {
            tselect++;
            if (s.equals("1")) {
                select1.setBackgroundResource(R.drawable.select_true);
                select1.setTextColor(Color.WHITE);
            } else if (s.equals("2")) {
                select2.setBackgroundResource(R.drawable.select_true);
                select2.setTextColor(Color.WHITE);
            } else if (s.equals("3")) {
                select3.setBackgroundResource(R.drawable.select_true);
                select3.setTextColor(Color.WHITE);
            } else if (s.equals("4")) {
                select4.setBackgroundResource(R.drawable.select_true);
                select4.setTextColor(Color.WHITE);
            }
        } else {
            fselect++;
            if (answers.get(record).toString().equals("1")) {
                select1.setBackgroundResource(R.drawable.select_true);
                select1.setTextColor(Color.WHITE);
            } else if (answers.get(record).toString().equals("2")) {
                select2.setBackgroundResource(R.drawable.select_true);
                select2.setTextColor(Color.WHITE);
            } else if (answers.get(record).toString().equals("3")) {
                select3.setBackgroundResource(R.drawable.select_true);
                select3.setTextColor(Color.WHITE);
            } else if (answers.get(record).toString().equals("4")) {
                select4.setBackgroundResource(R.drawable.select_true);
                select4.setTextColor(Color.WHITE);
            }

            if (s.equals("1")) {
                select1.setBackgroundResource(R.drawable.select_wrong);
                select1.setTextColor(Color.WHITE);
            } else if (s.equals("2")) {
                select2.setBackgroundResource(R.drawable.select_wrong);
                select2.setTextColor(Color.WHITE);
            } else if (s.equals("3")) {
                select3.setBackgroundResource(R.drawable.select_wrong);
                select3.setTextColor(Color.WHITE);
            } else if (s.equals("4")) {
                select4.setBackgroundResource(R.drawable.select_wrong);
                select4.setTextColor(Color.WHITE);
            }
        }
        Picasso.with(this).load(address + "/" + record + "/" + "p.gif").into(javab);
        javab.setVisibility(View.VISIBLE);
        select1.setEnabled(false);
        select2.setEnabled(false);
        select3.setEnabled(false);
        select4.setEnabled(false);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((HorizontalScrollView) findViewById(R.id.javab)).postDelayed(runnable, 1000);
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!((HorizontalScrollView) findViewById(R.id.question)).fullScroll(HorizontalScrollView.FOCUS_RIGHT)) {
                ((HorizontalScrollView) findViewById(R.id.question)).fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            } else {
                ((HorizontalScrollView) findViewById(R.id.question)).postDelayed(runnable, 40000);
            }

            if (!((HorizontalScrollView) findViewById(R.id.javab)).fullScroll(HorizontalScrollView.FOCUS_RIGHT)) {
                ((HorizontalScrollView) findViewById(R.id.javab)).fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            } else {
                ((HorizontalScrollView) findViewById(R.id.javab)).postDelayed(runnable, 40000);
            }
        }
    };

    public void loadQuestion() {
        page_title = name + " - تست" + (record + 1);
        title.setText(page_title);
        Picasso.with(this).load(address + "/" + (record+1) + "/" + "s.gif").memoryPolicy(MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_STORE).into(soal);
        Picasso.with(this).load(address + "/" + (record+1) + "/" + "p.gif").memoryPolicy(MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_STORE).into(javab);


        ((HorizontalScrollView) findViewById(R.id.question)).postDelayed(runnable, 1000);

        javab.setVisibility(View.INVISIBLE);

        select1.setEnabled(true);
        select2.setEnabled(true);
        select3.setEnabled(true);
        select4.setEnabled(true);

        status = false;

        select1.setBackgroundResource(R.drawable.select_normal);
        select2.setBackgroundResource(R.drawable.select_normal);
        select3.setBackgroundResource(R.drawable.select_normal);
        select4.setBackgroundResource(R.drawable.select_normal);
        select1.setTextColor(Color.parseColor("#da9292"));
        select2.setTextColor(Color.parseColor("#da9292"));
        select3.setTextColor(Color.parseColor("#da9292"));
        select4.setTextColor(Color.parseColor("#da9292"));
    }

}
