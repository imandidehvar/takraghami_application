package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by iman on 7س/9/2017 AD.
 */

public class Maghleh
{
    private int id;
    private String category;
    private String date;
    private String title;
    private String picture;
    private String likes;
    private String dislikes;


    public Maghleh(JSONArray object) {
        try
        {
            this.id = object.getInt(0);
            this.category = object.getString(1);
            this.date = object.getString(2);
            this.title = object.getString(3);
            this.picture =  object.getString(4);
            this.likes = object.getString(5);
            this.dislikes = object.getString(6);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getPicture() {
        return picture;
    }

    public String getLikes() {
        return likes;
    }

    public String getDislikes() {
        return dislikes;
    }
}
