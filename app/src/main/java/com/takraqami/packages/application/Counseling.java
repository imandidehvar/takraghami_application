package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Iman on 5/24/2017.
 */

public class Counseling
{
    public static Counseling counseling;
    private String id;
    private String user;
    private String title;
    private String category;
    private String question;
    private String answer;
    private String flag;

    public Counseling(JSONArray object)
    {
        try {
            id = object.get(0).toString();
            user = object.get(1).toString();
            title = object.get(2).toString();
            category = object.get(3).toString();
            question = object.get(4).toString();
            answer = object.get(5).toString();
            flag = object.getString(6).toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public String getFlag() {
        return flag;
    }
}
