package com.takraqami.packages.application;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Iman on 4/12/2017.
 */

public class IntroManager
{
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "androidhive-welcome";
    private static final String account_identify = "account_identify";
    private static final String account_reciption = "account_reciption";
    private static final String account_mode = "account_mode";
    private static final String account_name = "account_name";
    private static final String student_id = "student_id";
    private static final String student_phone = "student_phone";
    private static final String student_major = "student_major";
    private static final String student_major_id = "major_id";
    private static final String student_konkurs = "student_konkurs";

    public IntroManager(Context context) {
        _context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setAccountIdetified(boolean ident)
    {
        editor.putBoolean(account_identify,ident);
        editor.apply();
    }

    public boolean isLogedin() {
        return pref.getBoolean(account_identify, false);
    }

    public void setAccount_Reciption(String value)
    {
        editor.putString(account_reciption,value);
        editor.apply();
    }
    public String getAccount_reciption(){ return pref.getString(account_reciption,""); }

    public void setAccount_mode(String value)
    {
        editor.putString(account_mode,value);
        editor.apply();
    }
    public String getAccount_mode(){ return pref.getString(account_mode,""); }

    public void setAccount_name(String value)
    {
        editor.putString(account_name,value);
        editor.apply();
    }
    public String getAccount_name(){ return pref.getString(account_name,""); }

    public void setStudent_id(String id) {
        editor.putString(student_id,id);
        editor.apply();
    }

    public String getStudent_id() {
        return pref.getString(student_id,"");
    }

    public void setStudent_major(String major) {
        editor.putString(student_major, major);
        editor.apply();
    }

    public String getStudent_major() {
        return pref.getString(student_major,"");
    }

    public void setStudent_phone(String phone) {
        editor.putString(student_phone,phone);
        editor.apply();
    }

    public String getStudent_phone() {
        return pref.getString(student_phone,"");
    }

    public void setStudent_major_id(int major_id) {
        editor.putInt(student_major_id,major_id);
        editor.apply();
    }

    public int getStudent_major_id() {
        return pref.getInt(student_major_id,0);
    }

    public void addNewKonkur(String konkur_id) {
        String temp = pref.getString(student_konkurs,"");
        editor.putString(student_konkurs,temp + "," + konkur_id);
        editor.apply();
    }

    public String getKonkurs()
    {
        return pref.getString(student_konkurs,"");
    }
}
