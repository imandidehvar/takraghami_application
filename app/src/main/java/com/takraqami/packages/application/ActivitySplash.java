package com.takraqami.packages.application;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.takraqami.packages.application.Darsname.DarsnameActivity;
import com.takraqami.packages.application.classroom.ClassroomActivity;
import com.takraqami.packages.application.pastKonkur.Past_Konkur;
import com.takraqami.packages.application.util.IabHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.takraqami.packages.application.util.Utilities;

public class ActivitySplash extends AppCompatActivity{

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    ImageView toggle;
    boolean drawerStatus;
    public static ArrayList<String> drawerList;
    String id, name, phone, major, mode, reciption;
    int major_code;
    IabHelper mHelper;
    TextView date, days;

    Handler handler;
    int hour,minute,second,diff;
    Utilities timerHelper;

    String supportLink;
    JSONArray messageObject;
    public static Activity activity;
    private Dialog dialog;

    boolean status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = this;
        status = false;
        handler = new Handler();

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        id = IntroActivity.manager.getStudent_id();
        name = IntroActivity.manager.getAccount_name();
        phone = IntroActivity.manager.getStudent_phone();
        major = IntroActivity.manager.getStudent_major();
        major_code = IntroActivity.manager.getStudent_major_id();
        mode = IntroActivity.manager.getAccount_mode();
        reciption = IntroActivity.manager.getAccount_reciption();

        NavigationView navigationView = (NavigationView) findViewById(R.id.left_drawer);
        date = (TextView) findViewById(R.id.date);
        days = (TextView) findViewById(R.id.timer);
        date.setTypeface(MainActivity.font);
        days.setTypeface(MainActivity.font);
        ((TextView)findViewById(R.id.daysCountdesc)).setTypeface(MainActivity.font);
        timerHelper =new Utilities();

        new GetTimer().execute(String.valueOf(major_code));

        drawerStatus = false;

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = (ImageView) findViewById(R.id.toggle);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                drawerStatus = false;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                drawerStatus = true;
            }
        };

        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT))
                {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                }
                else
                {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        View navHeader = navigationView.getHeaderView(0);
        ImageView toggleClose = (ImageView) navHeader.findViewById(R.id.toggle);
        TextView account = (TextView) navHeader.findViewById(R.id.account);
        account.setTypeface(MainActivity.font);
        account.setText(name);

        toggleClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT))
                {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                }
                else
                {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });


        final ListView menulist = (ListView) navigationView.findViewById(R.id.menulist);
        drawerList = new ArrayList<>();
        switch (mode) {
            case "Guest":
                drawerList.add("ثبت نام");
                drawerList.add("پشتیبانی");
                break;
            case "Free":
                drawerList.add("خرید نسخه کامل");
                drawerList.add("پشتیبانی");
                drawerList.add("خروج از حساب کاربری");
                break;
            case "Registered":
                drawerList.add("خرید نسخه کامل");
                drawerList.add("پشتیبانی");
                drawerList.add("خروج از حساب کاربری");
                break;
            case "Activated":
                drawerList.add("پشتیبانی");
                drawerList.add("خروج از حساب کاربری");
                break;
        }

        //drawerList.add("تنظیمات");
        //drawerList.add("خروج از برنامه");
        ArrayAdapter arrayAdapter = new ArrayAdapter(ActivitySplash.this, R.layout.drawerlist_items, drawerList);
        menulist.setAdapter(arrayAdapter);
        menulist.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = (String) menulist.getItemAtPosition(i);
                drawerFunction(item);
            }
        });
        RecyclerView menu = (RecyclerView) findViewById(R.id.menu);
        menu.setLayoutManager(new LinearLayoutManager(ActivitySplash.this, LinearLayoutManager.VERTICAL, false));
        MenuList list = new MenuList(ActivitySplash.this);
        list.add(R.mipmap.testicon, Color.parseColor("#29a9c9"), "تست و تمرین", Test_Tamrin.class);
        //list.add(R.mipmap.classroom, Color.parseColor("#1bc9b2"), "کلاس درس", ClassroomActivity.class);
        list.add(R.mipmap.konkoricon, Color.parseColor("#b27df2"), "کنکور های گذشته", Past_Konkur.class);
        list.add(R.mipmap.flash, Color.parseColor("#ffffff"), "فلش کارت", FlashCard.class);
        list.add(R.mipmap.moshaver, Color.parseColor("#9bcf6a"), "مشاور آنلاین", Moshaver.class);
        list.add(R.mipmap.smart_managment, Color.parseColor("#43b6b4"), "برنامه ریزی هوشمند", SmartPlaning.class);
        //list.add(R.mipmap.darsname, Color.parseColor("#7532CB"), "درسنامه و جزوه", DarsnameActivity.class);
        list.add(R.mipmap.azmoonicon, Color.parseColor("#b27df2"), "تست جامع", AzmoonStart.class);
        list.add(R.mipmap.konkoricon, Color.parseColor("#66a7e3"), "کنکور آنلاین", Konkur.class);
        list.add(R.mipmap.barnamehicon, Color.parseColor("#50bab8"), "برنامه ریزی شخصی", com.takraqami.packages.application.test.PlanningActivity.class);
        list.add(R.mipmap.bodjeicon, Color.parseColor("#fdae3f"), "بودجه بندی", com.takraqami.packages.application.test.BudgetActivity.class);
        list.add(R.mipmap.takhminicon, Color.parseColor("#aeff00"), "تخمین رتبه", com.takraqami.packages.application.test.RankingActivity.class);
        list.add(R.mipmap.news, Color.parseColor("#cfcfcf"), "مقاله و تازه ها", MaghalehList.class);
        list.add(R.mipmap.nemudar, Color.parseColor("#ff4646"), "نمودار پیشرفت", CommingSoon.class);

        menu.setAdapter(list);

    }


    private void drawerFunction(String item)
    {
        Intent intent = new Intent(ActivitySplash.this,MainActivity.class);
        switch (item)
        {
            case "ثبت نام":
                IntroActivity.manager.setAccount_mode("");
                IntroActivity.manager.setAccount_name("");
                IntroActivity.manager.setAccount_Reciption("");
                IntroActivity.manager.setAccountIdetified(false);
                startActivity(intent);
                finish();
                break;
            case "خرید نسخه کامل":
                startActivity(new Intent(this,BuyIntent.class));
            break;
            case "خروج از حساب کاربری":
                IntroActivity.manager.setAccount_mode("");
                IntroActivity.manager.setAccount_name("");
                IntroActivity.manager.setAccount_Reciption("");
                IntroActivity.manager.setAccountIdetified(false);
                IntroActivity.manager.setStudent_id("");
                getSharedPreferences("androidhive-welcome",MODE_PRIVATE).edit().putBoolean("pushe_reg",false).apply();
                startActivity(intent);
                finish();
                break;
            case "پشتیبانی":
                supportLink = "";
                new GetSupport().execute();
                break;
            case "تنظیمات":
                break;
/*            case "خروج از برنامه":
                finish();
                break;*/
        }
    }

    @Override
    public void onBackPressed()
    {
        if(status)
        {
            super.onBackPressed();
            finish();
        }
        else
        {
            Toast.makeText(activity,"جهت خروج مجدد کلید بازگشت را فشار دهید",Toast.LENGTH_LONG).show();
            status = true;
        }
    }

    class GetTimer extends AsyncTask<String,Void,Void>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getTimer(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            date.setText(String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second));
            days.setText(String.valueOf(diff));
            try
            {
                if(messageObject.getInt(3) == 1)
                {
                    dialog(messageObject.getString(0), messageObject.getString(1),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            }
                            , messageObject.getString(2));
                }
                handler.postDelayed(runnable,1000);
            }
            catch (Exception e) {e.printStackTrace();
                date.setText("");
                days.setText("");}
        }
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, String btn1) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_soon, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setText(btn1);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
    }

    private void getTimer(String major_id)
    {
        try
        {

            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(major_id,"UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/Get_Timer.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("time");
            JSONArray object = data.getJSONArray(0);
            this.diff =  Integer.parseInt(object.get(0).toString());
            this.hour =  Integer.parseInt(object.get(1).toString());
            this.minute = Integer.parseInt(object.get(2).toString());
            this.second = Integer.parseInt(object.get(3).toString());

            JSONArray message = json.getJSONArray("message");
            this.messageObject = message.getJSONArray(0);
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    Runnable runnable = new Runnable(){
        public void run() {
            if(second>0)
            {
                second--;
            }
            else
            {
                second = 59;
                if(minute>0)
                {
                    minute--;
                }
                else
                {
                    minute = 59;
                    hour--;
                }
            }
            if(minute==0)
            {
                minute = 59;
                hour--;
            }
            date.setText(String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second));
            days.setText(String.valueOf(diff));
            handler.postDelayed(runnable,1000);
        }
    };

    class GetSupport extends AsyncTask<Void,Void,Void>
    {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ActivitySplash.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getSupport();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            if(supportLink.equals("null") || supportLink.equals("") || supportLink.equals(null))
            {
                Toast.makeText(ActivitySplash.this,"درحال حاظر اوپراتور جهت پاسخگویی در دسترس نیست",Toast.LENGTH_LONG).show();
            }
            else
            {
                Intent telegram = new Intent(Intent.ACTION_VIEW , Uri.parse(supportLink));
                startActivity(telegram);
            }
        }
    }

    private void getSupport()
    {
        try
        {

            URL url = new URL("http://www.takraqami.ir/Application/Get_Support.php");
            URLConnection connection = url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("link");
            JSONArray object = data.getJSONArray(0);
            supportLink = object.get(0).toString();
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}

    }
}