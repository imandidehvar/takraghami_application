package com.takraqami.packages.application.test.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Majid Ettehadi on 2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static class PlanColumns{
        public static String ID = "plan_id";
        public static String TITLE = "plan_title";
        public static String FROM = "date_from";
        public static String UNTIL = "date_until";
        public static String DESCRIPTION = "plan_description";
        public static String STATUS = "status";
        public static String NOTIFICATION = "notification";
    }

    private static final String DB_NAME = "plandb";
    private static final int DB_VER = 3;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS" +
                " plan ( " +
                PlanColumns.ID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                PlanColumns.TITLE+" TEXT, " +
                PlanColumns.FROM+" TEXT, " +
                PlanColumns.UNTIL+" TEXT, " +
                PlanColumns.DESCRIPTION+" TEXT, " +
                PlanColumns.STATUS+" INTEGER, " +
                PlanColumns.NOTIFICATION+" INTEGER " +
                " );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS plan");
        onCreate(db);
    }

    public boolean doQry(String sql) {
        try {
            this.getWritableDatabase().execSQL(sql);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Cursor selectQry(String sql) {
        try {
            return this.getWritableDatabase().rawQuery(sql, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
