package com.takraqami.packages.application.test.Calendar;

/**
 * Created by etteh on 5/8/2017.
 */

public class Day {

    public PersianDate date;
    public boolean holiday;
    public boolean today;
    public boolean selected;
    public String dateStr;

    public Day(String dateStr){
        this.dateStr=dateStr;
    }

}
