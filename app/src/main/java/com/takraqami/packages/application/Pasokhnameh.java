package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Iman on 6/15/2017.
 */

public class Pasokhnameh extends AppCompatActivity
{
    String pageTitle;
    TextView title, sahih, qalat;
    ImageView soal, javab, qNext;
    Button select1, select2, select3, select4, finish;
    ArrayList<Question> questions;
    ArrayList<Integer> selects;

    public int id;

    Typeface font;
    int record;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        id = (Integer)getIntent().getExtras().get("id");



        font = Typeface.createFromAsset(getAssets(),"fonts/nazanin.ttf");
        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        title = (TextView) findViewById(R.id.title1);
        title.setTypeface(MainActivity.font);
        sahih = (TextView) findViewById(R.id.txtsahihc);
        sahih.setTypeface(MainActivity.font);
        qalat = (TextView) findViewById(R.id.txtqalatc);
        qalat.setTypeface(MainActivity.font);
        soal = (ImageView) findViewById(R.id.question_img);
        select1 = (Button) findViewById(R.id.select1);
        select1.setTypeface(font);
        select1.setEnabled(false);
        select2 = (Button) findViewById(R.id.select2);
        select2.setTypeface(font);
        select2.setEnabled(false);
        select3 = (Button) findViewById(R.id.select3);
        select3.setTypeface(font);
        select3.setEnabled(false);
        select4 = (Button) findViewById(R.id.select4);
        select4.setTypeface(font);
        select4.setEnabled(false);
        javab = (ImageView) findViewById(R.id.pasokh);
        qNext = (ImageView) findViewById(R.id.questionnext);
        finish = (Button) findViewById(R.id.finish);
        finish.setTypeface(MainActivity.font);
        finish.setText("بازگشت");
        ((LinearLayout) findViewById(R.id.karname)).setVisibility(View.INVISIBLE);
        ((Button) findViewById(R.id.report)).setVisibility(View.INVISIBLE);


        record = 0;

        if(Karnameh_item.answered.get(id).status == true)
        {
            questions = new ArrayList<Question>(Karnameh_item.answered.get(id).questions);
            selects = Karnameh_item.answered.get(id).answers;
            loadQuestion();
            check(selects.get(0).toString());
        }
        else if(Karnameh_item.answered.get(id).status == false)
        {
            new GetData().execute(Karnameh_item.answered.get(id).konkur_id,Karnameh_item.answered.get(id).id,String.valueOf(IntroActivity.manager.getStudent_major_id()));
        }



        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        qNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(record < (questions.size()-1))
                {
                    record++;
                    loadQuestion();
                    check(selects.get(record).toString());
                }
                else
                {
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void check(String s) {
        if (s.equals(questions.get(record).getTrueSelect()))
        {
            if (s.equals("1"))
            {
                select1.setBackgroundResource(R.drawable.select_true);
                select1.setTextColor(Color.WHITE);
            }
            else if (s.equals("2"))
            {
                select2.setBackgroundResource(R.drawable.select_true);
                select2.setTextColor(Color.WHITE);
            }
            else if (s.equals("3"))
            {
                select3.setBackgroundResource(R.drawable.select_true);
                select3.setTextColor(Color.WHITE);
            }
            else if (s.equals("4"))
            {
                select4.setBackgroundResource(R.drawable.select_true);
                select4.setTextColor(Color.WHITE);
            }
            else
            {

            }
        }
        else
        {
            if (questions.get(record).getTrueSelect().equals("1"))
            {
                select1.setBackgroundResource(R.drawable.select_true);
                select1.setTextColor(Color.WHITE);
            }
            else if (questions.get(record).getTrueSelect().equals("2"))
            {
                select2.setBackgroundResource(R.drawable.select_true);
                select2.setTextColor(Color.WHITE);
            }
            else if (questions.get(record).getTrueSelect().equals("3"))
            {
                select3.setBackgroundResource(R.drawable.select_true);
                select3.setTextColor(Color.WHITE);
            }
            else if (questions.get(record).getTrueSelect().equals("4"))
            {
                select4.setBackgroundResource(R.drawable.select_true);
                select4.setTextColor(Color.WHITE);
            }

            if (s.equals("1"))
            {
                select1.setBackgroundResource(R.drawable.select_wrong);
                select1.setTextColor(Color.WHITE);
            }
            else if (s.equals("2"))
            {
                select2.setBackgroundResource(R.drawable.select_wrong);
                select2.setTextColor(Color.WHITE);
            }
            else if (s.equals("3"))
            {
                select3.setBackgroundResource(R.drawable.select_wrong);
                select3.setTextColor(Color.WHITE);
            }
            else if (s.equals("4"))
            {
                select4.setBackgroundResource(R.drawable.select_wrong);
                select4.setTextColor(Color.WHITE);
            }
        }
        Picasso.with(Pasokhnameh.this).load(questions.get(record).getTestAnswer()).into(javab);
        javab.setVisibility(View.VISIBLE);
        select1.setEnabled(false);
        select2.setEnabled(false);
        select3.setEnabled(false);
        select4.setEnabled(false);
        try{Thread.sleep(100);} catch (InterruptedException e){e.printStackTrace();}
        ((HorizontalScrollView)findViewById(R.id.javab)).postDelayed(runnable,1000);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(!((HorizontalScrollView)findViewById(R.id.question)).fullScroll(HorizontalScrollView.FOCUS_RIGHT))
            {
                ((HorizontalScrollView)findViewById(R.id.question)).fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
            else
            {
                ((HorizontalScrollView)findViewById(R.id.question)).postDelayed(runnable,40000);
            }

            if(!((HorizontalScrollView)findViewById(R.id.javab)).fullScroll(HorizontalScrollView.FOCUS_RIGHT))
            {
                ((HorizontalScrollView)findViewById(R.id.javab)).fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
            else
            {
                ((HorizontalScrollView)findViewById(R.id.javab)).postDelayed(runnable,40000);
            }
        }
    };

    public void loadQuestion() {
        pageTitle = Karnameh_item.answered.get(id).name + " - تست" + (record + 1);
        title.setText(pageTitle);
        Picasso.with(this).load(questions.get(record).getQuestion()).memoryPolicy(MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_STORE).into(soal);
        Picasso.with(this).load(questions.get(record).getTestAnswer()).memoryPolicy(MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_STORE).into(javab);

        ((HorizontalScrollView)findViewById(R.id.question)).postDelayed(runnable,1000);

        javab.setVisibility(View.INVISIBLE);

        select1.setBackgroundResource(R.drawable.select_normal);
        select2.setBackgroundResource(R.drawable.select_normal);
        select3.setBackgroundResource(R.drawable.select_normal);
        select4.setBackgroundResource(R.drawable.select_normal);
        select1.setTextColor(Color.parseColor("#da9292"));
        select2.setTextColor(Color.parseColor("#da9292"));
        select3.setTextColor(Color.parseColor("#da9292"));
        select4.setTextColor(Color.parseColor("#da9292"));
    }

    public class GetData extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            dialog = new ProgressDialog(Pasokhnameh.this);
            dialog.setMessage("در حال اماده سازی پاسخنامه");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings)
        {
            getData(strings[0], strings[1], strings[2]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            selects = new ArrayList<Integer>();
            for (int i = 0; i < questions.size(); i++)
            {
                selects.add(0);
            }
            loadQuestion();
            check(selects.get(record).toString());
            dialog.dismiss();
        }

    }


    private void getData(String konkur, String book, String major) {
        try {
            questions = new ArrayList<Question>();
            String req = URLEncoder.encode("konkur_id", "utf-8") + "=" + URLEncoder.encode(konkur, "UTF-8");
            req += "&" + URLEncoder.encode("book_id", "utf-8") + "=" + URLEncoder.encode(book, "UTF-8");
            req += "&" + URLEncoder.encode("major_id", "utf-8") + "=" + URLEncoder.encode(major, "UTF-8");


            URL url = new URL("http://www.takraqami.ir/Application/Get_Konkur_Question.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("question");
            for (int i = 0; i < data.length(); i++) {
                JSONArray object = data.getJSONArray(i);
                questions.add(new Question(object));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
