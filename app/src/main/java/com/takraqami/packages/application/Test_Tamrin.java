package com.takraqami.packages.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bignerdranch.expandablerecyclerview.model.Parent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Test_Tamrin extends AppCompatActivity
{
    private ArrayList<Book> special;
    private ArrayList<Book> general;
    private Map<String,List<Book>> list;

    public static  Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test__tamrin);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        activity = this;
        general = new ArrayList<Book>();
        special = new ArrayList<Book>();
        new GetDate().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()));

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    class ExpandableAdapter extends ExpandableRecyclerAdapter<GroupName, Book, ParentView, ChildView>
    {
        Context mContext;
        List<GroupName> list;

        public ExpandableAdapter(Context context, List<GroupName> parentItemList) {
            super(parentItemList);
            mContext = context;
            list = parentItemList;

        }

        @Override
        public ParentView onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.test_tamrin_item_header, parentViewGroup, false);
            return new ParentView(view);
        }

        @Override
        public ChildView onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.test_tamrin_item_child, childViewGroup, false);
            return new ChildView(view,childViewGroup);
        }

        @Override
        public void onBindParentViewHolder(@NonNull ParentView parentViewHolder, int parentPosition, @NonNull GroupName parent)
        {
            parentViewHolder.bind(parent);
        }

        @Override
        public void onBindChildViewHolder(@NonNull ChildView childViewHolder, int parentPosition, int childPosition, @NonNull Book child)
        {
            childViewHolder.bind(child);
        }

    }

    class ParentView extends ParentViewHolder {

        public TextView headerText;
        public ImageView mParentDropDownArrow;
        public CardView mLayout;
        boolean status;
        public ParentView(View itemView) {
            super(itemView);
            status = false;
            headerText = (TextView) itemView.findViewById(R.id.parent_list_item_crime_title_text_view);
            mParentDropDownArrow = (ImageView) itemView.findViewById(R.id.parent_list_item_expand_arrow);
            mLayout = (CardView) itemView.findViewById(R.id.expandable_header);
            headerText.setTypeface(MainActivity.font);
            mLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(status == false)
                    {
                        status = true;
                        mParentDropDownArrow.setBackgroundResource(R.drawable.ic_arrow_drop_up_white);
                    }
                    else
                    {
                        status = false;
                        mParentDropDownArrow.setBackgroundResource(R.drawable.ic_arrow_drop_down_white);
                    }
                }
            });
        }

        public void bind(GroupName name)
        {
            headerText.setText(name.name);
            mLayout.setBackgroundResource(name.color);
        }
    }

    class ChildView extends ChildViewHolder {

        public Button bookname;
        public Book book;
        public ViewGroup root;
        public ChildView(View itemView, final ViewGroup rootView) {
            super(itemView);
            bookname = (Button) itemView.findViewById(R.id.child_list_item_crime_date_text_view);
            bookname.setTypeface(MainActivity.font);
            root = rootView;
            bookname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(book.getBook_have_sub().equals("1"))
                    {
                        Book.book = book;
                        Intent intent = new Intent(Test_Tamrin.this,ChooseLesson.class);
                        intent.putExtra("book_id",book.getId());
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(Test_Tamrin.this,ChooseChapter.class);
                        intent.putExtra("book_id",book.getId());
                        startActivity(intent);
                    }
                }
            });
        }

        public void bind(Book book)
        {
            bookname.setText(book.getBook_name());
            this.book = book;
        }


    }

    class GetDate extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Test_Tamrin.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            GroupName group1 = new GroupName("دروس تخصصی",R.drawable.expandable_header_style_special, special.subList(0,special.size()));
            GroupName group2 = new GroupName("دروس عمومی",R.drawable.expandable_header_style_general, general.subList(0,general.size()));

            List<GroupName> groupNames = Arrays.asList(group2,group1);

            RecyclerView list = (RecyclerView) findViewById(R.id.lessons_list);
            ExpandableRecyclerAdapter adapter = new ExpandableAdapter(Test_Tamrin.this,groupNames);
            list.setAdapter(adapter);
            list.setLayoutManager(new LinearLayoutManager(Test_Tamrin.this,LinearLayoutManager.VERTICAL,false));
            dialog.dismiss();
        }
    }

    private void getData(String string)
    {
        try
        {
            String req = URLEncoder.encode("major","utf-8") + "=" + URLEncoder.encode(string,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Get_Books.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("book");
            for(int i = 0; i < data.length();i++)
            {
                JSONArray object = data.getJSONArray(i);
                if(object.get(6).equals("0"))
                {
                    general.add(new Book(object));
                }
                else
                {
                    special.add(new Book(object));
                }
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    public class GroupName implements Parent<Book>
    {
        String name;
        int color;
        private List<Book> books;

        @Override
        public List<Book> getChildList() {
            return books;
        }

        @Override
        public boolean isInitiallyExpanded() {
            return false;
        }

        public GroupName(String name, int color, List<Book> book) {
            this.name = name;
            this.color = color;
            this.books = book;
        }

    }
}
