package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.takraqami.packages.application.R.layout.dialog;

public class Konkur extends AppCompatActivity {

    AlertDialog dialogV;
    RecyclerView list;
    Dialog dialog;
    Adapter adapter;

    ArrayList<konkur> konkurlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konkur);

        konkurlist = new ArrayList<konkur>();
        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        if(IntroActivity.manager.getAccount_mode().equals("Guest"))
        {
            dialog("محدودیت نسخه مهمان", "برای دسترسی به کنکور های انلاین لطفا ابتدا در برنامه ثبت نام کنید", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    },
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(Konkur.this, MainActivity.class));
                            ActivitySplash.activity.finish();
                            finish();
                        }
                    }, "بازگشت", "ثبت نام");
        }

        if (IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered")) {
            dialog("محدودیت نسخه", "برای دسترسی به کنکور های انلاین لطفا نسخه کامل را تهیه فرمایید", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    },
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(Konkur.this, BuyIntent.class));
                        }
                    }, "بازگشت", "خرید نسخه کامل");


        }

        new GetData().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()));

    }

    public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(Konkur.this).inflate(R.layout.konkur_main_item,parent,false);
            return new ConfigViewer(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position)
        {
            ConfigViewer view = (ConfigViewer)holder;
            view.title.setText(konkurlist.get(position).title.toString());
            view.time.setText(konkurlist.get(position).date.toString());
            view.item.setCardBackgroundColor(Color.parseColor(konkurlist.get(position).flagColor.toString()));
            view.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(konkurlist.get(position).flagDate.toString().equals("1"))
                    {
                        String konkur_list = IntroActivity.manager.getKonkurs();
                        String[] list = konkur_list.split(",");
                        boolean flag = false;
                        for (int i = 0; i < list.length; i++)
                        {
                            if(konkurlist.get(position).id.equals(list[i]))
                            {
                                Intent intent = new Intent(Konkur.this,Karnameh.class);
                                intent.putExtra("Mode","Full");
                                intent.putExtra("konkur_id", konkurlist.get(position).id.toString());
                                startActivity(intent);
                                flag = true;
                                break;
                            }
                        }
                        if(flag == false) {
                            dialog("پیغام", "زمان این ازمون مطابق زمان استاندارد سازمان سنجش انتخاب شده است.",
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                            Intent intent = new Intent(Konkur.this, Konkur_Choose_Lesson.class);
                                            intent.putExtra("konkur_id", konkurlist.get(position).id.toString());
                                            startActivity(intent);
                                        }
                                    }, "شروع ازمون");
                        }

                    }
                    else if (konkurlist.get(position).flagDate.toString().equals("2"))
                    {
                        String konkur_list = IntroActivity.manager.getKonkurs();
                        String[] list = konkur_list.split(",");
                        boolean flag = false;
                        for (int i = 0; i < list.length; i++)
                        {
                            if(konkurlist.get(position).id.equals(list[i]))
                            {
                                flag = true;
                                break;
                            }
                        }
                        if(flag == false)
                        {
                            Toast.makeText(Konkur.this,"شما در این آزمون شرکت نکرده اید",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Intent intent = new Intent(Konkur.this,Karnameh.class);
                            intent.putExtra("Mode","Full");
                            intent.putExtra("konkur_id", konkurlist.get(position).id.toString());
                            startActivity(intent);
                        }

                    }
                    else if(konkurlist.get(position).flagDate.toString().equals("3"))
                    {
                        Toast.makeText(Konkur.this,"برگزار میگردد." + konkurlist.get(position).date.toString() + "این ازمون به صورت سراسری در تاریخ",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return konkurlist.size();
        }
    }



    class konkur
    {
        String id;
        String title;
        String date;
        String flagColor;
        String flagDate;

        public konkur(JSONArray object) {
            try
            {
                this.id = object.get(0).toString();
                this.title = object.get(1).toString();
                this.date = object.get(2).toString();
                this.flagColor = object.get(3).toString();
                this.flagDate = object.get(4).toString();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class ConfigViewer extends RecyclerView.ViewHolder {
        TextView title,time;
        CardView item;

        public ConfigViewer(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            time = (TextView) view.findViewById(R.id.time);
            item = (CardView) view.findViewById(R.id.item);
            title.setTypeface(MainActivity.font);
            time.setTypeface(MainActivity.font);
        }
    }
    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }
    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, String btn1) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_soon, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setText(btn1);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
    }
    public class GetData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Konkur.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            list = (RecyclerView) findViewById(R.id.lessons_list);
            list.setLayoutManager(new LinearLayoutManager(Konkur.this, LinearLayoutManager.VERTICAL, false));
            Adapter adapter = new Adapter();
            list.setAdapter(adapter);
            dialog.dismiss();
        }
    }

    private void getData(String string)
    {
        try
        {
            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(string,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Konkur_Online.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("konkur");
            for(int i = 0; i < data.length();i++)
            {
                JSONArray object = data.getJSONArray(i);
                konkurlist.add(new konkur(object));
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

}
