package com.takraqami.packages.application;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AzmoonStart extends AppCompatActivity {

    Button btn;
    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_azmoon_start);
        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);


        btn = (Button)findViewById(R.id.start);
        btn.setTypeface(MainActivity.font);
        ((TextView) findViewById(R.id.text)).setTypeface(MainActivity.font);
        ((TextView) findViewById(R.id.txttitle)).setTypeface(MainActivity.font);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(IntroActivity.manager.getAccount_mode().equals("Guest"))
                {
                    dialog("محدودیت نسخه مهمان", "برای دسترسی به کنکور های انلاین لطفا ابتدا در برنامه ثبت نام کنید", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                    dialog.dismiss();
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(AzmoonStart.this, MainActivity.class));
                                    dialog.dismiss();
                                    ActivitySplash.activity.finish();
                                    finish();
                                }
                            }, "بازگشت", "ثبت نام");
                }

                else if (IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered")) {
                    dialog("محدودیت نسخه", "برای دسترسی به کنکور های انلاین لطفا نسخه کامل را تهیه فرمایید", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(AzmoonStart.this, BuyIntent.class));
                                }
                            }, "بازگشت", "خرید نسخه کامل");


                }
                else
                {
                    Intent intent = new Intent(AzmoonStart.this,Activity_Azmoon.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }
}
