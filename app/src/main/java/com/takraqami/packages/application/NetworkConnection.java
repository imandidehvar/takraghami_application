package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.takraqami.packages.application.Identify.status;

/**
 * Created by Iman on 4/13/2017.
 */

public class NetworkConnection {
    private static Context context;
    private static boolean connected;
    Handler handler;
    private static String status;
    static boolean connect;

    public static boolean isConnected() {
        return connected;
    }

    private static void setConnected(boolean connected) {
        NetworkConnection.connected = connected;
    }

    public boolean check(Context context) {
        new GetStatus().execute();
        NetworkConnection.context = context;
        if (!connected())
        {
            networkErrorDialog();
            return false;
        }
        else if (connected())
        {
            return true;
        }
        return false;
    }


    private static void networkErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("عدم اتصال به اینترنت");
        builder.setMessage("لطفا از اتصال دستگاه خود به شبکه اینترنت مظمئن شوید. سپس دوباره تلاش کنید");
        builder.setNegativeButton("خروج", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                System.exit(0);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }



    private static boolean connected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            return false;
        } else {
            return true;
        }
    }

    public class GetStatus extends AsyncTask<String,Void,Void>
    {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                connect = false;
            }

        @Override
        protected Void doInBackground(String... strings) {
            getData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(status == "connected")
            {
                connect = true;
            }
        }
    }

    private void getData() {
        try {
                String req = URLEncoder.encode("pass", "utf-8") + "=" + URLEncoder.encode("Application", "UTF-8");
                URL url = new URL("http://www.takraqami.ir/Application/WebService.php");
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(req);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                String result = stringBuilder.toString();
                JSONObject json = new JSONObject(result);
                JSONArray data = json.getJSONArray("connection");
                for (int i = 0; i < data.length(); i++) {
                    JSONArray object = data.getJSONArray(i);
                    this.status = object.get(0).toString();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}
