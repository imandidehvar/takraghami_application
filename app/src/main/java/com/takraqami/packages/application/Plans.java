package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Plans extends AppCompatActivity {

    int count,index;
    TextView counttxt,indextxt,day;
    ImageView next,dblnext,last,dbllast;
    AlertDialog dialog;
    ProgressBar progressBar;
    TextView title1,title2,text1,text2;

    ArrayList<type> lst = new ArrayList<type>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        new GetData().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()));

        index = 0;

        indextxt = (TextView) findViewById(R.id.index);
        indextxt.setTypeface(MainActivity.font);
        indextxt.setText("0");

        counttxt = (TextView) findViewById(R.id.count);
        counttxt.setTypeface(MainActivity.font);

        day = (TextView) findViewById(R.id.onvan);
        day.setTypeface(MainActivity.font);


        progressBar = (ProgressBar) findViewById(R.id.progress);

        next = (ImageView) findViewById(R.id.next);
        last = (ImageView) findViewById(R.id.last);
        dbllast = (ImageView) findViewById(R.id.dbllast);
        dblnext = (ImageView) findViewById(R.id.dblnext);

        title1 = (TextView) findViewById(R.id.firsttitle);
        title1.setTypeface(MainActivity.font);
        text1 = (TextView) findViewById(R.id.plan1);
        text1.setTypeface(MainActivity.font);
        title2 = (TextView) findViewById(R.id.secondtitle);
        title2.setTypeface(MainActivity.font);
        text2 = (TextView) findViewById(R.id.plan2);
        text2.setTypeface(MainActivity.font);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index<count-1)
                {
                    index++;
                    loadFlash();
                }
                else
                {
                    Toast.makeText(Plans.this,"پایان فلش کارت ها",Toast.LENGTH_LONG).show();
                }
            }
        });
        last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index>0)
                {
                    index--;
                    loadFlash();
                }
            }
        });
        dblnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((index+5)<count-1)
                {
                    index += 5;
                }
                else
                {
                    index = count-1;
                }
                loadFlash();
            }
        });
        dbllast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((index-5)>=0)
                {
                    index-=5;
                }
                else
                {
                    index = 0;
                }
                loadFlash();
            }
        });
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run()
        {
            progressBar.setMax(count-1);
            progressBar.setProgress(index);
            progressBar.postDelayed(this,1000);
        }
    };

    public void loadFlash()
    {

        indextxt.setText(String.valueOf(index+1));
        day.setText("روز" + " " + String.valueOf(index+1));
        counttxt.setText(String.valueOf(count));

        progressBar.post(runnable);

        title1.setText(lst.get(index).title1);
        text1.setText(lst.get(index).text1);
        title2.setText(lst.get(index).title2);
        text2.setText(lst.get(index).text2);

        if(index > 5 && !IntroActivity.manager.getAccount_mode().equals("Activated")) {
            if (IntroActivity.manager.getAccount_mode().equals("Guest")) {
                dialog("محدودیت نسخه مهمان", "برای دسترسی به تمام قابلیت ها لطفا نسخه کامل را تهیه فرمایید",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(Plans.this, MainActivity.class));
                                ActivitySplash.activity.finish();
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        }, "ثبت نام", "بازگشت");
            }

            if (IntroActivity.manager.getAccount_mode().equals("Free") || IntroActivity.manager.getAccount_mode().equals("Registered")) {
                dialog("محدودیت نسخه", "برای دسترسی به تمام قابلیت ها لطفا نسخه کامل را تهیه فرمایید",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(Plans.this, BuyIntent.class));
                            }},
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        }
                        , "خرید نسخه کامل", "بازگشت");
            }
        }
    }




    public class GetData extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Plans.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            count = lst.size();
            loadFlash();
            dialog.dismiss();
        }
    }

    private void getData(String major) {
        try {
            URL url = new URL("http://takraqami.ir/Application/Get_Plan_List.php");
            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(major,"UTF-8");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Log.d("Test=>", stringBuilder.toString());
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("plan");
            for (int i = 0; i < data.length(); i++) {
                JSONArray object = data.getJSONArray(i);
                lst.add(new type(object));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

    class type
    {
        String title1;
        String text1;
        String title2;
        String text2;

        public type(JSONArray object)
        {
            try{

                title1 = object.get(0).toString();
                text1 = object.get(1).toString();
                title2 = object.get(2).toString();
                text2 = object.get(3).toString();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
