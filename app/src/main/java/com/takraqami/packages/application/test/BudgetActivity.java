package com.takraqami.packages.application.test;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Priority;
import com.coolerfall.download.URLDownloader;
import com.takraqami.packages.application.BuildConfig;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.R;

import java.io.File;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class BudgetActivity extends AppCompatActivity {

    public static final String RIAZI_PDF_LINK = "http://takraqami.ir/Application/Budget/Mathematic.pdf";
    public static final String TAJROBI_PDF_LINK = "http://takraqami.ir/Application/Budget/Empiric.pdf";
    public static final String ENSANI_PDF_LINK = "http://takraqami.ir/Application/Budget/Humanity.pdf";
    public static final String OMOOMI_PDF_LINK = "http://takraqami.ir/Application/Budget/General.pdf";

    public static final String FILE_STORAGE_PATH = "budgets";

    public static final String PREF_KEY_RIAZI_FILE_ADDRESS = "riazi_file_address@BudgetActivity";
    public static final String PREF_KEY_RIAZI_VERSION = "riazi_version@BudgetActivity";
    public static final String PREF_KEY_TAJROBI_FILE_ADDRESS = "tajrobi_file_address@BudgetActivity";
    public static final String PREF_KEY_TAJROBI_VERSION = "riazi_version@BudgetActivity";
    public static final String PREF_KEY_ENSANI_FILE_ADDRESS = "ensani_file_address@BudgetActivity";
    public static final String PREF_KEY_ENSANI_VERSION = "riazi_version@BudgetActivity";
    public static final String PREF_KEY_OMOOMI_FILE_ADDRESS = "omoomi_file_address@BudgetActivity";
    public static final String PREF_KEY_OMOOMI_VERSION = "riazi_version@BudgetActivity";

    SharedPreferences pref;
    DownloadManager downloadManager;
    String fileUrl;
    String prefAddressKey;
    String prefVersionKey;
    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        pref = getSharedPreferences("SHARED",MODE_PRIVATE);
        downloadManager = new DownloadManager.Builder().context(this)
                .downloader(URLDownloader.create())
                .build();
    }

    public void riaziOnClick(View v){
        if(!IntroActivity.manager.getAccount_mode().equals("Activated"))
        {
            Toast.makeText(this,"جهت استفاده از این منابع نسخه کامل را خریداری نمایید",Toast.LENGTH_LONG).show();
        }
        else
        {
            msg=getResources().getString(R.string.budget_downloading_riazi);
            fileUrl=RIAZI_PDF_LINK;
            prefAddressKey=PREF_KEY_RIAZI_FILE_ADDRESS;
            prefVersionKey=PREF_KEY_RIAZI_VERSION;
            checkPermission();
        }
    }

    public void ensaniOnClick(View v){
        if(!IntroActivity.manager.getAccount_mode().equals("Activated"))
        {
            Toast.makeText(this,"جهت استفاده از این منابع نسخه کامل را خریداری نمایید",Toast.LENGTH_LONG).show();
        }
        else
        {
            msg = getResources().getString(R.string.budget_downloading_ensani);
            fileUrl = ENSANI_PDF_LINK;
            prefAddressKey = PREF_KEY_ENSANI_FILE_ADDRESS;
            prefVersionKey = PREF_KEY_ENSANI_VERSION;
            checkPermission();
        }
    }

    public void tajrobiOnClick(View v){
        if(!IntroActivity.manager.getAccount_mode().equals("Activated"))
        {
            Toast.makeText(this,"جهت استفاده از این منابع نسخه کامل را خریداری نمایید",Toast.LENGTH_LONG).show();
        }
        else
        {
            msg = getResources().getString(R.string.budget_downloading_tajrobi);
            fileUrl = TAJROBI_PDF_LINK;
            prefAddressKey = PREF_KEY_TAJROBI_FILE_ADDRESS;
            prefVersionKey = PREF_KEY_TAJROBI_VERSION;
            checkPermission();
        }
    }

    public void omoomiOnClick(View v){
        msg=getResources().getString(R.string.budget_downloading_omoomi);
        fileUrl=OMOOMI_PDF_LINK;
        prefAddressKey=PREF_KEY_OMOOMI_FILE_ADDRESS;
        prefVersionKey=PREF_KEY_OMOOMI_VERSION;
        checkPermission();
    }

    public void checkPermission(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            LinkedList<String> perms = new LinkedList<>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (!perms.isEmpty()) {
                ActivityCompat.requestPermissions(this, perms.toArray(new String[]{}), 101);
                return;
            }
        }
        action();
    }

    public void action(){
        String path=pref.getString(prefAddressKey,"none");
        int version = pref.getInt(prefVersionKey,0);
        if(path.equals("none") || !(new File(path)).exists() || version< BuildConfig.VERSION_CODE){
            if(availableInternet()){
                sendRequest();
            } else {
                Toast.makeText(BudgetActivity.this,R.string.no_internet,Toast.LENGTH_LONG).show();
            }
        } else {
            showFile();
        }
    }

    public boolean availableInternet(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    
    public void sendRequest(){

        //delete previous
        if(!pref.getString(prefAddressKey,"none").equals("none")){
            File previousFile = new File(pref.getString(prefAddressKey,""));
            previousFile.delete();
        }

        //show dialog
        final ProgressDialog dialog;
        dialog = new ProgressDialog(BudgetActivity.this);
        dialog.setMessage(msg);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadManager.cancelAll();
            }
        });
        dialog.show();
        
        //send request
        DownloadRequest request = new DownloadRequest.Builder()
                .url(fileUrl)
                .retryTime(5)
                .retryInterval(2, TimeUnit.SECONDS)
                .progressInterval(1, TimeUnit.SECONDS)
                .priority(Priority.HIGH)
                .destinationDirectory(getExternalFilesDir(null)+ File.separator+FILE_STORAGE_PATH)
                .downloadCallback(new DownloadCallback() {

                    @Override
                    public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                        dialog.setIndeterminate(false);
                        dialog.setMax((int)totalBytes/1024);
                        dialog.setProgress((int)bytesWritten/1024);
                    }

                    @Override public void onSuccess(int downloadId, String filePath) {
                        pref.edit().putString(prefAddressKey,filePath).apply();
                        pref.edit().putInt(prefVersionKey,BuildConfig.VERSION_CODE).apply();
                        dialog.dismiss();
                        showFile();
                    }

                    @Override public void onFailure(int downloadId, int statusCode, String errMsg) {
                        dialog.dismiss();
                        //System.out.println(errMsg);
                        Toast.makeText(BudgetActivity.this,R.string.download_failed,Toast.LENGTH_LONG).show();
                    }
                })
                .build();
        downloadManager.add(request);
    }

    public void showFile(){
        try{
            Uri path;
            File pdfFile = new File(pref.getString(prefAddressKey,""));
            if(Build.VERSION.SDK_INT >= 24){
                path = FileProvider.getUriForFile(getApplicationContext()
                        , getPackageName() + ".provider", pdfFile);
            } else{
                path = Uri.fromFile(pdfFile);
            }
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(BudgetActivity.this, R.string.no_pdf, Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(BudgetActivity.this,  e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for(int result:grantResults){
                    if(result != PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(BudgetActivity.this,R.string.no_permission,Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                action();
            }
        }
    }

}
