package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.takraqami.packages.application.test.Calendar.DateConverter;
import com.takraqami.packages.application.test.Database.MyDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Karnameh extends AppCompatActivity {

    ListView list;
    String konkur_id;
    private Dialog dialog;
    int avg;
    String lessons = "";
    String members,rotbe = "";
    ArrayList<Lessons> lessonsList;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karnameh);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        list = (ListView) findViewById(R.id.percents);
        if(getIntent().hasExtra("konkur_id"))
        {
            konkur_id = getIntent().getExtras().get("konkur_id").toString();
        }
        avg = 0;

        if(!getIntent().hasExtra("Mode"))
        {
            ((LinearLayout) findViewById(R.id.karnameh)).setVisibility(View.INVISIBLE);
            calculate();
            for (int i = 0; i < Karnameh_item.answered.size(); i++)
            {
                lessons += Karnameh_item.answered.get(i).name + ":" + Karnameh_item.answered.get(i).percent + ",";
            }
            new SetKarnameh().execute(IntroActivity.manager.getStudent_id().toString(),String.valueOf(konkur_id),String.valueOf(avg),lessons);
            list.setAdapter(new Adapter());
        }
        else
        {
            ((TextView) findViewById(R.id.text1)).setTypeface(MainActivity.font);
            ((TextView) findViewById(R.id.text1)).setText("رتبه شما");
            ((TextView) findViewById(R.id.rotbeh)).setTypeface(MainActivity.font);
            new GetKarnameh().execute(IntroActivity.manager.getStudent_id().toString(),String.valueOf(konkur_id));
        }
    }

    class Adapter extends BaseAdapter {
        @Override
        public int getCount() {
            return Karnameh_item.answered.size();
        }

        @Override
        public Object getItem(int i) {
            return Karnameh_item.answered.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View item = LayoutInflater.from(Karnameh.this).inflate(R.layout.karnameh_item,viewGroup,false);
            TextView tv = (TextView) item.findViewById(R.id.percent);
            ProgressBar percentbar = (ProgressBar) item.findViewById(R.id.percentbar);
            tv.setTypeface(MainActivity.font);
            tv.setText(Karnameh_item.answered.get(i).name + ": " + (int)(Karnameh_item.answered.get(i).percent) + "%");
            percentbar.setProgress((int)(Karnameh_item.answered.get(i).percent));
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Karnameh.this,Pasokhnameh.class);
                    intent.putExtra("id",i);
                    startActivity(intent);
                }
            });
            return item;
        }
    }

    public void  calculate()
    {
        int factors = 0;
        for (int i = 0; i < Karnameh_item.answered.size(); i++)
        {
            avg += ((Karnameh_item.answered.get(i).percent)*Karnameh_item.answered.get(i).factor);
            factors += Karnameh_item.answered.get(i).factor;
        }
        avg = avg/factors;
    }

    /*public void calculate(String percent)
    {
        String major = "", database = "";
        switch (IntroActivity.manager.getStudent_major_id())
        {
            case 1:
                major = "riazi";
                database = MyDatabase.RIAZI_DATABASE_NAME;
                break;
            case 2:
                major = "tajrobi";
                database = MyDatabase.TAJROBI_DATABASE_NAME;
                break;
            case 3:
                major = "ensani";
                database = MyDatabase.ENSANI_DATABASE_NAME;
                break;
        }

        Cursor c = new MyDatabase(this,database).getReadableDatabase()
                .query(major,new String[]{"rank"},"percent <= ?", new String[]{String.valueOf(avg)}
                        , null, null, "rank ASC");
        int first=30001;
        int second=30001;
        int i=0;
        if (c != null && c.moveToFirst()) {
            do{
                if(i==0){
                    first = c.getInt(0);
                } else {
                    second = c.getInt(0);
                    break;
                }
                i++;
            } while (c.moveToNext());
        }
        if(c!=null){
            c.close();
        }
        if(second!=30001){
            return String.valueOf(first)+" تا "+String.valueOf(second);
        }
        return " بالاتر از 30000";
    }
*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Karnameh_item.answered.clear();
        IntroActivity.manager.addNewKonkur(konkur_id);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, String btn1) {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_soon, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setText(btn1);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
    }

    String response;
    class SetKarnameh extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog = new ProgressDialog(Karnameh.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setTitle("درحال ثبت اطلاعات کارنامه");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("لطفا صبر کنید");
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            setKarnameh(strings[0],strings[1],strings[2],strings[3]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Snackbar.make(findViewById(android.R.id.content),"جهت مشاهده کارنامه کلی پس از پایان ازمون مجدد وارد ان شوید",Snackbar.LENGTH_LONG).show();
            dialog.dismiss();
        }
    }

    private void setKarnameh(String student, String konkur, String percent,String lessons)
    {
        try
        {
            String request = URLEncoder.encode("student","utf-8")+"="+URLEncoder.encode(student,"UTF-8");
            request += "&" + URLEncoder.encode("konkur","utf-8")+"="+URLEncoder.encode(konkur,"UTF-8");
            request += "&" + URLEncoder.encode("percent","utf-8")+"="+URLEncoder.encode(percent,"UTF-8");
            request += "&" + URLEncoder.encode("lessons","utf-8")+"="+URLEncoder.encode(lessons,"UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/SetKarnameh.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(request);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("res");
            JSONArray object = data.getJSONArray(0);
            response = object.get(0).toString();
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    class AdapterOnline extends BaseAdapter {
        @Override
        public int getCount() {
            return lessonsList.size()-1;
        }

        @Override
        public Object getItem(int i) {
            return lessonsList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View item = LayoutInflater.from(Karnameh.this).inflate(R.layout.karnameh_item,viewGroup,false);
            TextView tv = (TextView) item.findViewById(R.id.percent);
            ProgressBar percentbar = (ProgressBar) item.findViewById(R.id.percentbar);
            tv.setTypeface(MainActivity.font);
            tv.setText(lessonsList.get(i).name + ": " + (lessonsList.get(i).percent) + "%");
            percentbar.setProgress(Integer.parseInt(lessonsList.get(i).percent));
            return item;
        }
    }


    class GetKarnameh extends AsyncTask<String,Void,Void>
    {

        @Override
        protected Void doInBackground(String... strings) {
            getKarnameh(strings[0],strings[1]);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            list.setAdapter(new AdapterOnline());
            ((TextView) findViewById(R.id.rotbeh)).setText(rotbe + "\n" + "تعداد کل شرکت کنندگان:" + members);
        }
    }

    private void getKarnameh(String student, String konkur)
    {
        try
        {
            lessonsList = new ArrayList<Lessons>();
            String request = URLEncoder.encode("student","utf-8")+"="+URLEncoder.encode(student,"UTF-8");
            request += "&" + URLEncoder.encode("konkur","utf-8")+"="+URLEncoder.encode(konkur,"UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/GetKarnameh.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(request);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("karnameh");
            JSONArray object = data.getJSONArray(0);
            members = object.get(0).toString();
            rotbe = object.get(1).toString();


            JSONArray lessons = json.getJSONArray("lessons");
            for (int i = 0; i < lessons.length(); i++) {
                JSONArray lessonsobj = lessons.getJSONArray(i);
                lessonsList.add(new Lessons(lessonsobj));
            }

        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    private class Lessons
    {
        String name;
        String percent;

        public Lessons(JSONArray object) throws JSONException {
            name = object.get(0).toString();
            percent = object.get(1).toString();
        }
    }
}
