package com.takraqami.packages.application.classroom;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Priority;
import com.coolerfall.download.URLDownloader;
import com.takraqami.packages.application.BuildConfig;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;
import com.takraqami.packages.application.classroom.Database.VideoDB;
import com.takraqami.packages.application.test.BudgetActivity;
import com.takraqami.packages.application.test.Database.DBHelper;
import com.takraqami.packages.application.test.Event;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import static android.R.attr.version;

public class PlayVideoActivity extends AppCompatActivity {

    public static final String FILE_STORAGE_PATH = "videos";

    private ClassItem classItem;

    LinearLayout buttonLayout;
    FrameLayout videoLayout;
    LinearLayout mainLayout;

    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;

    int selectedQuality;
    DownloadManager downloadManager;
    VideoDB db;

    boolean availableHd;
    boolean availableNormal;
    String hdFile;
    String normalFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);

        downloadManager = new DownloadManager.Builder().context(this)
                .downloader(URLDownloader.create())
                .build();
        db = new VideoDB(this);

        classItem = new ClassItem(getIntent().getStringExtra("id")
                ,getIntent().getStringExtra("title")
                ,getIntent().getStringExtra("category")
                ,getIntent().getStringExtra("size")
                ,getIntent().getStringExtra("duration")
                ,getIntent().getStringExtra("pic_location")
                ,getIntent().getStringExtra("content_location")
                ,getIntent().getStringExtra("hd_location"));

        ((TextView) findViewById(R.id.qualityText)).setTypeface(MainActivity.font);
        ((TextView) findViewById(R.id.downloadText)).setTypeface(MainActivity.font);

        mainLayout = (LinearLayout) findViewById(R.id.titleLayout);
        videoLayout = (FrameLayout) findViewById(R.id.video_layout);
        buttonLayout = (LinearLayout) findViewById(R.id.button_layout);

        mVideoView = (UniversalVideoView) findViewById(R.id.video);
        mMediaController = (UniversalMediaController) findViewById(R.id.controller);
        mMediaController.setTitle(classItem.title);
        mVideoView.setMediaController(mMediaController);
        mMediaController.show(2000);

        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
            @Override
            public void onScaleChange(boolean isFullscreen) {
                if (isFullscreen) {
                    buttonLayout.setVisibility(View.GONE);

                    //GONE the unconcerned views to leave room for video and controller
                    mMediaController.setVisibility(View.GONE);

                    videoLayout.getLayoutParams().width=ViewGroup.LayoutParams.FILL_PARENT;
                    videoLayout.getLayoutParams().height=ViewGroup.LayoutParams.FILL_PARENT;

                    mainLayout.getLayoutParams().width=ViewGroup.LayoutParams.MATCH_PARENT;
                    mainLayout.getLayoutParams().height=ViewGroup.LayoutParams.MATCH_PARENT;


                } else {
                    buttonLayout.setVisibility(View.VISIBLE);

                    mMediaController.setVisibility(View.VISIBLE);

                    videoLayout.getLayoutParams().width=ViewGroup.LayoutParams.FILL_PARENT;
                    videoLayout.getLayoutParams().height=720;

                    mainLayout.getLayoutParams().width=ViewGroup.LayoutParams.MATCH_PARENT;
                    mainLayout.getLayoutParams().height=ViewGroup.LayoutParams.WRAP_CONTENT;


                }
            }

            @Override
            public void onPause(MediaPlayer mediaPlayer) { // Video pause

            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play

            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading

            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading

            }

        });

        selectedQuality=0;

        checkPermission();
    }

    @Override
    public void onResume(){
        super.onResume();
        mVideoView.resume();
    }

    @Override
    public void onPause(){
        super.onPause();
        mVideoView.pause();
    }

    public void share(View v){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "http://"+classItem.content_location.replaceAll("\\s","%20");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, classItem.title);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "همرسانی"));
    }

    public void quality(View v){
        if(classItem.hd_location!=null && !classItem.hd_location.isEmpty() && !classItem.hd_location.equals("null")){
            CharSequence colors[] = new CharSequence[] {"کیفیت معمولی", "کفیت HD"};

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("انتخاب کیفیت فیلم");
            builder.setItems(colors, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(which!=selectedQuality){
                        selectedQuality=which;
                        checkPermission();
                        mVideoView.start();
                    }
                }
            });
            builder.show();
        } else {
            Toast.makeText(this,"این فیلم فقط با کیفیت معمولی در دسترس می باشد.",Toast.LENGTH_SHORT).show();
        }
    }

    public void download(View v){

        if((selectedQuality==0 && availableNormal) || (selectedQuality==1 && availableHd)){
            Toast.makeText(PlayVideoActivity.this,"فایل قبلا دانلود شده است.",Toast.LENGTH_LONG).show();
            return;
        }

        //show dialog
        final ProgressDialog dialog;
        dialog = new ProgressDialog(this);
        dialog.setMessage("در حال دانلود فیلم "+classItem.title);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadManager.cancelAll();
            }
        });
        dialog.show();

        //send request
        DownloadRequest request = new DownloadRequest.Builder()
                .url("http://"+classItem.content_location.replaceAll("\\s","%20"))
                .retryTime(5)
                .retryInterval(2, TimeUnit.SECONDS)
                .progressInterval(1, TimeUnit.SECONDS)
                .priority(Priority.HIGH)
                .destinationDirectory(getExternalFilesDir(null)+ File.separator+FILE_STORAGE_PATH)
                .downloadCallback(new DownloadCallback() {

                    @Override
                    public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                        dialog.setIndeterminate(false);
                        dialog.setMax((int)totalBytes/1024);
                        dialog.setProgress((int)bytesWritten/1024);
                    }

                    @Override public void onSuccess(int downloadId, String filePath) {
                        if(selectedQuality==0){
                            db.doQry("INSERT INTO video (id,content) VALUES('"+classItem.id+"','"+filePath+"')");
                        } else{
                            db.doQry("INSERT INTO video (id,hd) VALUES('"+classItem.id+"','"+filePath+"')");
                        }
                        dialog.dismiss();
                        mVideoView.setVideoURI(Uri.parse(filePath));
                        selectedQuality=0;
                        mVideoView.start();
                    }

                    @Override public void onFailure(int downloadId, int statusCode, String errMsg) {
                        dialog.dismiss();
                        //System.out.println(errMsg);
                        Toast.makeText(PlayVideoActivity.this,R.string.download_failed,Toast.LENGTH_LONG).show();
                    }
                })
                .build();
        downloadManager.add(request);
    }

    public void checkPermission(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            LinkedList<String> perms = new LinkedList<>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (!perms.isEmpty()) {
                ActivityCompat.requestPermissions(this, perms.toArray(new String[]{}), 101);
                return;
            }
        }
        action();
    }

    public void action(){
        //query
        Cursor c = db.selectQry("SELECT content,hd FROM video WHERE "
                + VideoDB.VideoColumns.ID+" = '"+classItem.id+"'");
        if (c != null && c.moveToFirst()){
            if(!c.isNull(0)){
                availableNormal=true;
                normalFile=c.getString(0);
            } else{
                availableNormal=false;
            }
            if(!c.isNull(1)){
                availableHd=true;
                hdFile=c.getString(0);
            } else{
                availableHd=false;
            }
        }
        if(c!=null){
            c.close();
        }

        if(selectedQuality==0){
            if(!availableNormal || !(new File(normalFile)).exists()){
                if(availableInternet()){
                    mVideoView.setVideoURI(Uri.parse("http://"+classItem.content_location.replaceAll("\\s","%20")));
                    mVideoView.start();
                } else {
                    Toast.makeText(this,R.string.no_internet,Toast.LENGTH_LONG).show();
                }
            } else {
                mVideoView.setVideoURI(Uri.parse(normalFile));
                mVideoView.start();
            }
        } else{
            if(!availableHd || !(new File(hdFile)).exists()){
                if(availableInternet()){
                    mVideoView.setVideoURI(Uri.parse("http://"+classItem.hd_location.replaceAll("\\s","%20")));
                    mVideoView.start();
                } else {
                    Toast.makeText(this,R.string.no_internet,Toast.LENGTH_LONG).show();
                }
            } else {
                mVideoView.setVideoURI(Uri.parse(hdFile));
                mVideoView.start();
            }
        }

    }

    public boolean availableInternet(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101: {
                for(int result:grantResults){
                    if(result != PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(PlayVideoActivity.this,R.string.no_permission,Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                action();
            }
        }
    }
}
