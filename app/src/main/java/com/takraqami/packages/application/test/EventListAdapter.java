package com.takraqami.packages.application.test;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.Database.DBHelper;

import java.util.List;

/**
 * Created by etteh on 5/7/2017.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> {

    private DBHelper dbHelper;
    private List<Event> items;
    private int itemLayout;
    private Context context;

    public EventListAdapter(int itemLayout, Context context, List<Event> items) {
        this.itemLayout = itemLayout;
        this.context=context;
        this.items=items;
        dbHelper=new DBHelper(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Event event = items.get(position);

        //done
        holder.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHelper.doQry("UPDATE plan SET "+DBHelper.PlanColumns.STATUS+" = "+Event.STATUS_DONE
                        +" WHERE "+DBHelper.PlanColumns.ID+" = "+event.id);
                ((PlanningActivity)context).initEvent(((PlanningActivity)context).selectedDate);
            }
        });

        //delete
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setTitle("حذف برنامه")
                        .setMessage("آیا از حذف این برنامه مطمئنید؟")
                        .setPositiveButton("بلی", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                dbHelper.doQry("DELETE FROM plan WHERE "+DBHelper.PlanColumns.ID+" = "+event.id);
                                ((PlanningActivity)context).initEvent(((PlanningActivity)context).selectedDate);
                            }
                        })
                        .setNegativeButton("بی خیال", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        //cancel
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHelper.doQry("UPDATE plan SET "+DBHelper.PlanColumns.STATUS+" = "+Event.STATUS_CANCEL
                        +" WHERE "+DBHelper.PlanColumns.ID+" = "+event.id);
                ((PlanningActivity)context).initEvent(((PlanningActivity)context).selectedDate);
            }
        });

        //title & start
        holder.title.setText(event.title);
        holder.start.setText(event.from.split(" ")[1]);

        holder.eventLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,AddEventActivity.class);
                intent.putExtra(AddEventActivity.EXTRA_ID,event.id);
                context.startActivity(intent);
            }
        });

        switch (event.status){
            case Event.STATUS_DONE:
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventDoneBackground));
                holder.delete.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventDoneBackground));
                holder.done.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventDoneBackground));
                holder.cancel.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventDoneBackground));
                break;
            case Event.STATUS_CANCEL:
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventCancelBackground));
                holder.delete.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventCancelBackground));
                holder.done.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventCancelBackground));
                holder.cancel.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventCancelBackground));
                break;
            default:
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventNormalBackground));
                holder.delete.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventNormalBackground));
                holder.done.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventNormalBackground));
                holder.cancel.setBackgroundColor(context.getResources().getColor(R.color.colorPlanningEventNormalBackground));
        }

        holder.itemView.setTag(event);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageButton done;
        public ImageButton cancel;
        public ImageButton delete;
        public LinearLayout eventLayout;
        public TextView title;
        public TextView start;

        public ViewHolder(View itemView) {
            super(itemView);
            done = (ImageButton) itemView.findViewById(R.id.doneEvent);
            cancel = (ImageButton) itemView.findViewById(R.id.cancelEvent);
            delete = (ImageButton) itemView.findViewById(R.id.deleteEvent);
            eventLayout = (LinearLayout) itemView.findViewById(R.id.eventLayout);
            title = (TextView) itemView.findViewById(R.id.eventTitle);
            start = (TextView) itemView.findViewById(R.id.eventStart);
        }
    }
}
