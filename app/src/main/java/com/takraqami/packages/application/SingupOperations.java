package com.takraqami.packages.application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by Iman on 4/26/2017.
 */

public class SingupOperations
{
    public static String response;
    public static String id;

    public void singup(Account account) {
        try
        {
            String request = URLEncoder.encode("name","utf-8")+"="+URLEncoder.encode(account.getFamily(),"UTF-8");
            request += "&" + URLEncoder.encode("phone","utf-8")+"="+URLEncoder.encode(account.getPhone(),"UTF-8");
            request += "&" + URLEncoder.encode("major","utf-8")+"="+URLEncoder.encode(account.getMajor(),"UTF-8");


            URL url = new URL("http://www.takraqami.ir/Application/Singup.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(request);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("res");
            JSONArray object = data.getJSONArray(0);
            response = object.get(0).toString();
            if(object.length() > 1)
            {
                id = object.getString(1).toString();
            }
    }
    catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }
}
