package com.takraqami.packages.application.test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.Calendar.Day;

import java.util.List;

/**
 * Created by etteh on 5/8/2017.
 */

public class CalendarGridAdapter extends BaseAdapter {

    private Context context;
    private List<Day> days;
    private int lastClicked=-1;

    public CalendarGridAdapter(Context context, List<Day> days) {
        this.context = context;
        this.days=days;
    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public Object getItem(int position) {
        return days.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        final Day day = (Day) getItem(position);

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(context);
            v = vi.inflate(R.layout.calendar_cell, null);
        }

        TextView text = (TextView) v.findViewById(R.id.cell);
        text.setText(day.dateStr);
        v.setBackground(context.getResources().getDrawable(R.color.colorPlanningBackground));
        if(text.getText().toString().isEmpty()){
            return v;
        }
        text.setTextColor(context.getResources().getColor(R.color.colorPlanningSecondText));

        if(day.holiday){
            text.setTextColor(context.getResources().getColor(R.color.colorPlanningText));
            v.setBackground(context.getResources().getDrawable(R.drawable.holiday_text_background));
        }
        if(day.today){
            text.setTextColor(context.getResources().getColor(R.color.colorPlanningSecondText));
            v.setBackground(context.getResources().getDrawable(R.drawable.today_text_background));
        }
        if(day.selected){
            text.setTextColor(context.getResources().getColor(R.color.colorPlanningSecondText));
            v.setBackground(context.getResources().getDrawable(R.drawable.select_day_text_background));
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lastClicked==position){
                    return;
                }

                if(lastClicked>=0){
                    ((Day)getItem(lastClicked)).selected=false;
                }
                ((Day)getItem(position)).selected=true;
                lastClicked=position;
                ((PlanningActivity)context).initEvent(day.date);
                ((PlanningActivity)context).selectedDate=day.date.clone();
                notifyDataSetChanged();
            }
        });

        return v;
    }

}
