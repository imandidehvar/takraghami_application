package com.takraqami.packages.application.test;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.takraqami.packages.application.R;

/**
 * Created by etteh on 5/9/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {

    public static final String EXTRA_MSG = "msg_extra";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(context.getResources().getString(R.string.add_event_notification_title))
                        .setContentText(intent.getStringExtra(EXTRA_MSG));
        Intent resultIntent = new Intent(context, PlanningActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }
}
