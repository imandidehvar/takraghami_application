package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaghalehShow extends AppCompatActivity {

    String maghaleh_id;
    Maghaleh maghaleh;
    TextView date,category,likesCount,disLikeCount;
    WebView matn;
    LinearLayout like,dislike;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maghaleh_show);

        date = (TextView) findViewById(R.id.date);
        date.setTypeface(MainActivity.font);
        category = (TextView) findViewById(R.id.category);
        category.setTypeface(MainActivity.font);
        likesCount = (TextView) findViewById(R.id.liketext);
        likesCount.setTypeface(MainActivity.font);
        disLikeCount = (TextView) findViewById(R.id.disliketext);
        disLikeCount.setTypeface(MainActivity.font);
        matn = (WebView) findViewById(R.id.matn);
        matn.setBackgroundColor(Color.TRANSPARENT);
        matn.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        matn.setLongClickable(false);
        like = (LinearLayout) findViewById(R.id.like);
        dislike = (LinearLayout) findViewById(R.id.dislike);

        maghaleh_id = getIntent().getExtras().get("maghaleh_id").toString();

        new GetData().execute(maghaleh_id,IntroActivity.manager.getStudent_id());

    }

    class GetData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MaghalehShow.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0],strings[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            date.setText(maghaleh.date);
            category.setText(maghaleh.category);
            likesCount.setText(maghaleh.likes);
            disLikeCount.setText(maghaleh.disLikes);
            matn.loadUrl(maghaleh.matn);

            if(maghaleh.status.equals("0"))
            {
                dislike.setEnabled(true);
                like.setEnabled(true);
            }
            else if(maghaleh.status.equals("1"))
            {
                dislike.setEnabled(true);
                like.setEnabled(false);
            }
            else if (maghaleh.status.equals("2"))
            {
                dislike.setEnabled(false);
                like.setEnabled(true);
            }

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(maghaleh.status.equals("0"))
                    {
                        new like().execute("1",maghaleh_id,IntroActivity.manager.getStudent_id(),"new");
                    }
                    else if(maghaleh.status.equals("2"))
                    {
                        new like().execute("1",maghaleh_id,IntroActivity.manager.getStudent_id(),"update");
                    }
                }
            });

            dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(maghaleh.status.equals("0"))
                    {
                        new like().execute("2",maghaleh_id,IntroActivity.manager.getStudent_id(),"new");
                    }
                    else if(maghaleh.status.equals("1"))
                    {
                        new like().execute("2",maghaleh_id,IntroActivity.manager.getStudent_id(),"update");
                    }

                }
            });
            dialog.dismiss();
        }
    }

    class like extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MaghalehShow.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings)
        {
            like(strings[0],strings[1],strings[2],strings[3]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new GetData().execute(maghaleh_id,IntroActivity.manager.getStudent_id());
            dialog.dismiss();
        }
    }

    public void like(String status,String maghaleh_id, String student_id,String mode)
    {
        try
        {
            String req = URLEncoder.encode("status","UTF-8") + "=" + URLEncoder.encode(status,"UTF-8");
            req += "&" + URLEncoder.encode("maghaleh_id","utf-8") + "=" + URLEncoder.encode(maghaleh_id,"UTF-8");
            req += "&" + URLEncoder.encode("student_id","utf-8") + "=" + URLEncoder.encode(student_id,"UTF-8");
            req += "&" + URLEncoder.encode("mode","utf-8") + "=" + URLEncoder.encode(mode,"UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/like.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Log.d("Test",stringBuilder.toString());
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("maghaleh");
            JSONArray object = data.getJSONArray(0);
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    private void getData(String maghaleh_id, String student_id)
    {
        try
        {
            String req = URLEncoder.encode("maghaleh_id","utf-8") + "=" + URLEncoder.encode(maghaleh_id,"UTF-8");
            req += "&" + URLEncoder.encode("student_id","utf-8") + "=" + URLEncoder.encode(student_id,"UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/Get_Maghaleh.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Log.d("Test",stringBuilder.toString());
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("maghaleh");
                JSONArray object = data.getJSONArray(0);
                maghaleh = new Maghaleh(object);
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }


    class Maghaleh
    {
        String id;
        String date;
        String title;
        String category;
        String matn;
        String picture;
        String likes;
        String disLikes;
        String status;

        public Maghaleh(JSONArray object) {
            try {
                this.id = object.get(0).toString();
                this.date = object.get(1).toString();
                this.title = object.get(2).toString();
                this.category = object.get(3).toString();
                this.matn = object.get(4).toString();
                this.picture =object.get(5).toString();
                this.likes = object.get(6).toString();
                this.disLikes = object.get(7).toString();
                this.status = object.get(8).toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
