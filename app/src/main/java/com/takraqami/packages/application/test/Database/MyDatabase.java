package com.takraqami.packages.application.test.Database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by etteh on 5/15/2017.
 */

public class MyDatabase extends SQLiteAssetHelper {

    public static final String RIAZI_DATABASE_NAME = "riazi.sqlite";
    public static final String ENSANI_DATABASE_NAME = "ensani.sqlite";
    public static final String TAJROBI_DATABASE_NAME = "tajrobi.sqlite";

    private static final int DATABASE_VERSION = 1;

    public MyDatabase(Context context, String dbName) {
        super(context, dbName, null, DATABASE_VERSION);
    }
}
