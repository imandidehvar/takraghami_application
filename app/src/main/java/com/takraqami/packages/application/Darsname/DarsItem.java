package com.takraqami.packages.application.Darsname;

import org.json.JSONObject;

/**
 * Created by Majid Ettehadi on 09/14/2017.
 */

public class DarsItem {

    String id;
    String title;
    String category;
    String size;
    String page_count;
    String pic_location;
    String content_location;

    public DarsItem(String id, String title, String category, String size
            , String page_count, String pic_location, String content_location){
        this.id=id;
        this.title=title;
        this.category=category;
        this.size=size;
        this.page_count=page_count;
        this.pic_location=pic_location;
        this.content_location=content_location;
    }

    public static DarsItem fromJson(JSONObject json){
        try{
            return new DarsItem(json.getString("id")
                    , json.getString("title")
                    , json.getString("category")
                    , json.getString("size")
                    , json.getString("page_count")
                    , json.getString("pic_location")
                    , json.getString("content_location"));
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
