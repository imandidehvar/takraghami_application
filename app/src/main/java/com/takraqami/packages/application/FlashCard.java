package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class FlashCard extends AppCompatActivity {

    ArrayList<FlashItem> list;
    ListView lst;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_card);
        list = new ArrayList<FlashItem>();
        lst = (ListView) findViewById(R.id.list);
        ((TextView) findViewById(R.id.formtitle)).setTypeface(MainActivity.font);
        new GetData().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()));

    }

    public class GetData extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(FlashCard.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            lst.setAdapter(new Adapter());
            dialog.dismiss();
        }
    }

    private void getData(String major) {
        try {
            URL url = new URL("http://takraqami.ir/Application/Get_Flashcard_List.php");
            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(major,"UTF-8");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
                Log.d("Test",stringBuilder.toString());
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("flashcard");
            for (int i = 0; i < data.length(); i++) {
                JSONArray object = data.getJSONArray(i);
                list.add(new FlashItem(object));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    class Adapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {
            return list.size();
        }

        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View view = LayoutInflater.from(FlashCard.this).inflate(R.layout.flashcard_lst_item,parent,false);
            ImageView background = (ImageView) view.findViewById(R.id.background);
            TextView title = (TextView) view.findViewById(R.id.flashtitle);
            title.setTypeface(MainActivity.font);
            TextView count = (TextView) view.findViewById(R.id.flashcount);
            count.setTypeface(MainActivity.font);
            LinearLayout button = (LinearLayout) view.findViewById(R.id.buttonmoror);
            Picasso.with(FlashCard.this).load(list.get(position).getImage()).into(background);
            title.setText(list.get(position).getTitle().toString());
            count.setText("تعداد فلش کارت: " + list.get(position).count.toString());
            ((TextView) view.findViewById(R.id.moror)).setTypeface(MainActivity.font);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(FlashCard.this,flashcards.class);
                    intent.putExtra("id",list.get(position).id);
                    intent.putExtra("name",list.get(position).title);
                    intent.putExtra("count",list.get(position).count);
                    intent.putExtra("link",list.get(position).image);
                    startActivity(intent);
                }
            });
            return view;
        }
    }

    class FlashItem
    {
        String id;
        String title;
        String count;
        String image;

        public FlashItem(JSONArray object)
        {
            try
            {
                id = object.get(0).toString();
                title = object.get(1).toString();
                count = object.get(2).toString();
                image = object.get(3).toString();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getCount() {
            return count;
        }

        public String getImage() {
            return image + ".jpg";
        }
    }
}
