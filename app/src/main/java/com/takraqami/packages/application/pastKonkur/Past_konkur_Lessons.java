package com.takraqami.packages.application.pastKonkur;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.takraqami.packages.application.Azmoon_Karnameh;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.Karnameh_item;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Past_konkur_Lessons extends AppCompatActivity {

    private ArrayList<Lesson> special;
    private ArrayList<Lesson> general;

    private ArrayList<Lesson> lessons;

    private Map<String,List<Lesson>> list;

    Button btnKarnameh;
    Handler handler;
    private AlertDialog dialogV;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_konkur__lessons);

        TextView tv = (TextView) findViewById(R.id.txttitle);
        tv.setTypeface(MainActivity.font);

        String book = String.valueOf(getIntent().getExtras().getInt("lessonId"));

        new GetData().execute(String.valueOf(IntroActivity.manager.getStudent_major_id()),book);

        general = new ArrayList<Lesson>();
        special = new ArrayList<Lesson>();

        btnKarnameh = (Button) findViewById(R.id.showKarnameh);
        btnKarnameh.setTypeface(MainActivity.font);
        btnKarnameh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (Karnameh_item.answered.size() <= 0) {
                    for (int i = 0; i < lessons.size(); i++) {
                        Karnameh_item.answered.add(new Karnameh_item(lessons.get(i).title, String.valueOf(i), 0,false));
                    }
                    Intent intent = new Intent(Past_konkur_Lessons.this, Azmoon_Karnameh.class);
                    startActivity(intent);
                    finish();
                } else {
                    for (int i = 0; i < lessons.size(); i++)
                    {
                        boolean flag = false;
                        for (int j = 0; j < Karnameh_item.answered.size(); j++)
                        {
                            if(Karnameh_item.answered.get(j).name.equals(lessons.get(i).title))
                            {
                                flag = true;
                                break;
                            }
                        }
                        if(flag == false)
                        {
                            Karnameh_item.answered.add(new Karnameh_item(lessons.get(i).title, String.valueOf(i), 0,false));
                        }
                    }
                    Intent intent = new Intent(Past_konkur_Lessons.this, Azmoon_Karnameh.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        dialog("پایان آزمون", "آیا میخواهید به آزمون خاتمه دهید؟",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Karnameh_item.answered.size() <= 0) {
                            for (int i = 0; i < lessons.size(); i++) {
                                Karnameh_item.answered.add(new Karnameh_item(lessons.get(i).title, String.valueOf(i), 0,false));
                            }
                            Intent intent = new Intent(Past_konkur_Lessons.this, Azmoon_Karnameh.class);
                            startActivity(intent);
                            finish();
                        } else {
                            for (int i = 0; i < lessons.size(); i++) {
                                boolean flag = false;
                                for (int j = 0; j < Karnameh_item.answered.size(); j++) {
                                    if (Karnameh_item.answered.get(j).name.equals(lessons.get(i).title.toString())) {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (flag == false) {
                                    Karnameh_item.answered.add(new Karnameh_item(lessons.get(i).title, String.valueOf(i), 0, false));
                                }
                            }
                            Intent intent = new Intent(Past_konkur_Lessons.this, Azmoon_Karnameh.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                },"ادامه آزمون","مشاهده ی کارنامه");
    }

    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, View.OnClickListener onCancelKeyPress, String btn1, String btn2) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog, parent, false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        Button cancel = (Button) view.findViewById(R.id.cancel);
        ok.setText(btn1);
        cancel.setText(btn2);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(onOkKeyPress);
        cancel.setOnClickListener(onCancelKeyPress);
    }

    class ExpandableAdapter extends ExpandableRecyclerAdapter<GroupName, Lesson, ParentView, ChildView>
    {
        Context mContext;
        List<GroupName> list;

        public ExpandableAdapter(Context context, List<GroupName> parentItemList)
        {
            super(parentItemList);
            mContext = context;
            list = parentItemList;
        }

        @Override
        public ParentView onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType)
        {
            View view = LayoutInflater.from(mContext).inflate(R.layout.test_tamrin_item_header, parentViewGroup, false);
            return new ParentView(view);
        }

        @Override
        public ChildView onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType)
        {
            View view = LayoutInflater.from(mContext).inflate(R.layout.test_tamrin_item_child, childViewGroup, false);
            return new ChildView(view,childViewGroup);
        }

        @Override
        public void onBindParentViewHolder(final ParentView parentViewHolder, final int parentPosition, final GroupName parent)
        {
            parentViewHolder.bind(parent);
        }

        @Override
        public void onBindChildViewHolder(ChildView childViewHolder, int parentPosition, int childPosition, @NonNull Lesson child)
        {
            childViewHolder.bind(child);
        }

    }
    class ParentView extends ParentViewHolder
    {

        public TextView headerText, timer;
        public ImageView mParentDropDownArrow;
        public CardView mLayout;
        boolean status;

        public ParentView(View itemView) {
            super(itemView);
            status = false;
            headerText = (TextView) itemView.findViewById(R.id.parent_list_item_crime_title_text_view);
            mParentDropDownArrow = (ImageView) itemView.findViewById(R.id.parent_list_item_expand_arrow);
            mLayout = (CardView) itemView.findViewById(R.id.expandable_header);
            headerText.setTypeface(MainActivity.font);
            mLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(status == false)
                    {
                        status = true;
                        mParentDropDownArrow.setBackgroundResource(R.drawable.ic_arrow_drop_up_white);
                    }
                    else
                    {
                        status = false;
                        mParentDropDownArrow.setBackgroundResource(R.drawable.ic_arrow_drop_down_white);
                    }
                }
            });
        }

        public void bind(GroupName name)
        {
            headerText.setText(name.name);
            mLayout.setBackgroundResource(name.color);
        }
    }

    class ChildView extends ChildViewHolder {

        public Button bookname;
        public Lesson book;
        public ViewGroup root;
        public ChildView(final View itemView, final ViewGroup rootView) {
            super(itemView);
            bookname = (Button) itemView.findViewById(R.id.child_list_item_crime_date_text_view);
            bookname.setTypeface(MainActivity.font);
            root = rootView;
            bookname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!checkBook(book.title))
                    {
                        //Book.book = book;
                        Intent intent = new Intent(Past_konkur_Lessons.this,Past_Konkur_Question.class);
                        intent.putExtra("address",book.link + "/" + book.konkur + "/" + book.lesson);
                        intent.putExtra("count",book.questioncount);
                        intent.putExtra("name",book.title);
                        intent.putExtra("book_id",String.valueOf(book.lesson));
                        intent.putExtra("konkur_id",String.valueOf(book.konkur));

                        startActivity(intent);
                    }
                    else
                    {
                        Snackbar.make(itemView,"شما به سوالات این درس پاسخ داده اید!",Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }

        public void bind(Lesson book)
        {
            bookname.setText(book.title);
            this.book = book;
        }


    }
    protected void onResume() {
        super.onResume();
        if((general.size()+special.size()) > 0 && Karnameh_item.answered.size() == (general.size()+special.size()))
        {
            dialog("پایان آزمون",
                    "شما به تمام درس های این ازمون پاسخ داده اید. \n جهت مشاهده ی کارنامه خود روی دکمه مشاهده کارنامه کلید کنید",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            Intent intent = new Intent(Past_konkur_Lessons.this,Azmoon_Karnameh.class);
                            startActivity(intent);
                            finish();
                        }
                    },"مشاهده ی کارنامه");
        }
    }

    public boolean checkBook(String name)
    {
        for (int i = 0; i < Karnameh_item.answered.size(); i++)
        {
            if(name.equals(Karnameh_item.answered.get(i).name.toString()))
            {
                return true;
            }
        }
        return false;
    }

    class GetData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Past_konkur_Lessons.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            getData(strings[0],strings[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            GroupName group2 = new GroupName("دروس عمومی",R.drawable.expandable_header_style_general, general.subList(0,general.size()));
            GroupName group1 = new GroupName("دروس تخصصی",R.drawable.expandable_header_style_special, special.subList(0,special.size()));

            List<GroupName> groupNames = Arrays.asList(group2,group1);

            RecyclerView list = (RecyclerView) findViewById(R.id.lessons_list);
            ExpandableRecyclerAdapter adapter = new ExpandableAdapter(Past_konkur_Lessons.this,groupNames);
            list.setAdapter(adapter);
            list.setLayoutManager(new LinearLayoutManager(Past_konkur_Lessons.this,LinearLayoutManager.VERTICAL,false));
            dialog.dismiss();
        }
    }

    private void getData(String major, String konkur)
    {
        try
        {
            lessons = new ArrayList<Lesson>();
            String req = URLEncoder.encode("major_id","utf-8") + "=" + URLEncoder.encode(major,"UTF-8");
            req += "&" + URLEncoder.encode("konkur_id","utf-8") + "=" + URLEncoder.encode(konkur,"UTF-8");
            URL url = new URL("http://www.takraqami.ir/Application/Get_Courses.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONArray json = new JSONArray(result);
            for(int i = 0; i < json.length();i++)
            {
                JSONObject object = json.getJSONObject(i);
                lessons.add(new Lesson(object));
                if(object.getString("past_konkur_course_type").equals("0"))
                {
                    general.add(new Lesson(object));
                }
                else
                {
                    special.add(new Lesson(object));
                }
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }

    class Karnameh_Lesson
    {
        public String id;
        public String name;

        public Karnameh_Lesson(String id, String name)
        {
            this.id = id;
            this.name = name;
        }
    }

    public class GroupName implements Parent<Lesson>
    {
        String name;
        int color;
        private List<Lesson> books;

        @Override
        public List<Lesson> getChildList() {
            return books;
        }

        @Override
        public boolean isInitiallyExpanded() {
            return false;
        }

        public GroupName(String name, int color, List<Lesson> book) {
            this.name = name;
            this.color = color;
            this.books = book;
        }

    }

    class Lesson
    {
        String title;
        int questioncount;
        int konkur;
        int lesson;
        String link;

        public Lesson(JSONObject object)
        {
            try {
                title = object.getString("past_konkur_course_title");
                questioncount = object.getInt("past_konkur_course_count");
                konkur = object.getInt("id_past_konkur");
                lesson = object.getInt("id_past_konkur_course");
                link = object.getString("past_konkur_question_location");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
    public void dialog(String dialog_title, String dialog_message, View.OnClickListener onOkKeyPress, String btn1)
    {
        ViewGroup parent = (ViewGroup) findViewById(android.R.id.content);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_soon,parent,false);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setText(btn1);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        title.setText(dialog_title);
        message.setText(dialog_message);
        builder.setView(view);
        dialogV = builder.create();
        dialogV.setCanceledOnTouchOutside(false);
        dialogV.setCancelable(false);
        dialogV.show();
        ok.setOnClickListener(onOkKeyPress);
        dialogV.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                finish();
            }
        });
    }

}
