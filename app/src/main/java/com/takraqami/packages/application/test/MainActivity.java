package com.takraqami.packages.application.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.takraqami.packages.application.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bbt);
    }

    public void goToBudget(View v){
        startActivity(new Intent(this,BudgetActivity.class));
    }
    public void goToPlanning(View v){
        startActivity(new Intent(this,PlanningActivity.class));
    }
    public void goToRanking(View v){
        startActivity(new Intent(this,RankingActivity.class));
    }
}
