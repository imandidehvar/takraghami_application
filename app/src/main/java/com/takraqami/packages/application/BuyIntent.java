package com.takraqami.packages.application;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.takraqami.packages.application.util.IabHelper;
import com.takraqami.packages.application.util.IabResult;
import com.takraqami.packages.application.util.Purchase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class BuyIntent extends AppCompatActivity {
    IabHelper mHelper;
    String id = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_intent);
        new GetData().execute();
        ((Button) findViewById(R.id.buy_Button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String base64EncodedPublicKey = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDDSgZ9rBOLvNiFDltVSBrjoitS04yVPtYVRlp5TQfqF7gsjal7/ZzUHJCqwr4b3frqfX8zMBIlrTIHeWmeC8TkXkzDZW7mT2w8YqKoaTImR+dEi17OQHLglVGM+QPHzGI/CeLmcEgiXnQtipWoBn2OEBvAq725kV3y5I+BOnSI9KmACh7KAl62jajTIhdBA/Fk0ChKg85sEbq9SAdKdf+FtnT++y9OF8pYnq1GdEUCAwEAAQ==";
                mHelper = new IabHelper(BuyIntent.this, base64EncodedPublicKey);
                mHelper.enableDebugLogging(false);
                buySub();
            }
        });

    }

    public void buySub()
    {
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if(result.isSuccess())
                {
                    mHelper.launchSubscriptionPurchaseFlow(BuyIntent.this, id, 1001,
                            new IabHelper.OnIabPurchaseFinishedListener() {
                                @Override
                                public void onIabPurchaseFinished(IabResult result, Purchase info) {
                                    if(result.isSuccess())
                                    {
                                        //onActivityResult
                                    }
                                    else
                                    {
                                        Toast.makeText(BuyIntent.this,result.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                }
                            },"skuMajor");
                }
                else
                {
                    Toast.makeText(BuyIntent.this,"اشکالی در اتصال به کافه بازار رخ داده است. لطفا لحظاتی دیگر محددا تلاش فرمایید",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try
        {
            if(data.hasExtra("INAPP_PURCHASE_DATA"))
            {
                JSONObject obejct = new JSONObject(data.getExtras().get("INAPP_PURCHASE_DATA").toString());
                new BuyOperation().execute(IntroActivity.manager.getStudent_id().toString(), obejct.get("orderId").toString());
                IntroActivity.manager.setAccount_Reciption(obejct.get("orderId").toString());
                IntroActivity.manager.setAccount_mode("Activated");
                finish();
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        //new BuyOperation().execute(id, data.getExtras().get("orderId"));
        //IntroActivity.manager.setAccount_Reciption(info.getOrderId());
    }

    class GetData extends AsyncTask<Void,Void,Void>
    {
        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                    URL url = new URL("http://www.takraqami.ir/Application/Buy.php");
                    URLConnection connection = url.openConnection();
                    connection.setDoOutput(true);
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.flush();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line + "\n");
                    }
                    String result = stringBuilder.toString();
                    JSONObject json = new JSONObject(result);
                    JSONArray data = json.getJSONArray("id");
                    JSONArray object = data.getJSONArray(0);
                    id = object.get(0).toString();
                } catch (IOException e) {e.printStackTrace();} catch (JSONException e) {e.printStackTrace();}
            return null;
        }
    }


    private static String BuyRes;
    class BuyOperation extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(BuyIntent.this);
            dialog.setMessage("در حال ثبت رسید. لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            Buy(strings[0],strings[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(BuyIntent.this);
            builder.setTitle("اتمام پرداخت");

            builder.setMessage("پرداخت با موفقیت انجام شد. لطفا مجدد برنامه را اجرا کنید");

            builder.setNegativeButton("خب!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void Buy(String student_id,String reciption) {
        try {
            String req = URLEncoder.encode("student_id", "utf-8") + "=" + URLEncoder.encode(student_id, "UTF-8");
            req += "&" + URLEncoder.encode("order_id", "utf-8") + "=" + URLEncoder.encode(reciption, "UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/BuyService.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(req);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("res");
            JSONArray object = data.getJSONArray(0);
            BuyRes = object.get(0).toString();
        } catch (IOException e) {
        } catch (JSONException e) {
        }
    }
}
