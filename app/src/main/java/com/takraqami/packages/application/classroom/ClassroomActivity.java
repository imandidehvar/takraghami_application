package com.takraqami.packages.application.classroom;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.takraqami.packages.application.ActivitySplash;
import com.takraqami.packages.application.BuyIntent;
import com.takraqami.packages.application.IntroActivity;
import com.takraqami.packages.application.MainActivity;
import com.takraqami.packages.application.R;
import com.takraqami.packages.application.test.util.VolleySingleton;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ClassroomActivity extends AppCompatActivity {

    final String URL = "http://takraqami.ir/Application/Get_Video_Category.php";

    LinkedList<CategoryItem> categories;
    ListView listView;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroom);

        findViewById(R.id.background_tarh).setBackgroundResource(R.mipmap.bg_tarh);
        ((TextView) findViewById(R.id.majorTitle)).setTypeface(MainActivity.font);

        listView = (ListView) findViewById(R.id.list);
        categories = new LinkedList<>();

        initList();
    }


    public void initList(){
        StringRequest req = new StringRequest(Request.Method.POST , URL , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray array = new JSONArray(response);
                    for(int i=0;i<array.length();i++){
                        categories.add( CategoryItem.fromJson(array.getJSONObject(i)));
                    }
                    listView.setAdapter(new CategoryItemAdapter(ClassroomActivity.this
                            ,R.layout.category_view,categories));
                    progress.dismiss();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: 09/14/2017
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("major",String.valueOf(IntroActivity.manager.getStudent_major_id()));
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(req);
        progress = new ProgressDialog(ClassroomActivity.this);
        progress.setMessage("لطفا صبر کنید");
        progress.setCancelable(false);
        progress.show();
    }

}
