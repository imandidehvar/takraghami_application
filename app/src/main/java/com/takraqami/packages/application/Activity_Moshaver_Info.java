package com.takraqami.packages.application;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Activity_Moshaver_Info extends AppCompatActivity {
    TextView tvCategory, answer;
    Spinner lstCategury;
    EditText question, title;
    Button sendQuestion;
    String[] categories;
    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__moshaver__info);

        ((ImageView) findViewById(R.id.background_tarh)).setBackgroundResource(R.mipmap.bg_tarh);

        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvCategory.setTypeface(MainActivity.font);
        lstCategury = (Spinner) findViewById(R.id.category);
        title = (EditText) findViewById(R.id.subject);
        title.setTypeface(MainActivity.font);
        answer = (TextView) findViewById(R.id.answer);
        answer.setTypeface(MainActivity.font);
        question = (EditText) findViewById(R.id.question);
        question.setTypeface(MainActivity.font);
        sendQuestion = (Button) findViewById(R.id.sendquestion);
        sendQuestion.setTypeface(MainActivity.font);

        if(getIntent().hasExtra("mode") && getIntent().getExtras().get("mode").toString().equals("send"))
        {
            tvCategory.setVisibility(View.INVISIBLE);
            answer.setVisibility(View.INVISIBLE);
            new GetData().execute();
        }
        else
        {
            lstCategury.setVisibility(View.INVISIBLE);
            tvCategory.setText(Counseling.counseling.getCategory());
            title.setText(Counseling.counseling.getTitle());
            question.setText(Counseling.counseling.getQuestion());
            question.setEnabled(false);
            title.setEnabled(false);
            if(Counseling.counseling.getFlag().equals("1"))
            {
                answer.setText(Counseling.counseling.getAnswer().toString());
            }
            else
            {
                answer.setText("هنوز پاسخی به درخواست مشاوره شما داده نشده!");
            }
            sendQuestion.setVisibility(View.INVISIBLE);
        }

        sendQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!title.getText().toString().isEmpty() && !question.getText().toString().isEmpty())
                {
                    new SendData().execute(getIntent().getExtras().get("user").toString(),title.getText().toString(),String.valueOf(lstCategury.getSelectedItemPosition()+1),question.getText().toString());
                }
                else
                {
                    Toast.makeText(Activity_Moshaver_Info.this,"لطفا تمام فیلد هارا کامل پر کنید",Toast.LENGTH_LONG).show();
                }
            }
        });

    }



    public class GetData extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Activity_Moshaver_Info.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... Params) {
            getData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayAdapter adapter = new ArrayAdapter(Activity_Moshaver_Info.this,android.R.layout.simple_spinner_dropdown_item,categories);
            lstCategury.setAdapter(adapter);
            dialog.dismiss();
        }
    }

    private void getData()
    {
        try
        {
            URL url = new URL("http://www.takraqami.ir/Application/Get_Categories.php");
            URLConnection connection = url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("categories");
            categories = new String[data.length()];
            for(int i = 0; i < data.length();i++)
            {
                JSONArray object = data.getJSONArray(i);
                categories[i] = object.get(0).toString();
            }
        }
        catch (IOException e) {e.printStackTrace();}
        catch (JSONException e){e.printStackTrace();}
    }


    class Categories
    {
        public String id;
        public String name;

        public Categories(JSONArray object)
        {
            try
            {
                id = object.get(0).toString();
                name = object.get(1).toString();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    class SendData extends AsyncTask<String,Void,Void>
    {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Activity_Moshaver_Info.this);
            dialog.setMessage("لطفا صبر کنید");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            sendData(strings[0],strings[1],strings[2],strings[3]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(result.equals("OK"))
            {
                Toast.makeText(Activity_Moshaver_Info.this,"درخواست مشاوره شما با موفقیت ثبت شد.", Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(Activity_Moshaver_Info.this,"مشکلی در شبکه پیش امده. درصورتی که از اتصال دستگاه خود به اینترنت اطمینان دارید دوباره امتحان کنید.", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
            startActivity(new Intent(Activity_Moshaver_Info.this,ActivitySplash.class));
            finish();
        }
    }

    private void sendData(String user, String title, String category, String question)
    {
        try
        {
            String request = URLEncoder.encode("user","utf-8")+"="+URLEncoder.encode(user,"UTF-8");
            request += "&" + URLEncoder.encode("title","utf-8")+"="+URLEncoder.encode(title,"UTF-8");
            request += "&" + URLEncoder.encode("category","utf-8")+"="+URLEncoder.encode(category,"UTF-8");
            request += "&" + URLEncoder.encode("question","utf-8")+"="+URLEncoder.encode(question,"UTF-8");

            URL url = new URL("http://www.takraqami.ir/Application/New_Question.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(request);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            String result = stringBuilder.toString();
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("res");
            JSONArray object = data.getJSONArray(0);
            Activity_Moshaver_Info.this.result = object.get(0).toString();
        }
        catch (IOException e) {}
        catch (JSONException e){}
    }
}
