package com.takraqami.packages.application.classroom;

import android.content.Context;
import android.widget.Button;

import org.json.JSONObject;

import static com.takraqami.packages.application.R.drawable.classroom_button;

/**
 * Created by Majid Ettehadi on 09/14/2017.
 */

public class CategoryItem {
    public String id;
    public String name;
    public String major;
    public String number;

    public CategoryItem(String id, String name, String major, String number){
        this.id=id;
        this.name=name;
        this.major=major;
        this.number=number;
    }

    public static CategoryItem fromJson(JSONObject json){
        try{
            return new CategoryItem(json.getString("id")
                    , json.getString("name")
                    , json.getString("major")
                    , json.getString("cnt"));
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
